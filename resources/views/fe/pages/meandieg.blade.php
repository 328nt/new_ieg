@extends('fe/layouts/index')
@section('content')

<!--Banner-->
<div class="container-fluid">
  <div class="row">
    <img src="fe/fe/image/bg_pageSmall.png" style="width:100%">
  </div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
  <h1 class="title_block font36">tôi và IEG</h1>
</div>

<!--qoutes-->
<div class="qoutes wpx1470">
  <p class="qoutes_content">
    Tại IEG, tất các các thông tin quan trọng cả bên trong & bên ngoài nhà trường được tổng hợp một các có hệ thống để
    việc lựa chọn luồng thông tin cho quá trình học tập trở nên thuận lợi hơn.
  </p>
</div>
<!--/qoutes_END-->

<!--Tôi và IEG-->
<div class="wpx1470 search_relative mt_160 mb_100 flex-box">

  <div class="col-md-7 col-sm-7 col-xs-12 oject_img m0">
    <a href=""><img src="upload/blog/{{$blog1->image}}"></a>
  </div>
  <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust ">

    <a href="/toi-va-ieg/{{$blog1->id}}/{{$blog1->titlenone}}.html">
      <h2 class="title_h2">{{$blog1->title}}</h2>
    </a>
    <p class="mt_20"><span>{{date("d/m/Y", strtotime($blog1->created_at))}}</span></p>
    <p class="font18">
      {!!$blog1->description!!}
    </p>
  </div>

</div>
<div class="toivaieg wpx1470 flex-box">
  @foreach ($blogs as $blog)
  <div class="col-4-cust">
    <a class="block_display" href="/toi-va-ieg/{{$blog->id}}/{{$blog->titlenone}}.html">
      <img class="blog-img" src="upload/blog/{{$blog->image}}">
      <p class="mt_20"><span>{{date("d/m/Y", strtotime($blog->created_at))}}</span></p>
      <p class="title_news font18">{{$blog->title}}</p>
      <p class="font18"><span>Trần Hoài</span></p>
    </a>
  </div>
  @endforeach
</div>
<!--/Tôi và IEG_END-->
<p><a class="mb_150 mt_130"></a></p>


<!--Khóa học-->
@include('fe.pages.form_course')
{{-- <div class="container wpx920_width100vh">
  <h1 class="title_block">CÁC KHÓA HỌC TẠI IEG</h1>
  <div class="row flex-box block_01">
    <div class="col-2-cust text-center">
      <a href="day-va-hoc/tieng-anh.html">
        <img src="fe/fe/image/iconinfo_01.png">
        <p class="info_KH">Tiếng Anh</p>
      </a>
    </div>
    <div class="col-2-cust text-center">
      <a href="khoa-hoc.html">
        <img src="fe/fe/image/iconinfo_02.png">
        <p class="info_KH">Khoa Học</p>
      </a>

    </div>
    <div class="col-2-cust text-center">
      <a href="day-va-hoc/toan-hoc.html">
        <img src="fe/fe/image/iconinfo_03.png">
        <p class="info_KH">Toán Học</p>
      </a>
    </div>
    <div class="col-2-cust text-center">
      <a href="day-va-hoc/socrates.html">
        <img src="fe/fe/image/iconinfo_04.png">
        <p class="info_KH">Socrates</p>
      </a>

    </div>
    <div class="col-2-cust text-center">
      <a href="day-va-hoc/ielts.html">
        <img src="fe/fe/image/iconinfo_05.png">
        <p class="info_KH">IELTS</p>
      </a>

    </div>
  </div>
</div> --}}
<!--/Khóa học_END-->
@endsection