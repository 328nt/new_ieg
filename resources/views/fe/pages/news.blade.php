@extends('fe/layouts/index')
@section('content')

<!--Banner-->
<div class="container-fluid">
	<div class="row">
		<img src="fe/image/bg_pageSmall_1.png" style="width:100%">
	</div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
	<a href="tin-tuc-news.html">
		<h1 class="title_block font36">tin tức</h1>
	</a>
</div>
<div class="qoutes wpx1470 mt_100">
	<p class="qoutes_content">
		Tại IEG, tất các các thông tin quan trọng cả bên trong & bên ngoài nhà trường được tổng hợp một các có hệ thống
		để việc lựa chọn luồng thông tin cho quá trình học tập trở nên thuận lợi hơn.
	</p>
</div>
<div class="content_baiviet wpx1470">
	<div class="content_left wpx400">
		<ul class="content_left--list mt_130_cust">
			<li class="active"><a>Tin tức mới</a></li>
			@foreach ($news as $new)
			@if ($new->id_category == 1)
			<li>
				<ul class="list-inline">
					<li class="col-md-6">
						<a href="{{route('titlenone', [$new->id, $new->titlenone])}}">
							<img src="upload/news/{{$new->image}}">
						</a>
					</li>
					<li class="col-md-6">
						<a class="font18 apd-10"
							href="tin-tuc/{{$new->id}}/{{$new->titlenone}}.html">{{$new->title}}</a>
					</li>
				</ul>
			</li>
			@endif
			@endforeach

		</ul>
	</div>
	<div class="content_right">
		<div class="toivaIEG_baivietchitiet mb_100">
			<div class="newrela">
				<a href="{{route('titlenone', [$allspl->id, $allspl->titlenone])}}">
				<span class="wimg100"><img src="upload/news/{{$allspl->image}}"></span>
				<div class="news--text">
					<p>{{$allspl->title}}</p>
				</div>
				</a>
			</div>
			<ul class="flex-box box_listCust">
				@foreach ($spotlight as $spl)
				@if ($spl->id_category == 1)
				<li class="bg_li bg_xam">
					<a href="tin-tuc/{{$spl->id}}/{{$spl->titlenone}}.html">
						<img style="
						height: 200px;
						width: auto;
					" src="upload/news/{{$spl->image}}">
						<p class="time_news pd_10">{{date("d/m/Y", strtotime($spl->created_at))}}</p>
						<p class="font30 pd_10 tttp">{{$spl->title}}</p>
					</a>
				</li>
				@endif
				@endforeach
			</ul>
		</div>
	</div>
</div>
<div class="container">
	<a href="su-kien.html">
		<h1 class="title_block font36">Sự kiện</h1>
	</a>
	@foreach ($events as $event)
	<p class="mt_40"><a target="_blank" href="{{$event->link}}"><img src="upload/news/{{$event->image}}"></a></p>
	@endforeach
</div>
<h1 class="title_block font36 mt_80 mb_80"><a href="ieg-tv.html">IEG TV</a></h1>
<div class="container-fluid bg_xam pdt_60 mb_30">
	<div class="row">
		<div class="container text-center">
			<a href="ieg-tv.html">
				<img src="fe/image/tintuc_8.jpg" width="100%">
			</a>
			<p>
				<a href="ieg-tv.html" class="btn_moreCust bg_vang font18">Xem thêm...</a>
			</p>
		</div>
	</div>
</div>

<!--Khóa học-->
    @include('fe/pages/form_course')
<!--/Khóa học_END-->
@endsection