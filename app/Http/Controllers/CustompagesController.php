<?php

namespace App\Http\Controllers;

use App\Custompages;
use Illuminate\Http\Request;

class CustompagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // return view('be/custompage/edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        
        $custom = new Custompages();
        $custom->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Custompages  $custompages
     * @return \Illuminate\Http\Response
     */
    public function show(Custompages $custompages)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Custompages  $custompages
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $custom = Custompages::all()->first();
        return view('be/custompage/edit', compact('custom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Custompages  $custompages
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $custom = Custompages::find($id);
        $custom->title = $rq->title;
        $custom->quote = $rq->quote;
        $custom->sec1_title = $rq->sec1_title;
        $custom->sec1_content = $rq->sec1_content;
        if ($rq->HasFile('sec1_image')) {
            $file = $rq->file('sec1_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/custom/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/custom", $img);
            // unlink("upload/custom/".$custom->image);
            $custom->sec1_image = $img;
        }
        $custom->sec2_title = $rq->sec2_title;
        $custom->sec2_content = $rq->sec2_content;
        if ($rq->HasFile('sec2_image')) {
            $file = $rq->file('sec2_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/custom/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/custom", $img);
            // unlink("upload/custom/".$custom->image);
            $custom->sec2_image = $img;
        }
        
        $custom->sec3_title = $rq->sec3_title;
        $custom->sec3_content = $rq->sec3_content;
        if ($rq->HasFile('sec3_image')) {
            $file = $rq->file('sec3_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/custom/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/custom", $img);
            // unlink("upload/custom/".$custom->image);
            $custom->sec3_image = $img;
        }
        $custom->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Custompages  $custompages
     * @return \Illuminate\Http\Response
     */
    public function destroy(Custompages $custompages)
    {
        //
    }
}
