@extends('be.layouts.index')
@section('title')
Sửa Khóa học
@endsection
@section('content')
@include('msg')
<div class="col-md-12">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Sửa Khóa học</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <form action="{{route('update_partner', $partner->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal"
                id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">name</label>
                    <div class="col-sm-10">
                    <input class="form-control" name="name" type="text" value="{{$partner->name}}" placeholder="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">link</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="link" type="text" value="{{$partner->link}}" placeholder="link">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">alt</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="alt" type="text" value="{{$partner->alt}}" placeholder="alt">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">image</label>
                    <div class="col-sm-5">
                        <input class="form-control" name="image" type="file">
                    </div>
                    <div class="col-sm-5">
                        <img width="200px" src="upload/partners/{{$partner->image}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-info" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<hr>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection