@extends('fe/layouts/index')
@section('content')

<div class="container-fluid">
    <div class="row">
        <img src="../fe/image/bg_pageSmall_1.png" style="width:100%">
    </div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
    <h1 class="title_block font36">{{$new1->title}}</h1>
</div>

<div class="content_baiviet wpx1470">
    <div class="content_left wpx400">
        <ul class="content_left--list">
            <li class="active"><a>Danh Mục</a></li>
            <li class=""><a href="tin-tuc.html">Tin tức</a></li>
            <li class=""><a href="su-kien.html">Sự kiện</a></li>
            <li class=""><a href="toi-va-ieg.html">Tôi và IEG</a></li>
            {{-- <li class=""><a href="highcharts-demo.html">Highcharts Demo</a></li> --}}
        </ul>
        <ul class="content_left--list mt_130_cust ">
            <li class="active"><a>Bài viết tương tự</a></li>
            @foreach ($news as $new)
            <li>
                <ul class="list-inline">
                    <li class="col-md-6">
                        <a href="tin-tuc/{{$new->id}}/{{$new->titlenone}}.html">
                            <img style="width:100%" src="upload/news/{{$new->image}}">
        </a>
        </li>
        <li class="col-md-6">
            <a class="font18 apd-10" href="tin-tuc/{{$new->id}}/{{$new->titlenone}}.html">{{$new->title}}</a>
        </li>
        </ul>
        </li>
        @endforeach
        </ul>
    </div>
    <div class="content_right">

        <div class="text-center mt_40 mb_40">
            <h1 class="title_block font36">{{$new1->title}}</h1>
        </div>
        <div class="toivaIEG_baivietchitiet mb_100">
            <span class="wimg100"><img src="upload/news/{{$new1->image}}"></span>
            <p class="mt_40"><span class="time_news">{{date("d/m/Y", strtotime($new1->created_at))}}</span></p>
            <p>{!!$new1->content!!}</p>
            <div class="col-md-12 mt_40">
                <div class="row">
                    <ul class="flex-box">
                        <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li>
                            <ul>
                                <li>
                                    <a href="#">Creat</a>
                                </li>
                                <li>
                                    Admin User
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Khóa học-->
    @include('fe/pages/form_course')
<!--/Khóa học_END-->
@endsection