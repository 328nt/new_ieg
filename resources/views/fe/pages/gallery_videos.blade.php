@extends('fe/layouts/index')
@section('title')
Video
@endsection
@section('content')
<!--Banner-->
<div class="container-fluid">
    <div class="row"> <img src="fe/image/bg_pageSmall_2.png"> </div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
    <h1 class="title_block font36">ieg tv</h1>
</div>
<div class="qoutes wpx1470 mt_100">
    <p class="qoutes_content"> Tại IEG, tất các các thông tin quan trọng cả bên trong & bên ngoài nhà trường được tổng
        hợp một các có hệ thống để việc lựa chọn luồng thông tin cho quá trình học tập trở nên thuận lợi hơn. </p>
</div>
<!--Tôi và IEG-->
<div class="newrela wpx1470 mb_30">
    <div class="wimg100">
        <iframe class="videos" src="https://www.youtube.com/embed/NQAYb9ok4s0"></iframe></div><a href="">
        <div class="news--text">
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the
                industr</p>
        </div>
    </a>
</div>
<div class="tinnoibat wpx1470">
    <h2 class="title_tintuc font24">Nổi bật</h2>
    <div class="toivaieg flex-box">
        @foreach ($spotlight as $video)
        <div class="col-4-cust">
            <div class="block_display">
                <a href="/ieg-tv/video/{{$video->id}}.html">
                    <p>
                        <img src="https://img.youtube.com/vi/{{substr($video->link,-11)}}/0.jpg" alt=""><span
                            class="icon-play2"><i class="fas fa-play-circle"></i></span></p>
                </a>
                <p class="title_news font18">{{$video->title}}</p>
                <p class="mt_20"><span>{{date("d/m/Y", strtotime($video->created_at))}}</span></p>
            </div>
        </div>
        @endforeach
    </div>
    <!--/Tôi và IEG_END-->
</div>
<div class="tinnoibat wpx1470">
    <h2 class="title_tintuc font24 mt_100">Phổ biến</h2>
    <div class="toivaieg flex-box">
        @foreach ($videos as $video)
        <div class="col-4-cust">
            <div class="block_display">
                <a href="/ieg-tv/video/{{$video->id}}.html">
                    <p>
                        <img src="https://img.youtube.com/vi/{{substr($video->link,-11)}}/0.jpg" alt=""><span
                            class="icon-play2"><i class="fas fa-play-circle"></i></span></p>
                </a>
                <p class="title_news font18">{{$video->title}}</p>
                <p class="mt_20"><span>{{date("d/m/Y", strtotime($video->created_at))}}</span></p>
            </div>
        </div>
        @endforeach
    </div>
    <!--/Tôi và IEG_END-->
</div>
{{-- <div class="tinnoibat wpx1470">
    <h2 class="title_tintuc font24">Chủ đề</h2>
    <div class="toivaieg flex-box">
        <div class="col-4-cust">
            <div class="block_display">

                <iframe width="100%" height="250px;"
                    src="https://www.youtube.com/embed/{{substr($video->link,-11)}}"></iframe>
<p class="title_news font18">Đồ gỗ mỹ nghệ tinh xảo giá “khủng” tại chợ xuân phố núi sdasd sdfsdfsdf</p>
<p class="mt_20"><span>2019-08-01 01:32:16</span></p>
</div>
</div>
<div class="col-4-cust">
    <div class="block_display">

        <iframe width="100%" height="250px;" src="https://www.youtube.com/embed/{{substr($video->link,-11)}}"></iframe>
        <p class="title_news font18">Đồ gỗ mỹ nghệ tinh xảo giá “khủng” tại chợ xuân phố núi sdasd sdfsdfsdf</p>
        <p class="mt_20"><span>2019-08-01 01:32:16</span></p>
    </div>
</div>
<div class="col-4-cust">
    <div class="block_display">

        <iframe width="100%" height="250px;" src="https://www.youtube.com/embed/{{substr($video->link,-11)}}"></iframe>
        <p class="title_news font18">Đồ gỗ mỹ nghệ tinh xảo giá “khủng” tại chợ xuân phố núi sdasd sdfsdfsdf</p>
        <p class="mt_20"><span>2019-08-01 01:32:16</span></p>
    </div>
</div>
</div>
<!--/Tôi và IEG_END-->
</div> --}}

<!--Khóa học-->
<div class="container wpx920_width100vh mt_100">
    <h1 class="title_block">CÁC KHÓA HỌC TẠI IEG</h1>
    <div class="row flex-box block_01">
        <div class="col-2-cust text-center"> <a href="day-va-hoc/tieng-anh.html"> <img
                    src="fe/fe/image/iconinfo_01.png">
                <p class="info_KH">Tiếng Anh</p>
            </a> </div>
        <div class="col-2-cust text-center"> <a href="khoa-hoc.html"> <img src="fe/fe/image/iconinfo_02.png">
                <p class="info_KH">Khoa Học</p>
            </a> </div>
        <div class="col-2-cust text-center"> <a href="day-va-hoc/toan-hoc.html"> <img src="fe/fe/image/iconinfo_03.png">
                <p class="info_KH">Toán Học</p>
            </a> </div>
        <div class="col-2-cust text-center"> <a href="day-va-hoc/socrates.html"> <img src="fe/fe/image/iconinfo_04.png">
                <p class="info_KH">Socrates</p>
            </a> </div>
        <div class="col-2-cust text-center"> <a href="day-va-hoc/ielts.html"> <img src="fe/fe/image/iconinfo_05.png">
                <p class="info_KH">IELTS</p>
            </a> </div>
    </div>
</div>
<!--/Khóa học_END-->
@endsection