<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// test
// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('index', function () {
//     return view('fe.layouts.index');
// });


Route::any('/search', 'PagesController@search');
Route::any('/search_results', 'PagesController@search_results');

// frontend
Route::get('/', 'PagesController@index');
Route::get('/cau-chuyen-ieg.html', 'PagesController@about')->name('about');
Route::get('/nguoi-giu-lua.html', 'PagesController@kotf')->name('kotf');
Route::get('/day-va-hoc.html', 'PagesController@teachandlearn')->name('teachandlearn');
Route::get('/day-va-hoc/{id}/{slug_name}.html', 'PagesController@course')->name('course');
Route::get('/day-va-hoc/toan-hoc.html', 'PagesController@math')->name('math');
Route::get('/day-va-hoc/tieng-anh.html', 'PagesController@eng')->name('eng');
Route::get('/day-va-hoc/khoa-hoc.html', 'PagesController@science')->name('science');
Route::get('/day-va-hoc/socrates.html', 'PagesController@socrates')->name('socrates');
Route::get('/day-va-hoc/ielts.html', 'PagesController@ielts')->name('ielts');
Route::get('/toi-va-ieg.html', 'PagesController@meandieg')->name('meandieg');
Route::get('/toi-va-ieg/{id}/{titlenone}.html', 'PagesController@blogsingle')->name('blogsingle');
Route::get('/tin-tuc.html', 'PagesController@news')->name('news');
Route::get('/blog.html', 'PagesController@news')->name('blog');
// Route::get('/tin-tuc/{id}.html', 'PagesController@newsingle')->name('newsingle');
Route::get('/tin-tuc/{id}/{titlenone}.html', 'PagesController@newsingle')->name('titlenone');
Route::get('/su-kien.html', 'PagesController@events')->name('events');

Route::post('/consulting', 'ConsultingController@store_consulting')->name('consulting');
Route::post('/test', 'ConsultingController@store_test')->name('contact_test');
Route::post('/register', 'ConsultingController@store_register')->name('contact_register');
Route::get('/congratulations.html', 'PagesController@congratulations')->name('congratulations');
Route::get('template', function () {
    return view('be.mail.confirm_email');
});



Route::get('news/category/{id}', 'PagesController@category')->name('category');
Route::get('/news', 'PagesController@news')->name('news');
Route::get('/news/{id}', 'PagesController@newsingle')->name('newsingle');
Route::post('comment/{id}', 'CommentController@store');
Route::post('video/comment/{id}', 'CommentController@store_video')->name('comment_video');
Route::get('/images', 'PagesController@images')->name('images');
Route::get('/ieg-tv.html', 'PagesController@gallery_videos')->name('gallery_videos');
Route::get('/ieg-tv/video/{id}.html', 'PagesController@video')->name('video');
;

Route::get('sendmail', 'PagesController@sendmail');
Route::post('gogoemail','PagesController@gogoemail')->name('send_mail');




// adminlogin
Route::get('/admin', 'UserController@getloginadmin');
Route::get('/admin/login', 'UserController@getloginadmin');
Route::post('/admin/login', 'UserController@postloginadmin')->name('admin_login');
Route::get('admin/logout', 'UserController@logoutadmin');

Route::get('export', 'ExcelController@export')->name('export');
Route::get('importExportView', 'ExcelController@importExportView');
Route::post('import', 'ExcelController@import')->name('import');
// endadminlogin

// admin
Route::group(['prefix' => 'admin' , 'middleware' => 'is_admin'], function () {

    Route::group(['prefix' => 'videos'], function () {
        Route::get('list', 'GalleryVideosController@index')->name('list_video');
        Route::get('add', 'GalleryVideosController@create');
        Route::post('add', 'GalleryVideosController@store')->name('add_video');
        Route::get('edit/{id}', 'GalleryVideosController@edit')->name('edit');
        Route::post('edit/{id}', 'GalleryVideosController@update')->name('update');
        // Route::get('delete/{id}', 'GalleryVideosController@destroy')->name('destroy');  
        Route::post('destroy', 'GalleryVideosController@destroy')->name('destroy_video'); 
    });

    Route::group(['prefix' => 'users', 'middleware' => 'is_all'], function () {
        Route::get('list', 'UserController@index');
        Route::get('add', 'UserController@create');
        Route::post('add', 'UserController@store')->name('add_user');
        Route::get('edit/{id}', 'UserController@edit')->name('edit_user');
        Route::post('edit/{id}', 'UserController@update')->name('update_user');
        Route::get('delete/{id}', 'UserController@destroy')->name('destroy');   
    });

    Route::group(['prefix' => 'news'], function () {
        Route::get('list', 'NewsController@index');
        Route::get('add', 'NewsController@create');
        Route::post('add', 'NewsController@store')->name('add_news');
        Route::get('edit/{id}', 'NewsController@edit')->name('edit_news');
        Route::post('edit/{id}', 'NewsController@update')->name('update_news');
        Route::get('delete/{id}', 'NewsController@destroy')->name('destroy');   
    });

    Route::group(['prefix' => 'blogs'], function () {
        Route::get('list', 'BlogController@index');
        Route::get('add', 'BlogController@create');
        Route::post('add', 'BlogController@store')->name('add_blog');
        Route::get('edit/{id}', 'BlogController@edit')->name('edit_blog');
        Route::post('edit/{id}', 'BlogController@update')->name('update_blog');
        Route::get('delete/{id}', 'BlogController@destroy')->name('destroy');   
    });

    Route::group(['prefix' => 'contacts'], function () {
        Route::get('consultings', 'ConsultingController@index_consultings');
        Route::get('test', 'ConsultingController@index_test');
        Route::get('register', 'ConsultingController@index_register');
        Route::get('edit/{id}', 'ConsultingController@edit')->name('edit_contact');
        Route::post('edit/{id}', 'ConsultingController@update')->name('update_contact');
        Route::post('contacted', 'ConsultingController@contacted')->name('contacted');  
    });

    Route::group(['prefix' => 'customs', 'middleware' => 'is_all'], function () {
        Route::get('list', 'CustomskotfController@index');
        Route::get('add', 'CustomskotfController@create');
        Route::post('add', 'CustomskotfController@store')->name('add_kotf');
        Route::get('edit/{id}', 'CustomskotfController@edit')->name('edit_kotf');
        Route::post('edit/{id}', 'CustomskotfController@update')->name('update_kotf');
        Route::get('delete/{id}', 'CustomskotfController@destroy')->name('destroy');   
    });
    Route::group(['prefix' => 'slides', 'middleware' => 'is_all'], function () {
        Route::get('list', 'SlideController@index');
        Route::get('add', 'SlideController@create');
        Route::post('add', 'SlideController@store')->name('add_slide');
        Route::get('edit/{id}', 'SlideController@edit')->name('edit_slide');
        Route::post('edit/{id}', 'SlideController@update')->name('update_slide');
        Route::get('delete/{id}', 'SlideController@destroy')->name('destroy');   
    });
    Route::group(['prefix' => 'custompage', 'middleware' => 'is_all'], function () {
        Route::get('list', 'CustompagesController@index');
        Route::get('add', 'CustompagesController@create');
        Route::post('store', 'CustompagesController@store')->name('add_config');
        Route::get('edit/{id}', 'CustompagesController@edit')->name('edit_config');
        Route::post('edit/{id}', 'CustompagesController@update')->name('update_config');
        Route::get('delete/{id}', 'CustompagesController@destroy')->name('destroy');   
    });
    Route::group(['prefix' => 'courses', 'middleware' => 'is_all'], function () {
        Route::get('list', 'CourseController@index');
        Route::get('add', 'CourseController@create');
        Route::post('store', 'CourseController@store')->name('add_course');
        Route::get('edit/{id}', 'CourseController@edit')->name('edit_course');
        Route::post('edit/{id}', 'CourseController@update')->name('update_course');
        Route::get('delete/{id}', 'CourseController@destroy')->name('destroy_course');   
    });
    Route::group(['prefix' => 'partners'], function () {
        Route::get('list', 'PartnerController@index');
        Route::get('add', 'PartnerController@create')->name('add_partner');
        Route::post('store', 'PartnerController@store')->name('store_partner');
        Route::get('edit/{id}', 'PartnerController@edit')->name('edit_partner');
        Route::post('edit/{id}', 'PartnerController@update')->name('update_partner');
        Route::get('delete/{id}', 'PartnerController@destroy')->name('destroy_partner');   
    });
    Route::group(['prefix' => 'about', 'middleware' => 'is_all'], function () {
        Route::get('edit/{id}', 'AboutController@edit')->name('edit_about');
        Route::post('edit/{id}', 'AboutController@update')->name('update_about');  
    });
    Route::group(['prefix' => 'comment'], function () {
        
        Route::get('delete/{id}', 'CommentController@destroy')->name('destroy');   
    });
});
