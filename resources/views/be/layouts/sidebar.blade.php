<nav class="page-sidebar" id="sidebar">
    <div id="sidebar-collapse">
        <div class="admin-block d-flex">
            <div>
                <img src="upload/users/{{Auth::User()->image}}" width="45px" />
            </div>
            <div class="admin-info">
                <div class="font-strong">{{Auth::User()->fullname}}</div><small>{{Auth::User()->roles->name}}</small>
            </div>
        </div>
        <ul class="side-menu metismenu">
            <li>
                <a class="active" href="index.html"><i class="sidebar-item-icon fa fa-th-large"></i>
                    <span class="nav-label">Dashboard</span>
                </a>
            </li>
            <li class="heading">FEATURES</li>
            @if (Auth::User()->role == 1)
            <li class="{{ request()->is('admin/users*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-bookmark"></i>
                    <span class="nav-label">Quản trị</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/users/list">Danh sách quản trị</a>
                    </li>
                    <li>
                        <a href="admin/users/add">Thêm quản trị</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/news*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-newspaper-o"></i>
                    <span class="nav-label">Tin tức</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/news/list">Danh sách tin tức</a>
                    </li>
                    <li>
                        <a href="admin/news/add">Thêm bài viết</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/blogs*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-hacker-news"></i>
                    <span class="nav-label">Blogs</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/blogs/list">Danh sách Blog</a>
                    </li>
                    <li>
                        <a href="admin/blogs/add">Thêm bài viết</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/courses*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-video-camera"></i>
                    <span class="nav-label">Courses</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/courses/list">Danh sách courses</a>
                    </li>
                    <li class="{{ request()->is('admin/courses/add') ? 'active' : '' }}">
                        <a href="admin/courses/add">Thêm courses</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/videos*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-video-camera"></i>
                    <span class="nav-label">Videos</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/videos/list">Danh sách video</a>
                    </li>
                    <li class="{{ request()->is('admin/videos/add') ? 'active' : '' }}">
                        <a href="admin/videos/add">Thêm video</a>
                    </li>
                </ul>
            </li>
            <li class="heading">CUSTOMS</li>
            <li class="{{ request()->is('admin/about*') ? 'active' : '' }}">
                <a href="{{route('edit_about', 1 )}}"><i class="sidebar-item-icon fa fa-envelope"></i>
                    <span class="nav-label">Thông tin công ty</span></a>
                    
            </li>
            <li class="{{ request()->is('admin/partners*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-video-camera"></i>
                    <span class="nav-label">Đối tác</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/partners/list">Danh sách đối tác</a>
                    </li>
                    <li class="{{ request()->is('admin/partners/add') ? 'active' : '' }}">
                        <a href="admin/partners/add">Thêm đối tác</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/custompage*') ? 'active' : '' }}">
                <a href="{{route('edit_config', 1 )}}"><i class="sidebar-item-icon fa fa-envelope"></i>
                    <span class="nav-label">Câu chuyện IEG</span></a>
                    
            </li>
            <li class="{{ request()->is('admin/customs*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-envelope"></i>
                    <span class="nav-label">Người giữ lửa</span><i class="fa fa-angle-left arrow"></i></a>
                    <ul class="nav-2-level collapse">
                        <li>
                            <a href="admin/customs/list">Customs page</a>
                        </li>
                        <li class="{{ request()->is('admin/customs/add') ? 'active' : '' }}">
                            <a href="admin/customs/add">add</a>
                        </li>
                    </ul>
            </li>
            <li class="{{ request()->is('admin/slides*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-video-camera"></i>
                    <span class="nav-label">Slide</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/slides/list">Danh sách slide</a>
                    </li>
                    <li class="{{ request()->is('admin/slides/add') ? 'active' : '' }}">
                        <a href="admin/slides/add">Thêm slide</a>
                    </li>
                </ul>
            </li>
            <li class="heading">LIÊN HỆ</li>
            <li class="{{ request()->is('admin/contacts*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-envelope"></i>
                    <span class="nav-label">Mailbox</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li class="{{ request()->is('admin/contacts/consultings*') ? 'active' : '' }}">
                        <a href="admin/contacts/consultings">Tư vấn</a>
                    </li>
                    <li>
                        <a href="admin/contacts/test">Đăng ký test</a>
                    </li>
                    <li>
                        <a href="admin/contacts/register">Đăng ký</a>
                    </li>
                </ul>
            </li>
            @else
            <li class="{{ request()->is('admin/news*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-newspaper-o"></i>
                    <span class="nav-label">Tin tức</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/news/list">Danh sách tin tức</a>
                    </li>
                    <li>
                        <a href="admin/news/add">Thêm bài viết</a>
                    </li>
                </ul>
            </li>
            <li class="{{ request()->is('admin/blogs*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-hacker-news"></i>
                    <span class="nav-label">Blogs</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li>
                        <a href="admin/blogs/list">Danh sách Blog</a>
                    </li>
                    <li>
                        <a href="admin/blogs/add">Thêm bài viết</a>
                    </li>
                </ul>
            </li>
            <li class="heading">LIÊN HỆ</li>
            <li class="{{ request()->is('admin/contacts*') ? 'active' : '' }}">
                <a href="javascript:;"><i class="sidebar-item-icon fa fa-envelope"></i>
                    <span class="nav-label">Mailbox</span><i class="fa fa-angle-left arrow"></i></a>
                <ul class="nav-2-level collapse">
                    <li class="{{ request()->is('admin/contacts/consultings*') ? 'active' : '' }}">
                        <a href="admin/contacts/consultings">Tư vấn</a>
                    </li>
                    <li>
                        <a href="admin/contacts/test">Đăng ký test</a>
                    </li>
                    <li>
                        <a href="admin/contacts/register">Đăng ký</a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
</nav>