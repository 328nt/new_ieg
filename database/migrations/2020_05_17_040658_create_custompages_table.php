<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCustompagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('custompages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title')->nullable();
            $table->string('quote')->nullable();
            $table->string('sec1_title')->nullable();
            $table->string('sec1_content')->nullable();
            $table->string('sec1_image')->nullable();
            $table->string('sec2_title')->nullable();
            $table->string('sec2_content')->nullable();
            $table->string('sec2_image')->nullable();
            $table->string('sec3_title')->nullable();
            $table->string('sec3_content')->nullable();
            $table->string('sec3_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('custompages');
    }
}
