<?php

namespace App\Http\Controllers;

use App\Customconfig;
use Illuminate\Http\Request;

class CustomconfigController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $customs = Customconfig::find(1);
        return view('be/custompage/add', compact('customs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        
        $custom = new Customconfig();
        $custom->store_ieg = [
            'title'=>$rq->title,
            'quote'=>$rq->quote
        ];
        // dd($custom->store_ieg);
        $custom->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customconfig  $customconfig
     * @return \Illuminate\Http\Response
     */
    public function show(Customconfig $customconfig)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customconfig  $customconfig
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $custom = Customconfig::all()->first();
        return view('be/custompage/edit', compact('custom'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customconfig  $customconfig
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $custom = Customconfig::find($id);
        $custom->store_ieg = [
            'title'=>$rq->title,
            'quote'=>$rq->quote,
            'sec1_title'=>$rq->sec1_title,
            'sec1_content'=>$rq->sec1_content,
            'sec1_image'=>$rq->sec1_image,
            'sec2_title'=>$rq->sec2_title,
            'sec2_content'=>$rq->sec2_content,
            'sec2_image'=>$rq->sec2_image,
            'sec3_title'=>$rq->sec3_title,
            'sec3_content'=>$rq->sec3_content,
            'sec1_image'=>$rq->sec1_image,
        ];
        // dd($custom->store_ieg);
        $custom->save();
        return redirect()->back()->with('msg','oklah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customconfig  $customconfig
     * @return \Illuminate\Http\Response
     */
    public function destroy(Customconfig $customconfig)
    {
        //
    }
}
