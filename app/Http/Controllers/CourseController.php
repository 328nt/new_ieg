<?php

namespace App\Http\Controllers;

use App\Course;
use Illuminate\Http\Request;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $courses = Course::All();
        return view('be/courses/list', ['courses'=>$courses]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be/courses/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'title' => 'required|max:500',
            'quote' => 'required',
            'image' => 'required',

        ],[
            'title.required' =>'chưa nhập tiêu đề',
            'quote.required' =>'chưa nhập tóm tắt',
            'image.required' => 'vui lòng nhập ảnh',

        ]);
        $courses = new Course();
        $courses->name = $rq->name;
        $courses->slug_name = str_slug($rq->name);
        if ($rq->hasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            $courses->image = $img;
        } else {
            $courses->image = "";
        }
        if ($rq->hasFile('icon')) {
            $file = $rq->file('icon');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            $courses->icon = $img;
        } else {
            $courses->icon = "";
        }
        $courses->title = $rq->title;
        $courses->quote = $rq->quote;
        $courses->header_course = $rq->header_course;
        $courses->sec1_title = $rq->sec1_title;
        $courses->sec1_content = $rq->sec1_content;
        if ($rq->hasFile('sec1_image')) {
            $file = $rq->file('sec1_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            $courses->sec1_image = $img;
        } else {
            $courses->sec1_image = "";
        }
        $courses->sec2_title = $rq->sec2_title;
        $courses->sec2_content = $rq->sec2_content;
        if ($rq->hasFile('sec2_image')) {
            $file = $rq->file('sec2_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            $courses->sec2_image = $img;
        } else {
            $courses->sec2_image = "";
        }
        $courses->sec3_title = $rq->sec3_title;
        $courses->sec3_content = $rq->sec3_content;
        if ($rq->hasFile('sec3_image')) {
            $file = $rq->file('sec3_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            $courses->sec3_image = $img;
        } else {
            $courses->sec3_image = "";
        }
        
        $courses->save();
        return redirect('admin/courses/list')->with('msg','thêm nhân viên thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function show(Course $course)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $course = Course::Find($id);
        return view('be/courses/edit', ['course'=>$course ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        
        $this->validate($rq,[

        ],[

        ]);
        $courses = Course::Find($id);
        $courses->name = $rq->name;
        if ($rq->hasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            // unlink("upload/courses/".$courses->image);
            $courses->image = $img;
        }
        if ($rq->hasFile('icon')) {
            $file = $rq->file('icon');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            // unlink("upload/courses/".$courses->icon);
            $courses->icon = $img;
        }
        $courses->title = $rq->title;
        $courses->quote = $rq->quote;
        $courses->header_course = $rq->header_course;
        $courses->sec1_title = $rq->sec1_title;
        $courses->sec1_content = $rq->sec1_content;
        if ($rq->hasFile('sec1_image')) {
            $file = $rq->file('sec1_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            $courses->sec1_image = $img;
        }
        $courses->sec2_title = $rq->sec2_title;
        $courses->sec2_content = $rq->sec2_content;
        if ($rq->hasFile('sec2_image')) {
            $file = $rq->file('sec2_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            // unlink("upload/courses/".$courses->sec2_image);
            $courses->sec2_image = $img;
        }
        $courses->sec3_title = $rq->sec3_title;
        $courses->sec3_content = $rq->sec3_content;
        if ($rq->hasFile('sec3_image')) {
            $file = $rq->file('sec3_image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/courses/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/courses", $img);
            // unlink("upload/courses/".$courses->sec3_image);
            $courses->sec3_image = $img;
        }
        
        $courses->save();
        return redirect('admin/courses/edit/'.$id)->with('msg','Sửa khóa học thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Course  $course
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Course::find($id);
        $course->delete();
        return redirect('admin/courses/list')->with('msg', 'xóa bài viết thành công');
    }
}
