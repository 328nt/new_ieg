@extends('fe/layouts/index')
@section('content')
<!--Banner-->
<div class="container-fluid">
    <div class="row">
        <img src="../fe/fe/image/bg_pageSmall.png">
    </div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
    <h1 class="font36 title_h1">{{$course->title}}</h1>
    <div class="col-md-6 col-md-offset-3">
        <p>{{$course->quote}}
        </p>
    </div>
</div>
<!--/qoutes_END-->

<!--Mục đích-->
{{-- <div class="wpx1470 wdthIMG">
    <div class="row search_relative no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img">
            <img src="../fe/fe/image/toanhoc_01.jpg">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust">
            <h2 class="title_h2">Tất cả khởi nguồn từ một triết lý...</h2>
            <p class="font18">
                Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
                khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và
                đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp
            </p>
        </div>
    </div>
</div> --}}

<!--Đặc điểm khóa học-->
<div class="wpx1470 wdthIMG dacdiemKH">
    <h1 class="title_block font36 mt_80 mb_80">{{$course->header_course}}</h1>
    <div class="row search_relative no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img pull-right">
            <img src="upload/courses/{{$course->sec1_image}}">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust absoluteLeft_0">
            <h2 class="title_h2"><img src="../fe/fe/image/ico_01.png" />{{$course->sec1_title}}</h2>
            <p class="font18">
                {{$course->sec1_content}}
            </p>
        </div>
    </div>
    <div class="row search_relative mt_160 no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img">
            <img src="upload/courses/{{$course->sec2_image}}">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust">
            <h2 class="title_h2"><img src="../fe/fe/image/ico_02.png" />{{$course->sec2_title}}</h2>
            <p class="font18">
                {{$course->sec2_content}}
            </p>
        </div>
    </div>
    <div class="row search_relative mt_160 mb_100 no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img pull-right">
            <img src="upload/courses/{{$course->sec3_image}}">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust absoluteLeft_0">
            <h2 class="title_h2"><img src="../fe/fe/image/ico_03.png" />{{$course->sec3_title}}</h2>
            <p class="font18">
                {{$course->sec3_content}}
            </p>
        </div>
    </div>
</div>
<!--/Đặc điểm khóa học_END-->

<!--Lộ trình học-->
<div class="wpx1470 lotrinhhoc">
    <h1 class="title_block font36 mt_80 mb_80">IEG đồng hành cùng con như thế nào</h1>
    <div class="flex-box">
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 4 đến 5 tuổi</a></p>

        </div>
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 6 đến 9 tuổi</a></p>

        </div>
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 9 đến 12 tuổi</a></p>

        </div>
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 13 đến 15 tuổi</a></p>

        </div>
    </div>
</div>
<!--/Lộ trình học_END-->
<h1 class="title_block">những khoảnh khắc bật sáng</h1>
<div class="container">
    <div class="wpx1470 block_03">
        <div class="owl-carousel owl-theme sliderInfo cust_btn_nextPrev">
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_01.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_02.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_03.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_02.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_01.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
        </div>
        <p><a class="btn_boxMore" href="#">Xem thêm...</a></p>
    </div>
</div>
<!--block_04-->
<div class="container wpx920_width100vh">
    <div class="row">
        <div class="wpx1470 block_04 bg_vang">
            <h4 class="text-center font24 colorBleck">Đăng ký tham gia kiểm tra năng lực</h4>
            <form class="row" action="{{route('contact_test')}}" method="POST">
                {{ csrf_field() }}
                <div class="col-md-12 flex-box form-flex mb_30">
                    <div class="col-4-cust">
                        <input type="text" name="fullname" required placeholder="Họ và tên...">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="phone" required placeholder="Số điện thoại">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="email" required placeholder="Email...">
                    </div>
                </div>
                <div class="col-md-12 flex-box form-flex mb_30">
                    <div class="col-4-cust">
                        <input type="text" name="student_name" required placeholder="Họ và tên học sinh...">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="dob" required placeholder="Năm sinh học sinh">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="center" required placeholder="Trung tâm...">
                    </div>
                </div>
                <div class="col-md-12 flex-box form-flex">
                    <div class="col-8-cust">
                        <textarea placeholder="content" required name="text"></textarea>
                    </div>
                    <div class="col-4-cust flex-box flext-wrapRev">
                        <button class="btn_block04 bg_xanhnuocbien" type="reset">Reset</button>
                        <button class="btn_block04 bg_xanhnuocbien" type="submit">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--/block_04_END-->
@include('fe/pages/form_course')
<!--block_05-->
<div class="container-fluid">
    <h1 class="title_block">ĐỐi tác và đồng hành</h1>
    <div class="row block_05 mb_0">
        <div class="wpx1470">
            <div class="wpx1200">
                <div class="owl-carousel owl-theme slider_doitac cust_btn_nextPrev">
                    <div class="item"><img src="../fe/fe/image/logo_dt_01.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_02.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_01.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_02.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_01.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_02.png"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/block_05_END-->

@endsection