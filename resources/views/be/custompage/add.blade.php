@extends('be.layouts.index')
@section('title')
add 
@endsection
@section('content')
@include('msg')
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">
                        {{$customs->store_ieg['title']}}</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <form action="{{route('add_config')}}" method="post" enctype="multipart/form-data"
                        class="form-horizontal" id="form-sample-1" novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">title</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="title" type="text" placeholder="title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">quote</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="quote" type="text" placeholder="quote">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sec-1-title</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="sec1-title" type="text" placeholder="title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sec-1-content</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="sec1-content" type="text" placeholder="quote">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sec-2-title</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="sec2-title" type="text" placeholder="title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sec-2-content</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="sec2-content" type="text" placeholder="quote">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Sec-3-title</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="sec3-title" type="text" placeholder="title">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">Sec-3-content</label>
                                <textarea name="sec3_content" id="" cols="80%" rows="5"></textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Image</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="image" type="file">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 ml-sm-auto">
                                <button class="btn btn-info" type="submit">Submit</button>
                            </div>
                        </div>
                        {{$customs->store_ieg['quote']}}
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection