<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Congratulation</title>

    <style>
        body {
            font-family: "SFU Futura", Helvetica, Arial, sans-serif;
            word-spacing: 1px;
            text-align: left;
            font-size: 20px;
            color: black;
            line-height: 22pt
        }

        i {
            font-size: 24px;
        }

        /* p {
            font-size: 16px;
        } */

        .red {
            color: red;
            font-style: italic;
        }

        ul {
            list-style-type: none;
        }

        img {
            width: 100%;
        }

        .flex {
            display: flex;
        }

        .orange {
            color: orange;
        }

        .main-color {
            color: #00a6e2;
        }

        .title {
            text-align: left;
            font-family: none;
        }

        .align-center {
            text-align: center;
        }

        .align-left {
            text-align: left;
        }

        .header-font-size {
            font-size: 24px;
        }

        .default-font-size {
            font-size: 20px;
        }

        #img-content {
            padding-left: 4%;
        }
    </style>
</head>

<body>
    <div class="flex" style="width: 100%">
        <div style="width: 80%;" style="text-align: left">
            <div class="flex">
                <div style="width: 5%"></div>
                <div style="width: 90%">
                    <div>
                        <div>
                            <i class="title main-color ">
                                Ban tổ chức HỌC TOÁN ONLINE KANGAROO !
                            </i>
                        </div>
                        <div class="content default-font-size">
                            <p class="default-font-size">
                                Kính gửi quý phụ huynh và thí sinh, <br>
                            </p>
                            <p >
                                Ban quản trị (BQT) website học Toán online Kangaroo xin trân
                                trọng cảm ơn sự ủng hộ của Quý phụ huynh và học sinh trong thời gian vừa qua. <br>
                            </p>
                            <p>
                                Như Quý phụ huynh đã biết, tình hình dịch bệnh ncovid-19 đang diễn biến rất phức tạp. Để
                                đảm bảo an toàn cho học sinh, cũng như ngăn dịch bệnh lây lan nhanh chóng, Bộ Giáo dục
                                đã cho học sinh nghỉ học trong thời gian dài. Trong đó, hình thức học trực tuyến đang là kênh học tập hữu hiệu để giúp các em học sinh duy trì và củng cố kiến thức.
                            </p>
                            <p>
                                Theo khuyến cáo của trung tâm an ninh mạng, để đảm bảo an toàn bảo mật thông tin của
                                người dùng, các hệ thống lớp học online cần tiến hành ra soát, hiệu chỉnh kỹ thuật để
                                tránh bị tấn công.
                            </p>
                        </div>
                        <div class=" default-font-size">
                            Trong những ngày vừa qua, website học Toán online  <a
                                href="kangaroo-math.vn" target="_blank">kangaroo-math.vn</a> đã tiến hành bảo trì và nâng cấp hệ thống để đáp ứng nhu cầu học tăng cao của học sinh. <br>
                        </div>
                        <div class=" default-font-size">
                            Vì vậy, BQT kính đề nghị tất cả Quý phụ huynh tiến thành thay đổi mật khẩu đăng nhập trong
                            vòng 24 giờ kể từ khi nhận được thông báo này theo hướng dẫn sau hoặc video đính kèm bên
                            dưới:<br>
                        </div>
                        <div class=" default-font-size">
                            <p class=" default-font-size"><b>Bước 1:</b> Truy cập vào website <a href="kangaroo-math.vn"
                                    target="_blank">kangaroo-math.vn</a></p>
                            <p class=" default-font-size"><b>Bước 2:</b> Ấn vào avatar của học sinh (ô trên cùng bên
                                phải, cạnh tên học sinh). Chọn
                                mục Thay đổi mật khẩu.</p>
                            <p class=" default-font-size"><b>Bước 3:</b> Điền đầy đủ mật khẩu cũ (mật khẩu đang sử dụng)
                                và nhập mật khẩu mới.</p>
                            <p class="red default-font-size"><b>Chú ý: Mật khẩu mới có ít nhất 8 ký tự, phải chứa chữ
                                    cái, số và ký tự đặc biệt ( ví dụ: !, @, #, $, %, ^, &, *).</b> </p>
                        </div>
                        <div>
                            <p class="default-font-size">Video hướng dẫn đổi mật khẩu <a
                                    href="https://www.youtube.com/watch?v=3raR_aJbmFQ" target="_blank">tại đây</a> </p>
                        </div>
                        <div>
                            <p class=" default-font-size">BQT xin chân thành cảm ơn sự hợp tác của Quý phụ huynh. <br>
                            </p>
                            <p class=" default-font-size">
                                Trân trọng</p>
                        </div>
                        <hr>
                        <div class="default-font-size">
                            <p><b class="orange">INTERNATIONAL KANGAROO MATH CONTEST</b></p>
                            <p>Tel: (+84-4) 7109 1099</p>
                            <p>Hotline: 098 104 8228 - 0936 255 598</p>
                            <p>Facebook: IKMCVietnam</p>
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div>
                <img src="https://lh3.googleusercontent.com/-stCwiLgaBwQ/XaPgShlBCTI/AAAAAAAAAsE/BXdFu7b5LWEdRYcT2ZH2TSTGZRIEcgBggCK8BGAsYHg/s0/kmc%2Bsu%25CC%259B%25CC%2589a-08.png"
                    alt="">
            </div> --}}
        </div>
    </div>
</body>

</html>