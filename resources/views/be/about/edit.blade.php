@extends('be.layouts.index')
@section('title')
Thông tin công ty
@endsection
@section('content')
@include('msg')
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Thông tin công ty</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <form action="{{route('update_about', $about->id)}}" method="post" enctype="multipart/form-data"
                        class="form-horizontal" id="form-sample-1" novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-sm-5">
                                <label class="col-sm-2 col-form-label">Logo1</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="logo1" type="file">
                                </div>
                                <img width="300px" src="upload/about/{{$about->logo1}}" alt="">
                            </div>
                            <div class="col-sm-5">
                                <label class="col-sm-2 col-form-label">Logo2</label>
                                <div class="col-sm-8">
                                    <input class="form-control" name="logo2" type="file">
                                </div>
                                <img width="300px" src="upload/about/{{$about->logo2}}" alt="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">company_name</label>
                                <textarea name="company_name" id="" cols="80%"
                                    rows="5">{{$about->company_name}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">short_name</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="short_name" value="{{$about->short_name}}"
                                        type="text" placeholder="short_name">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">address1</label>
                                <textarea name="address1" id="" cols="80%" rows="5">{{$about->address1}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">hotline1</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="hotline1" value="{{$about->hotline1}}" type="text"
                                        placeholder="hotline1">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">phone1</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="phone1" value="{{$about->phone1}}" type="text"
                                        placeholder="phone1">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">address2</label>
                                <textarea name="address2" id="" cols="80%" rows="5">{{$about->address2}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">hotline2</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="hotline2" value="{{$about->hotline2}}" type="text"
                                        placeholder="hotline2">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">phone2</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="phone2" value="{{$about->phone2}}" type="text"
                                        placeholder="phone2">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">inieg_title</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="inieg_title" value="{{$about->inieg_title}}"
                                        type="text" placeholder="inieg_title">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">inieg_content</label>
                                <textarea name="inieg_content" id="" cols="80%"
                                    rows="5">{{$about->inieg_content}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">inieg_link</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="inieg_link" value="{{$about->inieg_link}}"
                                        type="text" placeholder="inieg_link">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">statistical info</label>
                                <div class="col-sm-5">
                                    <label class="col-sm-2 col-form-label">num1</label>
                                    <input class="form-control" name="num1" value="{{$about->statistical['num1']}}" type="text"
                                        placeholder="num1">
                                    <label class="col-sm-2 col-form-label">num2</label>
                                    <input class="form-control" name="num2" value="{{$about->statistical['num2']}}" type="text"
                                        placeholder="num2">
                                    <label class="col-sm-2 col-form-label">num3</label>
                                    <input class="form-control" name="num3" value="{{$about->statistical['num3']}}" type="text"
                                        placeholder="num3">
                                    <label class="col-sm-2 col-form-label">num4</label>
                                    <input class="form-control" name="num4" value="{{$about->statistical['num4']}}" type="text"
                                        placeholder="num4">
                                    <label class="col-sm-2 col-form-label">num5</label>
                                    <input class="form-control" name="num5" value="{{$about->statistical['num5']}}" type="text"
                                        placeholder="num5">
                                    <label class="col-sm-2 col-form-label">num6</label>
                                    <input class="form-control" name="num6" value="{{$about->statistical['num6']}}" type="text"
                                        placeholder="num6">
                                </div>
                                <div class="col-sm-5">
                                    <label class="col-sm-2 col-form-label">title1</label>
                                    <input class="form-control" name="title1" value="{{$about->statistical['title1']}}" type="text"
                                        placeholder="title1">
                                    <label class="col-sm-2 col-form-label">title2</label>
                                    <input class="form-control" name="title2" value="{{$about->statistical['title2']}}" type="text"
                                        placeholder="title2">
                                    <label class="col-sm-2 col-form-label">title3</label>
                                    <input class="form-control" name="title3" value="{{$about->statistical['title3']}}" type="text"
                                        placeholder="title3">
                                    <label class="col-sm-2 col-form-label">title4</label>
                                    <input class="form-control" name="title4" value="{{$about->statistical['title4']}}" type="text"
                                        placeholder="title4">
                                    <label class="col-sm-2 col-form-label">title5</label>
                                    <input class="form-control" name="title5" value="{{$about->statistical['title5']}}" type="text"
                                        placeholder="title5">
                                    <label class="col-sm-2 col-form-label">title6</label>
                                    <input class="form-control" name="title6" value="{{$about->statistical['title6']}}" type="text"
                                        placeholder="title6">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">statistical_link</label>
                                <div class="col-sm-10">
                                    <input class="form-control" name="statistical_link" value="{{$about->statistical['statistical_link']}}"
                                        type="text" placeholder="statistical_link">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 ml-sm-auto">
                                <button class="btn btn-info" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection