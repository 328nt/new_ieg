<?php

namespace App\Http\Controllers;

use App\About;
use Illuminate\Http\Request;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function show(About $about)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $about = About::all()->first();
        return view('be/about/edit', compact('about'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $about = About::find($id);
        $about->company_name = $rq->company_name;
        $about->short_name = $rq->short_name;
        $about->address1 = $rq->address1;
        $about->hotline1 = $rq->hotline1;
        $about->phone1 = $rq->phone1;
        $about->address2 = $rq->address2;
        $about->hotline2 = $rq->hotline2;
        $about->phone2 = $rq->phone2;
        $about->inieg_title = $rq->inieg_title;
        $about->inieg_content = $rq->inieg_content;
        $about->inieg_link = $rq->inieg_link;
        $about->statistical = [
            'num1'=>$rq->num1,
            'title1'=>$rq->title1,
            'num2'=>$rq->num2,
            'title2'=>$rq->title2,
            'num3'=>$rq->num3,
            'title3'=>$rq->title3,
            'num4'=>$rq->num4,
            'title4'=>$rq->title4,
            'num5'=>$rq->num5,
            'title5'=>$rq->title5,
            'num6'=>$rq->num6,
            'title6'=>$rq->title6,
            'statistical_link'=>$rq->statistical_link,
        ];
        // dd($about->statistical);
        if ($rq->HasFile('logo1')) {
            $file = $rq->file('logo1');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/about/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/about", $img);
            // unlink("upload/about/".$about->image);
            $about->logo1 = $img;
        }
        if ($rq->HasFile('logo2')) {
            $file = $rq->file('logo2');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/about/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/about", $img);
            // unlink("upload/about/".$about->image);
            $about->logo2 = $img;
        }
        $about->save();
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\About  $about
     * @return \Illuminate\Http\Response
     */
    public function destroy(About $about)
    {
        //
    }
}
