@extends('be.layouts.index')
@section('title')
Danh sách slide
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Danh sách slide</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Alt</th>
                        <th>Location</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($slides as $slide)
                    <tr>
                        <td>{{$slide->id}}</td>
                        <td><img width="100px" src="upload/slide/{{$slide->image}}" alt=""></td>
                        <td>{{$slide->name}}</td>
                        <td>{{$slide->alt}}</td>
                        <td>
                            @if ($slide->location == 1)
                            trang chủ
                            @elseif($slide->location == 2)
                            dạy và học
                            @endif</td>
                        {{-- <td>{{$slide->category->name}}</td> --}}
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a
                                href="admin/slides/edit/{{$slide->id}}">Edit</a> <br>
                            <i class="fa fa-trash-o  fa-fw"></i><a href="admin/slides/delete/{{$slide->id}}"> Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            //"ajax": './assets/demo/data/table_data.json',
            /*"columns": [
                { "data": "name" },
                { "data": "office" },
                { "data": "extn" },
                { "data": "start_date" },
                { "data": "salary" }
            ]*/
        });
    })
</script>
@endsection