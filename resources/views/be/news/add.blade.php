@extends('be.layouts.index')
@section('title')
add
@endsection
@section('content')
@include('msg')
<div class="col-md-10 col-md-offset-1">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Thêm Tin tức</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <form action="{{route('add_news')}}" method="post" enctype="multipart/form-data" class="form-horizontal"
                id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Category</label>
                    <div class="col-sm-10">
                        <select name="id_category" class="form-control select2_demo_1" style="height: 34px;">
                            @foreach ($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}} </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="title" type="text" placeholder="Title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">description</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="description" type="text" placeholder="Description">
                    </div>
                </div>

                <div class="form-group">
                    <label>Content</label>
                    <textarea id="summernote" rows="5" name="content">content</textarea>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Image (4x3)</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="image" type="file">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">highlight</label>
                    <div class="col-sm-10">
                        <input name="hightlight" type="checkbox" name="" value="1" id="">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-info" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection