@extends('fe/layouts/index')
@section('content')


<div class="block_brand_story_4">
  <div class="container">
    <div class="row">
      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
        <h2 class="section_heading line_after_heading block_brand_story_title line_after_heading_section">NHỮNG NGƯỜI
          LAN TOẢ GIÁ TRỊ GIÁO DỤC</h2>
        <div class="list_image_human row clearfix">
          <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
            <div class="human_item full_img">
              @foreach ($kotfs as $kotf)
              @if ($kotf->layouts == 0)
              <a class="p-b-30" data-fancybox="gallery" href="#human{{$kotf->id}}"><img
                  src="upload/kotf/{{$kotf->image1}}" alt="human {{$kotf->id}}"></a>
              @endif
              @endforeach
            </div>
            <div class="human_item half_img_left p-r-30">
              @foreach ($kotfs as $kotf)
              @if ($kotf->layouts == 1)
              <a class="p-b-30" data-fancybox="gallery" href="#human{{$kotf->id}}"><img
                  src="upload/kotf/{{$kotf->image1}}" alt="human {{$kotf->id}}"></a>
              @endif
              @endforeach
            </div>
            <div class="human_item half_img_right">
              @foreach ($kotfs as $kotf)
              @if ($kotf->layouts == 2)
              <a class="p-b-30" data-fancybox="gallery" href="#human{{$kotf->id}}"><img
                  src="upload/kotf/{{$kotf->image1}}" alt="human {{$kotf->id}}"></a>
              @endif
              @endforeach
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <div class="human_item">
              @foreach ($kotfs as $kotf)
              @if ($kotf->layouts == 3)
              <a class="p-b-30" data-fancybox="gallery" href="#human{{$kotf->id}}"><img
                  src="upload/kotf/{{$kotf->image1}}" alt="human {{$kotf->id}}"></a>
              @endif
              @endforeach
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-6">
            <div class="human_item">
              @foreach ($kotfs as $kotf)
              @if ($kotf->layouts == 4)
              <a class="p-b-30" data-fancybox="gallery" href="#human{{$kotf->id}}"><img
                  src="upload/kotf/{{$kotf->image1}}" alt="human {{$kotf->id}}"></a>
              @endif
              @endforeach
            </div>
          </div>
          <div class="clearfix"></div>
        </div>
        @foreach ($kotfs as $kotf)
        <div class="clearfix human_wrap" id="human{{$kotf->id}}" style="display: none;">
          <div class="human_img" style="background-image:url(upload/kotf/{{$kotf->image2}})"></div>
          <div class="human_content">
            <h3 class="human_name section_heading line_after_heading">{{$kotf->fullname}}<br>{{$kotf->position}}<p
                class="human_job">{{$kotf->quote}}</p>
            </h3>
            <div class="human_des">{!!$kotf->content!!}
            </div>
          </div>
        </div>
        @endforeach
      </div>
    </div>
  </div>
</div>


@endsection