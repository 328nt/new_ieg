<?php

namespace App\Http\Controllers;

use App\Customskotf;
use Illuminate\Http\Request;

class CustomskotfController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kotfs = Customskotf::all();
        return view('be.custom.list', compact('kotfs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be.custom.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $kotf = new Customskotf();
        
        $kotf->fullname = $rq->fullname;
        $kotf->position = $rq->position;
        $kotf->quote = $rq->quote;
        $kotf->content = $rq->content;
        if ($rq->hasFile('image1')) {
            $file = $rq->file('image1');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/kotf/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/kotf", $img);
            $kotf->image1 = $img;
        } else {
            $kotf->image1 = "";
        }
        if ($rq->hasFile('image2')) {
            $file = $rq->file('image2');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/kotf/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/kotf", $img);
            $kotf->image2 = $img;
        } else {
            $kotf->image2 = "";
        }
        $kotf->layouts = $rq->layouts;
        // dd($kotf);
        $kotf->save();
        return redirect('admin/customs/list')->with('msg','thêm image thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Customskotf  $customskotf
     * @return \Illuminate\Http\Response
     */
    public function show(Customskotf $customskotf)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Customskotf  $customskotf
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kotf = Customskotf::find($id);
        return view('be.custom.edit', compact('kotf'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Customskotf  $customskotf
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[

        ],[

        ]);
        
        $kotf = Customskotf::find($id);
        
        $kotf->fullname = $rq->fullname;
        $kotf->position = $rq->position;
        $kotf->quote = $rq->quote;
        $kotf->content = $rq->content;
        if ($rq->hasFile('image1')) {
            $file = $rq->file('image1');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/kotf/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/kotf", $img);
            unlink("upload/kotf/".$kotf->image1);
            $kotf->image1 = $img;
        }
        if ($rq->hasFile('image2')) {
            $file = $rq->file('image2');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/kotf/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/kotf", $img);
            unlink("upload/kotf/".$kotf->image2);
            $kotf->image2 = $img;
        }
        $kotf->layouts = $rq->layouts;
        $kotf->save();
        return redirect('admin/customs/list')->with('msg','okla');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Customskotf  $customskotf
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $news = Customskotf::find($id);
        $news->delete();
        return redirect('admin/customs/list')->with('msg', 'xóa người giữ lửa thành công');
    }
}
