@extends('fe/layouts/index')
@section('title')
{{$video->title}}
@endsection
@section('content')

<!--Banner-->
<div class="container-fluid">
    <div class="row"> <img src="fe/image/bg_pageSmall_2.png"> </div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
    <h1 class="title_block font36">ieg tv</h1>
</div>

<!--Tôi và IEG-->
<div class="newrela wpx1470 mb_30"> <a href="" class="mb_20 block_display">
        <div class="wimg100">
            <iframe class="videos" src="https://www.youtube.com/embed/{{substr($video->link,-11)}}"></iframe>
        </div>
    </a>
    <h2 class="">
        <p>{{$video->title}}</p>
    </h2>
    <p class="mt_40"><span class="time_news">{{date("d/m/Y", strtotime($video->created_at))}}</span></p>
    <p class="font18">{!!$video->content!!}</p>
</div>
<div class="tinnoibat wpx1470 mt_40">
    <h2 class="title_tintuc font24">Video liên quan</h2>
    <div class="toivaieg flex-box">

        @foreach ($videos as $video)
        <div class="col-4-cust">
            <div class="block_display">
                <a href="/ieg-tv/video/{{$video->id}}.html">
                    <p>
                        <img src="https://img.youtube.com/vi/{{substr($video->link,-11)}}/0.jpg" alt=""><span
                            class="icon-play2"><i class="fas fa-play-circle"></i></span></p>
                    <p class="title_news font18">{{$video->title}}</p>
                    <p class="mt_20"><span>{{date("d/m/Y", strtotime($video->created_at))}}</span></p>
                </a>
            </div>
        </div>
        @endforeach
    </div>
    <!--/Tôi và IEG_END-->
</div>
@endsection