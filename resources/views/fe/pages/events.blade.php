@extends('fe/layouts/index')
@section('content')



<div class="container text-center mt_40 mb_40">
  <h1 class="title_block font36">sự kiện</h1>
</div>
<div class="qoutes wpx1470 mt_100">
  <p class="qoutes_content"> Tại IEG, tất các các thông tin quan trọng cả bên trong & bên ngoài nhà trường được tổng hợp
    một các có hệ thống để việc lựa chọn luồng thông tin cho quá trình học tập trở nên thuận lợi hơn. </p>
</div>
<div class="container">
	@foreach ($events as $event)
	<p class="mt_40"><a target="_blank" href="{{$event->link}}"><img src="upload/news/{{$event->image}}"></a></p>
	@endforeach
</div>

<!--Khóa học-->
    @include('fe/pages/form_course')
<!--/Khóa học_END-->
@endsection