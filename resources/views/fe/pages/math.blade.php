@extends('fe/layouts/index')
@section('content')
<!--Banner-->
<div class="container-fluid">
    <div class="row">
        <img src="../fe/fe/image/bg_pageSmall.png">
    </div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
    <h1 class="font36 title_h1">Tất cả khởi nguồn từ một triết lý</h1>
    <div class="col-md-6 col-md-offset-3">
        <p>
            Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
            khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và
            đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp
        </p>
    </div>
</div>
<!--/qoutes_END-->

<!--Mục đích-->
{{-- <div class="wpx1470 wdthIMG">
    <div class="row search_relative no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img">
            <img src="../fe/fe/image/toanhoc_01.jpg">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust">
            <h2 class="title_h2">Tất cả khởi nguồn từ một triết lý...</h2>
            <p class="font18">
                Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
                khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và
                đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp
            </p>
        </div>
    </div>
</div> --}}

<!--Đặc điểm khóa học-->
<div class="wpx1470 wdthIMG dacdiemKH">
    <h1 class="title_block font36 mt_80 mb_80">Điều gì làm nên sự khác biệt tại IEG</h1>
    <div class="row search_relative no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img pull-right">
            <img src="../fe/fe/image/toanhoc_02.jpg">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust absoluteLeft_0">
            <h2 class="title_h2"><img src="../fe/fe/image/ico_01.png" />Đề cập <br /> có hệ thống</h2>
            <p class="font18">
                Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
                khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và
                đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp
            </p>
        </div>
    </div>
    <div class="row search_relative mt_160 no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img">
            <img src="../fe/fe/image/toanhoc_03.jpg">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust">
            <h2 class="title_h2"><img src="../fe/fe/image/ico_02.png" />kích thích <br /> tính sáng tạo</h2>
            <p class="font18">
                Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
                khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và
                đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp
            </p>
        </div>
    </div>
    <div class="row search_relative mt_160 mb_100 no-margin">
        <div class="col-md-7 col-sm-7 col-xs-12 oject_img pull-right">
            <img src="../fe/fe/image/toanhoc_04.jpg">
        </div>
        <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust absoluteLeft_0">
            <h2 class="title_h2"><img src="../fe/fe/image/ico_03.png" />phát triển <br /> tư duy</h2>
            <p class="font18">
                Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
                khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và
                đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp
            </p>
        </div>
    </div>
</div>
<!--/Đặc điểm khóa học_END-->

<!--Lộ trình học-->
<div class="wpx1470 lotrinhhoc">
    <h1 class="title_block font36 mt_80 mb_80">IEG đồng hành cùng con như thế nào</h1>
    <div class="flex-box">
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 4 đến 5 tuổi</a></p>

        </div>
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 6 đến 9 tuổi</a></p>

        </div>
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 9 đến 12 tuổi</a></p>

        </div>
        <div class="col-3-cust font24">
            <div id="ex1" class="modal">
                <p>Thanks for clicking. That felt good.</p>
                <a href="#" rel="modal:close">Close</a>
            </div>

            <!-- Link to open the modal -->
            <p><a href="#ex1" rel="modal:open">Từ 13 đến 15 tuổi</a></p>

        </div>
    </div>
</div>
<!--/Lộ trình học_END-->
<h1 class="title_block">những khoảnh khắc bật sáng</h1>
<div class="container">
    <div class="wpx1470 block_03">
        <div class="owl-carousel owl-theme sliderInfo cust_btn_nextPrev">
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_01.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_02.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_03.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_02.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
            <div class="item">
                <a href="#">
                    <img src="../fe/fe/image/img_01.jpg">
                    <div class="sliderInfo--text">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been
                        the industry's standard dummy text ever since the 1500s
                    </div>
                </a>
                <h5 class="h5--sliderInfo--text">Nguyễn Văn A</h5>
                <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
            </div>
        </div>
        <p><a class="btn_boxMore" href="#">Xem thêm...</a></p>
    </div>
</div>
@include('fe/pages/form_course')
<!--block_05-->
<div class="container-fluid">
    <h1 class="title_block">ĐỐi tác và đồng hành</h1>
    <div class="row block_05 mb_0">
        <div class="wpx1470">
            <div class="wpx1200">
                <div class="owl-carousel owl-theme slider_doitac cust_btn_nextPrev">
                    <div class="item"><img src="../fe/fe/image/logo_dt_01.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_02.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_01.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_02.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_01.png"></div>
                    <div class="item"><img src="../fe/fe/image/logo_dt_02.png"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/block_05_END-->

@endsection