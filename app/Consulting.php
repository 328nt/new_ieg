<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Consulting extends Model
{
    
    public function users()
    {
        return $this->belongsTo('App\User', 'author', 'id');
    }
}
