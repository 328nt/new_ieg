@extends('be.layouts.index')
@section('title')
Danh sách bài viết
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">
    
@include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Tin tức HR</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>image</th>
                        <th>title</th>
                        <th>Description</th>
                        <th>Content</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>id</th>
                        <th>image</th>
                        <th>title</th>
                        <th>Description</th>
                        <th>Content</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
                <tbody>
                    @foreach ($blogs as $blog)
                    <tr>
                        <td>{{$blog->id}}</td>
                        <td><img width="100px" src="upload/blog/{{$blog->image}}" alt=""></td>
                        {{-- <td>{{$blog->category->name}}</td> --}}
                        <td>{{substr($blog->title,0,100)}}</td>
                        <td>{{substr($blog->description,0,100)}} ...</td>
                        <td>{{substr($blog->content,0,100)}} ....</td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a
                                href="admin/blogs/edit/{{$blog->id}}">Edit</a> <br> 
                                <i class="fa fa-trash-o  fa-fw"></i><a
                                href="admin/blogs/delete/{{$blog->id}}"> Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
        "order": [[ 0, "desc" ]],
            //"ajax": './assets/demo/data/table_data.json',
            /*"columns": [
                { "data": "name" },
                { "data": "office" },
                { "data": "extn" },
                { "data": "start_date" },
                { "data": "salary" }
            ]*/
        });
    })
</script>
@endsection