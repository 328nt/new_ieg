@extends('be.layouts.index')
@section('title')
Sửa Khóa học
@endsection
@section('content')
@include('msg')
<div class="col-md-12">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Sửa Khóa học</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <form action="{{route('update_course', $course->id)}}" method="post" enctype="multipart/form-data" class="form-horizontal"
                id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">name</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="name" type="text" value="{{$course->name}}" placeholder="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">icon</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="icon"  type="file">
                    </div>
                    <div class="col-sm-6">
                        <img width="200px" src="upload/courses/{{$course->icon}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="image" type="file">
                    </div>
                    <div class="col-sm-6">
                        <img width="200px" src="upload/courses/{{$course->image}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="title" type="text" value="{{$course->title}}" placeholder="title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">quote</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="quote" type="text" value="{{$course->quote}}" placeholder="quote">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">header_course</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="header_course" type="text" value="{{$course->header_course}}" placeholder="header_course">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec1_title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec1_title" type="text" value="{{$course->sec1_title}}" placeholder="sec1_title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec1_content</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec1_content" type="text" value="{{$course->sec1_content}}" placeholder="sec1_content">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec1_image</label>
                    <div class="col-sm-4">
                        <input class="form-control" name="sec1_image" type="file">
                    </div>
                    <div class="col-sm-6">
                        <img width="300px" src="upload/courses/{{$course->sec1_image}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec2_title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec2_title" type="text" value="{{$course->sec2_title}}" placeholder="sec2_title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec2_content</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec2_content" type="text" value="{{$course->sec2_content}}" placeholder="sec2_content">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec2_image</label>
                    <div class="col-sm-5">
                        <input class="form-control" name="sec2_image" type="file">
                    </div>
                    <div class="col-sm-5">
                        <img width="200px" src="upload/courses/{{$course->sec2_image}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec3_title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec3_title" type="text" value="{{$course->sec3_title}}" placeholder="sec3_title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec3_content</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec3_content" type="text" value="{{$course->sec3_content}}" placeholder="sec3_content">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec3_image</label>
                    <div class="col-sm-5">
                        <input class="form-control" name="sec3_image" type="file">
                    </div>
                    <div class="col-sm-5">
                        <img width="200px" src="upload/courses/{{$course->sec3_image}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">col-md</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="col-md" type="text" value="{{$course->col_md}}" placeholder="col-md">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-info" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<hr>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection