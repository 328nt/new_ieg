<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAboutsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_name')->nullable();
            $table->string('address1')->nullable();
            $table->string('hotline1')->nullable();
            $table->string('phone1')->nullable();
            $table->string('address2')->nullable();
            $table->string('hotline2')->nullable();
            $table->string('phone2')->nullable();
            $table->string('inieg_title')->nullable();
            $table->longText('inieg_content')->nullable();
            $table->string('inieg_link')->nullable()->default('#');
            $table->json('statistical')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
