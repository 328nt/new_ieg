

<!--Khóa học-->
<div class="container wpx920_width100vh">
    <h1 class="title_block">CÁC KHÓA HỌC KHÁC TẠI IEG</h1>
    <div class="row flex-box block_01">
        @foreach ($courses as $item)
        <div class="col-2-cust {{ request()->is('day-va-hoc/'.$item->id.'/*') ? 'hidden' : '' }} text-center">
            <a href="{{route('course', [$item->id, $item->slug_name])}}">
                <img src="upload/courses/{{$item->icon}}">
                <p class="info_KH">{{$item->name}}</p>
            </a>
        </div>
        @endforeach
    </div>
</div>
<!--/Khóa học_END-->