@extends('be.layouts.index')
@section('title')
Thêm khóa học
@endsection
@section('content')
@include('msg')
<div class="col-md-10 col-md-offset-1">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Thêm khóa học</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <form action="{{route('add_course')}}" method="post" enctype="multipart/form-data" class="form-horizontal"
                id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">name</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="name" type="text" placeholder="name">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">icon</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="icon" type="file">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Image</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="image" type="file">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="title" type="text" placeholder="title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">quote</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="quote" type="text" placeholder="quote">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">header_course</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="header_course" type="text" placeholder="header_course">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec1_title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec1_title" type="text" placeholder="sec1_title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec1_content</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec1_content" type="text" placeholder="sec1_content">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec1_image</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec1_image" type="file">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec2_title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec2_title" type="text" placeholder="sec2_title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec2_content</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec2_content" type="text" placeholder="sec2_content">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec2_image</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec2_image" type="file">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec3_title</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec3_title" type="text" placeholder="sec3_title">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec3_content</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec3_content" type="text" placeholder="sec3_content">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">sec3_image</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="sec3_image" type="file">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">col-md</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="col-md" type="text" placeholder="col-md">
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-info" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection