@extends('be.layouts.index')
@section('title')
add new slide
@endsection
@section('content')
@include('msg')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Thêm Tin tức</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <form action="{{route('add_slide')}}" method="post" enctype="multipart/form-data"
                        class="form-horizontal" id="form-sample-1" novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">name</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="name" type="text" placeholder="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">alt</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="alt" type="text" placeholder="alt">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 col-form-label">Content</label>
                            <div class="col-sm-10">
                                <select name="location" id="">
                                    <option value="1">Trang chủ</option>
                                    <option value="2">Dạy và học</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Image</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="image" type="file">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 ml-sm-auto">
                                <button class="btn btn-info" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection