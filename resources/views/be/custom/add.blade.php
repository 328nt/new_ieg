@extends('be.layouts.index')
@section('title')
Thêm ảnh Người giữ lửa
@endsection
@section('content')
@include('msg')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">Thêm Người giữ lửa</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <form action="{{route('add_kotf')}}" method="post" enctype="multipart/form-data"
                        class="form-horizontal" id="form-sample-1" novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Fullname</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="fullname" type="text" placeholder="Tên Đầy đủ">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Position</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="position" type="text" placeholder="Chức vụ">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Trích dẫn</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="quote" type="text" placeholder="Trích dẫn">
                            </div>
                        </div>
                        <div class="form-group">
                            <label>Content</label>
                            <textarea id="summernote" rows="15" name="content" placeholder="content"></textarea>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Ảnh 1</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="image1" type="file">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Ảnh 2</label>
                            <div class="col-sm-10">
                                <input class="form-control" name="image2" type="file">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Vị trí ảnh</label>
                            <div class="col-sm-10">
                                <select name="layouts" id="">
                                    <option value="0">Lớn nửa bên trái</option>
                                    <option value="1">nhỏ bên trái nửa trái</option>
                                    <option value="2">nhỏ bên phải nửa trái</option>
                                    <option value="3">nhỏ giữa</option>
                                    <option value="4">nhỏ phải</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 ml-sm-auto">
                                <button class="btn btn-info" type="submit">Thêm người giữ lửa</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection