-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th5 17, 2020 lúc 06:56 PM
-- Phiên bản máy phục vụ: 10.4.6-MariaDB
-- Phiên bản PHP: 7.2.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `ieg`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `company_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `short_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo1` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `logo2` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotline1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hotline2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inieg_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inieg_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `inieg_link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT '#',
  `statistical` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ;

--
-- Đang đổ dữ liệu cho bảng `abouts`
--

INSERT INTO `abouts` (`id`, `company_name`, `short_name`, `logo1`, `logo2`, `address1`, `hotline1`, `phone1`, `address2`, `hotline2`, `phone2`, `inieg_title`, `inieg_content`, `inieg_link`, `statistical`, `created_at`, `updated_at`) VALUES
(1, 'Học viện phát triển Tư duy và Kỹ năng IEG', 'IEG', 'AzMC-logo.png', 'nhSW-footer_logo.png', '128 Nguyễn Thái Học, Ba Đình, Hà Nội', '0916688208', '(04) 71091099', NULL, NULL, NULL, 'TRIẾT LÝ GIÁO DỤC', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled', 'google.com', '{\"num1\":\"70000\",\"title1\":\"H\\u1ecdc vi\\u00ean\",\"num2\":\"3\",\"title2\":\"H\\u1ecdc vi\\u1ec7n\",\"num3\":\"70000\",\"title3\":\"Th\\u00ed sinh\",\"num4\":\"50\",\"title4\":\"Gi\\u1ea3i th\\u01b0\\u1edfng Olympic\",\"num5\":\"50\",\"title5\":\"Gi\\u1ea3i th\\u01b0\\u1edfng Olympic\",\"num6\":\"70000\",\"title6\":\"Th\\u00ed sinh\",\"statistical_link\":\"#\"}', NULL, '2020-05-17 03:34:00');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `titlenone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hightlight` int(11) NOT NULL DEFAULT 0,
  `author` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `blogs`
--

INSERT INTO `blogs` (`id`, `title`, `titlenone`, `description`, `content`, `image`, `hightlight`, `author`, `created_at`, `updated_at`) VALUES
(2, 'Google inks pact for new 35-storey office', 'google-inks-pact-for-new-35-storey-office', 'That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.', '<p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><hr><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p><br></p>', 'JbLD-imgtamnhin.png', 0, 54, '2020-01-31 20:26:07', '2020-02-21 21:04:33'),
(3, 'Việt Nam đã có 8 ca dương tính với virus Corona', 'viet-nam-da-co-8-ca-duong-tinh-voi-virus-corona', 'Bệnh nhân thứ 8 là nữ giới, 29 tuổi, làm công nhân ở Bình Xuyên, tỉnh Vĩnh Phúc đi cùng chuyến bay với 3 người nhiễm virus Corona vừa được công bố ngày 31/1.', '<p><br></p><p align=\"center\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6; text-align: center;\"><img class=\"news-image initial loading\" alt=\"Việt Nam đã có 8 ca dương tính với virus Corona - 1\" src=\"https://cdn.24h.com.vn/upload/1-2020/images/2020-02-03/1580694231-984-viet-nam-da-co-8-ca-duong-tinh-voi-virus-corona-ava-1580694207-width660height371.jpg\" data-was-processed=\"true\" style=\"font-size: 12px; max-width: 660px;\"></p><p class=\"img_chu_thich_0407\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6; color: rgb(37, 37, 37); font-style: italic; text-align: center; margin-top: -15px;\">Số ca nhiễm&nbsp;<a class=\"TextlinkBaiviet\" href=\"https://www.24h.com.vn/virus-corona-c62e6058.html\" title=\"virus Corona\" style=\"color: rgb(0, 0, 255); line-height: 1.6;\">virus Corona</a>&nbsp;ở Việt Nam đã tăng lên 8 người (ảnh minh họa: NLD)</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Ngày 31/01/2020, đoàn công tác chống dịch của Bộ Y tế đã xuống ổ dịch tại huyện Bình Xuyên, tỉnh Vĩnh Phúc để chỉ đạo, kiểm tra công tác chống dịch. Qua việc kiểm tra, rà soát các hoạt động phòng chống dịch, đoàn công tác và Trung tâm Kiểm soát bệnh tật tỉnh Vĩnh Phúc nhận thấy trường hợp V. H. L là một trong 08 người trở về từ Vũ Hán trên cùng một chuyến bay, trong đó có 03 người đã xác định mắc bệnh viêm đường hô hấp cấp do chủng mới của virus Corona&nbsp;(nCoV), đang cách ly và điều trị tại Bệnh viện Bệnh nhiệt đới Trung ương và Khoa nhiệt đới, Bệnh viện đa khoa tỉnh Thanh Hoá.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Đây là một trường hợp tiếp xúc gần với các ca bệnh xác định có nguy cơ cao. Với sự cảnh giác cần thiết, đoàn công tác của Bộ Y tế đã chỉ định lấy mẫu bệnh phẩm gửi Viện Vệ sinh Dịch tễ Trung ương xét nghiệm xác định tác nhân và chỉ đạo đưa ngay trường hợp này vào cách ly tại cơ sở y tế cùng ngày 31/01/2020.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Hiện bệnh nhân đang được cách ly tại Bệnh viện Bệnh nhiệt đới Trung ương cơ sở Đông Anh và đang trong tình trạng ổn định</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Theo báo cáo của hệ thống giám sát bệnh truyền nhiễm Bộ Y tế, đến 07 giờ 30, ngày 3/2/2020, tình hình dịch bệnh viêm đường hô hấp do chủng mới của virus&nbsp;<a class=\"TextlinkBaiviet\" href=\"https://www.24h.com.vn/virus-corona-c62e6058.html\" title=\"Corona\" style=\"color: rgb(0, 0, 255); line-height: 1.6;\">Corona</a>&nbsp;(nCoV) trên thế giới cụ thể như sau:</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">- Tổng số trường hợp mắc: 17.387 ,trong đó tại lục địa Trung Quốc: 17.205</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">- Tổng số trường hợp tử vong: 362, trong đó tại lục địa Trung Quốc: 361, tại Philippine: 01</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">- Tổng số trường hợp mắc bên ngoài lục địa Trung Quốc: 182.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">- 26 quốc gia, vùng lãnh thổ (bên ngoài lục địa Trung Quốc) ghi nhận trường hợp mắc như sau:</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">1. Nhật Bản: 20 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">2. Thái Lan: 19 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">3. Singapore: 18 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">4. Hàn Quốc: 15 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">5. Hồng Kông (TQ): 14 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">6. Úc: 12 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">7. Đài Loan (TQ): 10 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">8. Đức: 10 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">9. Mỹ: 9 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">10. Malaysia: 8 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">11. Ma Cao (TQ): 8 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">12. Việt Nam: 8 trường hợp</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">13. Pháp: 6 trường hợp</p>', 'Plzk-imgsumenh.png', 0, 54, '2020-02-02 20:57:58', '2020-02-21 21:04:52'),
(4, 'Xếp hàng mua vàng giữa dịch Corona: Đến \"Thần Tài\" cũng phải bịt khẩu trang', 'xep-hang-mua-vang-giua-dich-corona-den-than-tai-cung-phai-bit-khau-trang', 'Sáng 3/2, ngày vía Thần Tài (tức ngày 10/1 Âm lịch), phố Trần Nhân Tông (quận Hai Bà Trưng) - nơi tập trung nhiều cửa hàng của doanh nghiệp kinh doanh vàng lớn, người dân đến từ rất sớm chờ mua vàng cầu may.', '<p><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\">Sáng&nbsp;3/2, ngày vía Thần Tài (tức ngày 10/1 Âm lịch), phố Trần Nhân Tông (quận Hai Bà Trưng) - nơi tập trung nhiều cửa hàng của doanh nghiệp kinh doanh vàng lớn, người dân đến từ rất sớm chờ mua vàng cầu may.</span></p><p><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\">Sáng&nbsp;3/2, ngày vía Thần Tài (tức ngày 10/1 Âm lịch), phố Trần Nhân Tông (quận Hai Bà Trưng) - nơi tập trung nhiều cửa hàng của doanh nghiệp kinh doanh vàng lớn, người dân đến từ rất sớm chờ mua vàng cầu may.</span></p><hr><p><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\">Sáng&nbsp;3/2, ngày vía Thần Tài (tức ngày 10/1 Âm lịch), phố Trần Nhân Tông (quận Hai Bà Trưng) - nơi tập trung nhiều cửa hàng của doanh nghiệp kinh doanh vàng lớn, người dân đến từ rất sớm chờ mua vàng cầu may.</span></p><hr><p><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\">Sáng&nbsp;3/2, ngày vía Thần Tài (tức ngày 10/1 Âm lịch), phố Trần Nhân Tông (quận Hai Bà Trưng) - nơi tập trung nhiều cửa hàng của doanh nghiệp kinh doanh vàng lớn, người dân đến từ rất sớm chờ mua vàng cầu may.</span></p><p><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\">Sáng&nbsp;3/2, ngày vía Thần Tài (tức ngày 10/1 Âm lịch), phố Trần Nhân Tông (quận Hai Bà Trưng) - nơi tập trung nhiều cửa hàng của doanh nghiệp kinh doanh vàng lớn, người dân đến từ rất sớm chờ mua vàng cầu may.</span></p><p><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\">Sáng&nbsp;3/2, ngày vía Thần Tài (tức ngày 10/1 Âm lịch), phố Trần Nhân Tông (quận Hai Bà Trưng) - nơi tập trung nhiều cửa hàng của doanh nghiệp kinh doanh vàng lớn, người dân đến từ rất sớm chờ mua vàng cầu may.</span></p><p><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\"><br></span><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\"><br></span><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\"><br></span><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\"><br></span><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\"><br></span><span style=\"color: rgb(255, 255, 255); font-family: Arial, Helvetica, sans-serif; font-size: 15px; background-color: rgb(26, 30, 43);\"><br></span><br></p>', 'z5WQ-2019-11-27.png', 0, 54, '2020-02-02 20:59:51', '2020-02-02 20:59:51'),
(5, 'Trung Quốc xây xong bệnh viện 1.000 giường bệnh trong 10 ngày', 'trung-quoc-xay-xong-benh-vien-1000-giuong-benh-trong-10-ngay', 'Trung Quốc đã chính thức xây xong bệnh viện dã chiến với 1.000 giường bệnh vào ngày 2.2, đánh dấu 10 ngày các công nhân xây dựng làm việc không ngừng nghỉ.', '<p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Theo Business Insider,&nbsp;<a class=\"TextlinkBaiviet\" href=\"https://www.24h.com.vn/tin-tuc-trung-quoc-c415e3782.html\" title=\"Trung Quốc\" style=\"color: rgb(0, 0, 255); line-height: 1.6;\">Trung Quốc</a>&nbsp;bắt đầu xây bệnh viện Hỏa Thần Sơn vào ngày 23.1. Viên gạch cuối cùng được đặt đúng vị trí vào sáng ngày 2.2, đánh dấu toàn bộ quá trình xây dựng đã hoàn tất, theo CGTN.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Bệnh viện dã chiến với 1.000 giường bệnh đã được bàn giao cho đội ngũ y bác sĩ và bắt đầu tiếp nhận bệnh nhân từ ngày 3.2, theo Thời báo Hoàn Cầu.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Bệnh viện Hỏa Thần Sơn được xây dựng tại khu đất rộng 25.000m2. 1.400 nhân viên y tế sẽ làm việc tại bệnh viện mới ở tỉnh Hồ Bắc.</p><p align=\"center\" class=\"img_chu_thich_0407\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6; color: rgb(37, 37, 37); font-style: italic; text-align: center; margin-top: -15px;\"><img class=\"news-image initial loading\" alt=\"Trung Quốc xây xong bệnh viện 1.000 giường bệnh trong 10 ngày - 1\" src=\"https://cdn.24h.com.vn/upload/1-2020/images/2020-02-03/1580696733-653-trung-quoc-xay-xong-benh-vien-1000-giuong-benh-trong-10-ngay-benh-vien-1-1580693527-width1024height683.jpg\" width=\"660\" data-was-processed=\"true\" style=\"font-size: 12px; max-width: 660px;\"></p><p class=\"img_chu_thich_0407\" style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6; color: rgb(37, 37, 37); font-style: italic; text-align: center; margin-top: -15px;\">Bệnh viện Hỏa Thần Sơn được xây xong chỉ sau 10 ngày.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Đoạn video tua nhanh từ lúc bắt đầu xây dựng đến lúc xong cho thấy một khu vực rộng lớn được dọn dẹp gần như ngay lập tức. Các phòng với giường bệnh xuất hiện gần như ngay lập tức trong đêm.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Tính đến ngày 2.2, Trung Quốc ghi nhận 360 trường hợp tử vong vì dịch bệnh&nbsp;<a class=\"TextlinkBaiviet\" href=\"https://www.24h.com.vn/virus-corona-c62e6058.html\" title=\"virus Corona\" style=\"color: rgb(0, 0, 255); line-height: 1.6;\">virus Corona</a>. Nhiều thành phố ở Trung Quốc bị phong tỏa, bao gồm cả Vũ Hán – thành phố nằm ở tâm dịch.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Trung Quốc hiện đang xây bệnh viện dã chiến thứ hai, nằm cách bệnh viện Hỏa Thần Sơn khoảng 40km. Bệnh viện Lôi Thần Sơn có sức chứa 1.600 giường bệnh, dự kiến đi vào hoạt động trong ngày 5.2, theo CGTN.</p>', 'f6pg-2.png', 0, 54, '2020-02-02 21:01:28', '2020-02-02 21:01:28'),
(6, 'Nguyễn Văn Xyz', 'nguyen-van-xyz', 'Đây là tóm tắt', '<p>xcvxcbvxbdfbrdfh ehrth rth rth gh rthrt heth&nbsp;</p><p>rth</p><p>rthrt</p><p>&nbsp;rth</p><p>&nbsp;trh trh rt</p><p>h rth&nbsp;</p>', 'GONr-img_02.jpg', 0, 54, '2020-02-21 21:04:20', '2020-02-21 21:04:20');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'News', NULL, NULL),
(2, 'Events', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `comments`
--

CREATE TABLE `comments` (
  `id` int(10) UNSIGNED NOT NULL,
  `idUser` int(10) UNSIGNED NOT NULL,
  `idNews` int(10) UNSIGNED DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `comment_video` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `id_video` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `comments`
--

INSERT INTO `comments` (`id`, `idUser`, `idNews`, `content`, `comment_video`, `created_at`, `updated_at`, `id_video`) VALUES
(1, 54, 6, 'retergergher', NULL, '2019-09-22 21:17:51', '2019-09-22 21:17:51', NULL),
(2, 54, 6, 'ergher hrth rth', NULL, '2019-09-22 21:17:57', '2019-09-22 21:17:57', NULL),
(3, 54, 5, 'greger thrt hrth', NULL, '2019-09-22 21:18:20', '2019-09-22 21:18:20', NULL),
(4, 54, 7, 'fsdfdsfdsf', NULL, '2019-09-23 00:09:54', '2019-09-23 00:09:54', NULL),
(5, 54, 7, 'sddvds', NULL, '2019-10-23 02:25:23', '2019-10-23 02:25:23', NULL),
(6, 54, 6, 'lâu vl', NULL, '2019-10-23 02:48:06', '2019-10-23 02:48:06', NULL),
(7, 54, 5, 'dạt', NULL, '2019-10-30 02:27:21', '2019-10-30 02:27:21', NULL),
(8, 54, NULL, 'hello !!', NULL, '2019-10-31 00:58:44', '2019-10-31 00:58:44', 1);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `consultings`
--

CREATE TABLE `consultings` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `category` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `author` int(10) UNSIGNED DEFAULT NULL,
  `student_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `center` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `consultings`
--

INSERT INTO `consultings` (`id`, `fullname`, `phone`, `email`, `content`, `status`, `category`, `created_at`, `updated_at`, `author`, `student_name`, `dob`, `center`, `password`) VALUES
(1, 'Tien Datfff', '0344445304', 'datnt@ieg.vn', 'uiyi', '1', '1', '2020-02-04 20:40:25', '2020-02-06 00:34:24', 54, NULL, NULL, NULL, NULL),
(2, 'Tien Dat', '3103413269', 'datnt@ieg.vn', '654645', '0', '1', '2020-02-04 20:59:58', '2020-02-04 20:59:58', NULL, NULL, NULL, NULL, NULL),
(3, 'sdfsdfsdfd', '3103413269', 'clonebyby1@gmail.com', 'fsdfsfs', '0', '1', '2020-02-05 20:19:40', '2020-02-05 20:19:40', NULL, NULL, NULL, NULL, NULL),
(4, 'Tien Dat', '0344445304', 'datnt@ieg.vn', 'sdvsd', '0', '1', '2020-02-05 20:37:40', '2020-02-05 20:37:40', NULL, NULL, NULL, NULL, NULL),
(5, 'Tbvdfbgdfien Dat', '0344445304', 'datnt@ieg.vn', 'sdvsd', '0', '1', '2020-02-05 20:40:20', '2020-02-05 20:40:20', NULL, NULL, NULL, NULL, NULL),
(6, 'Tien Dat', '0344445304', 'datnt@ieg.vn', 'gsd', '1', '2', '2020-02-05 20:41:27', '2020-02-06 00:47:53', 54, NULL, NULL, NULL, NULL),
(7, 'Tien Dat', '0344445304', 'datnt@ieg.vn', NULL, '0', '2', '2020-02-05 20:46:04', '2020-02-05 20:46:04', NULL, 'Tien Dat', '2', '3', NULL),
(8, 'Tien Dat', NULL, 'datnt@ieg.vn', NULL, '0', '3', '2020-02-05 20:51:19', '2020-02-05 20:51:19', NULL, NULL, NULL, NULL, '$2y$10$798Vw0kj0gcenQRCEmeOi.5kkCdNM9JrdmwmFLr3G8ifezKz6C2b2'),
(9, 'Tien Dat', '0344445304', 'datnt@ieg.vn', NULL, '0', '2', '2020-02-21 22:19:38', '2020-02-21 22:19:38', NULL, 'Tien Dat', '2003', '3', NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `header_course` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec1_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec1_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec1_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec2_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec2_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec2_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec3_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec3_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec3_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `col_md` int(10) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `courses`
--

INSERT INTO `courses` (`id`, `name`, `slug_name`, `icon`, `image`, `title`, `quote`, `header_course`, `sec1_title`, `sec1_content`, `sec1_image`, `sec2_title`, `sec2_content`, `sec2_image`, `sec3_title`, `sec3_content`, `sec3_image`, `col_md`, `created_at`, `updated_at`) VALUES
(1, 'Tiếng anh', 'tieng-anh', '4CyV-iconinfo_01.png', 'd0Fj-00.jpg', 'Tất cả khởi nguồn từ một triết lý', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'ĐIỀU GÌ LÀM NÊN SỰ KHÁC BIỆT TẠI IEG', 'ĐỀ CẬP <br> CÓ HỆ THỐNG', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'E4C7-toanhoc_02.jpg', 'KÍCH THÍCH <br> TÍNH SÁNG TẠO', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'LZFx-toanhoc_03.jpg', 'PHÁT TRIỂN TƯ DUY', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'm0HQ-toanhoc_04.jpg', 8, '2020-05-17 00:53:41', '2020-05-17 00:53:41'),
(2, 'Toán Học', 'toan-hoc', 'nv9c-iconinfo_03.png', 'jtBB-VN.png', 'Tất cả khởi nguồn từ một triết lý', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'ĐIỀU GÌ LÀM NÊN SỰ KHÁC BIỆT TẠI IEG', 'ĐỀ CẬP CÓ HỆ THỐNG', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'E4C7-toanhoc_02.jpg', 'KÍCH THÍCH <br> TÍNH SÁNG TẠO', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'LZFx-toanhoc_03.jpg', 'PHÁT TRIỂN TƯ DUY', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'm0HQ-toanhoc_04.jpg', 4, '2020-05-17 00:53:41', '2020-05-17 01:33:44'),
(3, 'Khoa học', 'khoa-hoc', 'BQuU-iconinfo_02.png', 'ub15-VN (1).png', 'Tất cả khởi nguồn từ một triết lý', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'ĐIỀU GÌ LÀM NÊN SỰ KHÁC BIỆT TẠI IEG', 'ĐỀ CẬP <br> CÓ HỆ THỐNG', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'E4C7-toanhoc_02.jpg', 'KÍCH THÍCH <br> TÍNH SÁNG TẠO', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'LZFx-toanhoc_03.jpg', 'PHÁT TRIỂN TƯ DUY', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'm0HQ-toanhoc_04.jpg', 4, '2020-05-17 00:53:41', '2020-05-17 01:34:32'),
(4, 'socrates', 'socrates', '32mJ-iconinfo_04.png', 'CnXG-VN.png', 'Tất cả khởi nguồn từ một triết lý', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'ĐIỀU GÌ LÀM NÊN SỰ KHÁC BIỆT TẠI IEG', 'ĐỀ CẬP <br> CÓ HỆ THỐNG', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'E4C7-toanhoc_02.jpg', 'KÍCH THÍCH <br> TÍNH SÁNG TẠO', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'LZFx-toanhoc_03.jpg', 'PHÁT TRIỂN TƯ DUY', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'm0HQ-toanhoc_04.jpg', 4, '2020-05-17 00:53:41', '2020-05-17 01:35:37'),
(5, 'Ielts', 'ielts', 'WJoC-iconinfo_05.png', 'OYNg-VN.jpg', 'Tất cả khởi nguồn từ một triết lý', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'ĐIỀU GÌ LÀM NÊN SỰ KHÁC BIỆT TẠI IEG', 'ĐỀ CẬP <br> CÓ HỆ THỐNG', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'E4C7-toanhoc_02.jpg', 'KÍCH THÍCH <br> TÍNH SÁNG TẠO', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'LZFx-toanhoc_03.jpg', 'PHÁT TRIỂN TƯ DUY', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'm0HQ-toanhoc_04.jpg', 4, '2020-05-17 00:53:41', '2020-05-17 01:35:57');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customconfigs`
--

CREATE TABLE `customconfigs` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_ieg` longtext CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `teach_learn` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_bin DEFAULT NULL
) ;

--
-- Đang đổ dữ liệu cho bảng `customconfigs`
--

INSERT INTO `customconfigs` (`id`, `store_ieg`, `teach_learn`, `created_at`, `updated_at`) VALUES
(1, '{\"title\":\"H\\u00c0NH TR\\u00ccNH V\\u1ea0N D\\u1eb6M B\\u1eaeT \\u0110\\u1ea6U T\\u1eea M\\u1ed8T B\\u01af\\u1edaC CH\\u00c2N\",\"quote\":\"aaaaT\\u1ea1i IEG, t\\u1ea5t c\\u00e1c c\\u00e1c th\\u00f4ng tin quan tr\\u1ecdng c\\u1ea3 b\\u00ean trong & b\\u00ean ngo\\u00e0i nh\\u00e0 tr\\u01b0\\u1eddng \\u0111\\u01b0\\u1ee3c t\\u1ed5ng h\\u1ee3p m\\u1ed9t c\\u00e1c c\\u00f3 h\\u1ec7 th\\u1ed1ng \\u0111\\u1ec3 vi\\u1ec7c l\\u1ef1a ch\\u1ecdn lu\\u1ed3ng th\\u00f4ng tin cho qu\\u00e1 tr\\u00ecnh h\\u1ecdc t\\u1eadp tr\\u1edf n\\u00ean thu\\u1eadn l\\u1ee3i h\\u01a1n.\",\"sec1_title\":\"V\\u00cc SAO CH\\u00daNG T\\u00d4I \\u1ede \\u0110\\u00c2Y\",\"sec1_content\":\"T\\u00f9y n\\u1ed9i dung c\\u1ee7a m\\u1ed7i bu\\u1ed5i h\\u1ecdc v\\u00e0 t\\u00f9y tr\\u00ecnh \\u0111\\u1ed9, h\\u1ecdc sinh s\\u1ebd \\u0111\\u01b0\\u1ee3c th\\u1eed s\\u1ee9c v\\u1edbi c\\u00e1c d\\u1ea1ng tr\\u00f2 ch\\u01a1i v\\u00e0 \\u0111\\u1ed9 kh\\u00f3 kh\\u00e1c nhau. \\u0110\\u00e2y l\\u00e0 c\\u00e1ch v\\u1eeba k\\u00edch th\\u00edch tr\\u00ed th\\u00f4ng minh c\\u1ee7a c\\u00e1c em, v\\u1eeba k\\u00edch th\\u00edch ni\\u1ec1m \\u0111am m\\u00ea To\\u00e1n h\\u1ecdc v\\u00e0 \\u0111\\u1eb7c bi\\u1ec7t \\u0111\\u1ec3 vi\\u1ec7c h\\u1ecdc kh\\u00f4ng c\\u00f2n l\\u00e0 g\\u00e1nh n\\u1eb7ng m\\u00e0 l\\u00e0 ni\\u1ec1m vui cho m\\u1ed7i ng\\u00e0y \\u0111\\u1ebfn l\\u1edbp\",\"sec2_title\":\"CH\\u00daNG T\\u00d4I \\u0110ANG MU\\u1ed0N V\\u1ebc G\\u00cc\",\"sec2_content\":\"T\\u00f9y n\\u1ed9i dung c\\u1ee7a m\\u1ed7i bu\\u1ed5i h\\u1ecdc v\\u00e0 t\\u00f9y tr\\u00ecnh \\u0111\\u1ed9, h\\u1ecdc sinh s\\u1ebd \\u0111\\u01b0\\u1ee3c th\\u1eed s\\u1ee9c v\\u1edbi c\\u00e1c d\\u1ea1ng tr\\u00f2 ch\\u01a1i v\\u00e0 \\u0111\\u1ed9 kh\\u00f3 kh\\u00e1c nhau. \\u0110\\u00e2y l\\u00e0 c\\u00e1ch v\\u1eeba k\\u00edch th\\u00edch tr\\u00ed th\\u00f4ng minh c\\u1ee7a c\\u00e1c em, v\\u1eeba k\\u00edch th\\u00edch ni\\u1ec1m \\u0111am m\\u00ea To\\u00e1n h\\u1ecdc v\\u00e0 \\u0111\\u1eb7c bi\\u1ec7t \\u0111\\u1ec3 vi\\u1ec7c h\\u1ecdc kh\\u00f4ng c\\u00f2n l\\u00e0 g\\u00e1nh n\\u1eb7ng m\\u00e0 l\\u00e0 ni\\u1ec1m vui cho m\\u1ed7i ng\\u00e0y \\u0111\\u1ebfn l\\u1edbp\",\"sec3_title\":\"\\u0110I\\u1ec0U G\\u00cc NEO \\u0110\\u1eacU CH\\u00daNG T\\u00d4I\",\"sec3_content\":\"T\\u00f9y n\\u1ed9i dung c\\u1ee7a m\\u1ed7i bu\\u1ed5i h\\u1ecdc v\\u00e0 t\\u00f9y tr\\u00ecnh \\u0111\\u1ed9, h\\u1ecdc sinh s\\u1ebd \\u0111\\u01b0\\u1ee3c th\\u1eed s\\u1ee9c v\\u1edbi c\\u00e1c d\\u1ea1ng tr\\u00f2 ch\\u01a1i v\\u00e0 \\u0111\\u1ed9 kh\\u00f3 kh\\u00e1c nhau. \\u0110\\u00e2y l\\u00e0 c\\u00e1ch v\\u1eeba k\\u00edch th\\u00edch tr\\u00ed th\\u00f4ng minh c\\u1ee7a c\\u00e1c em, v\\u1eeba k\\u00edch th\\u00edch ni\\u1ec1m \\u0111am m\\u00ea To\\u00e1n h\\u1ecdc v\\u00e0 \\u0111\\u1eb7c bi\\u1ec7t \\u0111\\u1ec3 vi\\u1ec7c h\\u1ecdc kh\\u00f4ng c\\u00f2n l\\u00e0 g\\u00e1nh n\\u1eb7ng m\\u00e0 l\\u00e0 ni\\u1ec1m vui cho m\\u1ed7i ng\\u00e0y \\u0111\\u1ebfn l\\u1edbp\"}', NULL, '2020-03-13 21:12:25', '2020-03-16 03:17:48');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `custompages`
--

CREATE TABLE `custompages` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec1_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec1_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec1_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec2_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec2_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec2_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec3_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec3_content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sec3_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `custompages`
--

INSERT INTO `custompages` (`id`, `title`, `quote`, `sec1_title`, `sec1_content`, `sec1_image`, `sec2_title`, `sec2_content`, `sec2_image`, `sec3_title`, `sec3_content`, `sec3_image`, `created_at`, `updated_at`) VALUES
(1, 'HÀNH TRÌNH VẠN DẶM BẮT ĐẦU TỪ MỘT BƯỚC CHÂN', 'Tại IEG, tất các các thông tin quan trọng cả bên trong & bên ngoài nhà trường được tổng hợp một các có hệ thống để việc lựa chọn luồng thông tin cho quá trình học tập trở nên thuận lợi hơn.', 'VÌ SAO CHÚNG TÔI Ở ĐÂY', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'Bx4t-imgsumenh.png', 'CHÚNG TÔI ĐANG MUỐN VẼ GÌ', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', '2WGv-imgtamnhin.png', 'ĐIỀU GÌ NEO ĐẬU CHÚNG TÔI', 'Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp', 'bwUf-imggiatri.png', '2020-05-16 21:22:34', '2020-05-16 21:35:35');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `customskotfs`
--

CREATE TABLE `customskotfs` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `position` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quote` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `layouts` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `customskotfs`
--

INSERT INTO `customskotfs` (`id`, `fullname`, `position`, `quote`, `content`, `image1`, `image2`, `layouts`, `created_at`, `updated_at`) VALUES
(1, 'Nguyễn Tiến Đạt', 'Nhân viên quét rác', 'Mỗi tách cà phê tại The Coffee House đều bắt đầu từ cây cà phê', '1', 'BVvs-ba_hong.jpg', 'WItI-ba_hong_grande.jpg', '1', '2020-03-08 01:38:28', '2020-03-08 05:00:52'),
(2, 'Nguyễn Chí Hiếu', 'CEO', '4 người nhiễm Covid-19 ở Hà Nội: Cách ly 365 người', '<p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Theo ông Nguyễn Khắc Hiền - Giám đốc Sở Y tế Hà Nội, trong số 21 ca bệnh Covid-19 đã xác nhận tại Việt Nam, Hà Nội đã có 4 ca nhiễm bệnh&nbsp;gồm: N.H.N (26 tuổi); D.D.P&nbsp;(27 tuổi, lái xe riêng của bệnh nhân N.H.N)&nbsp;; L.T.H. (64 tuổi, bác của bệnh nhân N.H.N); N.Q.T (61 tuổi, ngồi gần bệnh nhân N.H.N trên chuyến bay VN0054).</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Tổng số người tiếp xúc gần (F1) với 4 trường hợp này là 130 người, số người tiếp xúc với&nbsp;người tiếp xúc gần&nbsp;(F2) là 226.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Ở phường Trúc Bạch, cơ quan điều tra đã điều tra 66 hộ gia đình với tổng số&nbsp;189 người dân, lấy 148 mẫu xét nghiệm.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Tại Bệnh viện Hồng Ngọc, có 20 người tiếp xúc gần (F1) đang cách ly tại Bệnh viện Nhiệt đới Trung ương; 164 người tiếp xúc với người tiếp xúc&nbsp;(F2), trong đó có 64 bệnh nhân ngoại trú các ly tại nhà, 60 nhân viên y tế cách ly tại Long Biên và 40 nhân viên y tế khác các ly tại bệnh viện. Có 20 mẫu xét nghiệm đã được lấy, trong đó 19 mẫu âm tính, 1 mẫu đang chờ kết quả.</p>', 'Y8XQ-anhninh1_grande.jpg', 'TF1s-anhninh2.jpg', '1', '2020-03-08 01:56:03', '2020-03-08 01:56:03'),
(3, 'Nguyễn Chí Trung', 'CEO', 'COVID-19 tác động mạnh đến nền kinh tế của các nước đang phát triển ở Châu Á', '<p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">COVID-19 đang diễn ra có tác động đáng kể đến các nền nền kinh tế Châu Á đang phát triển thông qua nhiều kênh, bao gồm giảm mạnh nhu cầu trong nước, du lịch và kinh doanh du lịch, liên kết sản xuất và thương mại, gián đoạn cung ứng và ảnh hưởng sức khỏe.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Theo ADB, mức độ thiệt hại kinh tế sẽ phụ thuộc vào mức độ bùng phát khó lường của dịch bệnh. Phạm vi các kịch bản được khảo sát trong phân tích cho thấy tác động toàn cầu trong phạm vi từ 77 tỷ đến 347 tỷ USD, tương đương 0,1% đến 0,4% tổng sản phẩm quốc nội (GDP) toàn cầu.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">ADB cũng cho rằng, trong một kịch bản ở mức vừa phải, khi các hành vi và biện pháp phòng ngừa sau 3 tháng dịch bệnh bùng phát như cấm đi lại và các hạn chế đã được áp dụng vào cuối tháng 1 bắt đầu được nới lỏng, thiệt hại toàn cầu có thể lên tới 156 tỷ USD, tương đương 0,2% GDP toàn cầu. Trong đó, Cộng hòa Nhân dân Trung Hoa sẽ chịu thiệt hại 103 tỷ USD, hay 0,8% GDP của nước này. Phần còn lại của châu Á sẽ mất 22 tỷ USD, tương đương 0,2% GDP.</p>', '435o-anhhoa2.jpg', '2fxD-anhhoa2.jpg', '2', '2020-03-08 02:04:18', '2020-03-08 02:04:18'),
(4, 'Nguyễn Chí Hoa', 'COO', 'COVID-19 tác động mạnh đến nền kinh tế của các nước đang phát triển ở Châu Á', '<p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">COVID-19 đang diễn ra có tác động đáng kể đến các nền nền kinh tế Châu Á đang phát triển thông qua nhiều kênh, bao gồm giảm mạnh nhu cầu trong nước, du lịch và kinh doanh du lịch, liên kết sản xuất và thương mại, gián đoạn cung ứng và ảnh hưởng sức khỏe.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Theo ADB, mức độ thiệt hại kinh tế sẽ phụ thuộc vào mức độ bùng phát khó lường của dịch bệnh. Phạm vi các kịch bản được khảo sát trong phân tích cho thấy tác động toàn cầu trong phạm vi từ 77 tỷ đến 347 tỷ USD, tương đương 0,1% đến 0,4% tổng sản phẩm quốc nội (GDP) toàn cầu.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">ADB cũng cho rằng, trong một kịch bản ở mức vừa phải, khi các hành vi và biện pháp phòng ngừa sau 3 tháng dịch bệnh bùng phát như cấm đi lại và các hạn chế đã được áp dụng vào cuối tháng 1 bắt đầu được nới lỏng, thiệt hại toàn cầu có thể lên tới 156 tỷ USD, tương đương 0,2% GDP toàn cầu. Trong đó, Cộng hòa Nhân dân Trung Hoa sẽ chịu thiệt hại 103 tỷ USD, hay 0,8% GDP của nước này. Phần còn lại của châu Á sẽ mất 22 tỷ USD, tương đương 0,2% GDP.</p>', '435o-anhhoa2.jpg', '2fxD-anhhoa2.jpg', '0', '2020-03-08 02:04:18', '2020-03-08 02:04:18'),
(5, 'Nguyễn Chí Hiền', 'CEO', 'Hội kiến trúc \"chơi lớn\" năm 2020: Tặng 100% chi phí thiết kế kiến trúc', '<p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\"><strong style=\"line-height: 1.6;\">Một số điều bạn nên biết về Hội Kiến Trúc</strong></p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Để xác định được chương trình “Chào hè 2020” trên thuộc đơn vị uy tín không? Bạn không nên bỏ qua một số thông tin về Hoikientruc.com ngay dưới đây:</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\"><em style=\"line-height: 1.6;\"><strong style=\"line-height: 1.6;\">Hội Kiến Trúc là ai?</strong></em></p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Hội Kiến Trúc là nơi hội tụ đông đảo các kiến trúc sư giỏi trên khắp cả nước. Đến với chúng tôi, khách hàng sẽ hài lòng tuyệt đối về đội ngũ họa sĩ, kiến trúc sư, kỹ sư, công nhân chuyên nghiệp và xuất sắc của chúng tôi. Sẵn sàng hỗ trợ, đáp ứng đầy đủ mọi nhu cầu của quý khách hàng một cách tận tình nhất cho thể.</p><p style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">Đội ngũ chuyên gia của Hoikientruc.com đã có trên dưới 20 năm kinh nghiệm trên các lĩnh vực như: Thiết kế nội thất, thiết kế kiến trúc, thiết kế thi công nội ngoại thất,....Ở đa dạng các công trình như: Nhà hàng, nội thất căn hộ chung cư, khách sạn, quán cafe, nội thất văn phòng,....</p>', 'amcA-28827277_2051622368445112_5970865629524999462_o_2x_grande.jpg', 'cRSZ-ba_uyen2.jpg', '3', '2020-03-08 02:13:38', '2020-03-08 02:13:38'),
(6, 'Nguyễn Chí Mùi', 'CEO', 'Mỗi tách cà phê tại The Coffee House đều bắt đầu từ cây cà phê', '<p><span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px;\">Cũng tại buổi Lễ, đại diện chủ đầu tư, ông Nguyễn Văn On – Giám đốc công ty TNHH Nam Châu đã chia sẻ:&nbsp;</span><em style=\"font-family: Arial, Helvetica, sans-serif; font-size: 15px; line-height: 1.6;\">“Sau quá trình triển khai thành công giai đoạn 1 của dự án cuối năm 2019 vừa qua, Công ty TNHH Nam Châu tiếp tục đẩy mạnh phát triển dự án Vạn Phát Sông Hậu, đây sẽ là một trong những dự án mũi nhọn của thị trường Bất động sản phân khúc Bất động sản Công nghiệp tại khu vực Miền Tây trong năm nay. Được mệnh danh là Khu đô thị thủ phủ công nghiệp Miền Tây, Vạn Phát Sông Hậu sở hữu vị trí đắc địa là dự án duy nhất nằm liền kề cụm khu công nghiệp Sông Hậu và tiếp giáp Cảng Cái Cui, Trần Đề, đồng thời nằm ngay tuyến đường quốc lộ 927 – là của ngõ giao thương huyết mạch tại khu vực, thúc đẩy kinh tế vùng. Ngoài việc chỉnh chu và hoàn thiện cơ sở hạ tầng, đây là một trong những dự án góp phần giải quyết được nhu cầu nhà ở, việc làm cho hơn 18.000 công nhân tại khu vực Đồng bằng Sông Cửu Long, mở ra cơ hội đầu tư sinh lời phân khúc Công nghiệp hoàn toàn mới và đảm bảo sinh lời tối đa cho nhà đầu tư.”</em><br></p>', 'IwwW-ba_leduy.jpg', 'zMk5-sup_niemvq1_grande.jpg', '4', '2020-03-08 02:16:50', '2020-03-08 02:16:50');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `gallery_videos`
--

CREATE TABLE `gallery_videos` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug_title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `highlight` int(10) DEFAULT 0,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `gallery_videos`
--

INSERT INTO `gallery_videos` (`id`, `title`, `slug_title`, `description`, `content`, `link`, `highlight`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Avicii - Wake Me Up (Mellen Gi & Tommee Profitt Remix) [Jeep,BMW,Ferrari,Dodge]', 'avicii-wake-me-up-mellen-gi-tommee-profitt-remix-jeepbmwferraridodge', 'Avicii - Wake Me Up (Mellen Gi & Tommee Profitt Remix)', 'contentmm', 'https://www.youtube.com/watch?v=CWmPi4lIZxA', 0, 54, '2019-10-30 20:47:00', '2019-10-30 21:50:29'),
(3, 'The Chainsmokers & Coldplay - Something Just Like This (Extended Radio Edit)', 'the-chainsmokers-coldplay-something-just-like-this-extended-radio-edit', 'lolo', 'content', 'https://www.youtube.com/watch?v=_tNU6dpjIyM', 0, 54, '2019-10-30 21:14:15', '2020-02-21 03:20:15'),
(5, '[Lyrics + Vietsub] Inner Demons - Julia Brennan', 'lyrics-vietsub-inner-demons-julia-brennan', '[Lyrics + Vietsub] Inner Demons - Julia Brennanf', 'contentfdsfsdfsdfsdfdsf', 'https://www.youtube.com/watch?v=vEPAvEqYtOI', 0, 54, '2019-10-31 02:50:12', '2019-10-31 02:50:12'),
(6, 'Billie Eilish - idontwannabeyouanymore', 'billie-eilish-idontwannabeyouanymore', 'Billie Eilish - idontwannabeyouanymoreBillie Eilish - idontwannabeyouanymore', '<h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Billie Eilish - idontwannabeyouanymore</yt-formatted-string>Billie Eilish - idontwannabeyouanymoreBillie Eilish - idontwannabeyouanymore</h1><p style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><br></p><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Billie Eilish - idontwannabeyouanymore</yt-formatted-string>Billie Eilish - idontwannabeyouanymoreBillie Eilish - idontwannabeyouanymore</h1><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Billie Eilish - idontwannabeyouanymore</yt-formatted-string>Billie Eilish - idontwannabeyouanymoreBillie Eilish - idontwannabeyouanymore</h1><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Billie Eilish - idontwannabeyouanymore</yt-formatted-string>Billie Eilish - idontwannabeyouanymoreBillie Eilish - idontwannabeyouanymore</h1>', 'https://www.youtube.com/watch?v=skHbZBsS7hM', 1, 54, '2020-02-21 03:39:30', '2020-02-21 03:39:30'),
(7, 'Imagine Dragons - Bad Liar', 'imagine-dragons-bad-liar', 'Imagine Dragons - Bad LiarImagine Dragons - Bad LiarImagine Dragons - Bad Liar', '<h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">v gdsfg&nbsp;</yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Imagine Dragons - Bad Liar</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><br></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div>', 'https://www.youtube.com/watch?v=I-QfPUz1es8', 1, 54, '2020-02-21 03:43:31', '2020-02-21 03:43:31'),
(8, 'Kina - Can We Kiss Forever? (feat. Adriana Proenza)', 'kina-can-we-kiss-forever-feat-adriana-proenza', 'Kina - Can We Kiss Forever? (feat. Adriana Proenza)', '<h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; color: rgb(0, 0, 0); font-family: Roboto, Arial, sans-serif; font-size: 10px; font-style: normal; font-variant-ligatures: normal; font-variant-caps: normal; font-weight: 400; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;\"></div></h1><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin-top: 0px; margin-bottom: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); max-height: 4.8rem; overflow: hidden; line-height: 2.4rem; font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none);\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div><h1 class=\"title style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: block; max-height: 4.8rem; overflow: hidden; font-weight: 400; line-height: 2.4rem; color: var(--ytd-video-primary-info-renderer-title-color, var(--yt-spec-text-primary)); font-family: Roboto, Arial, sans-serif; font-size: var(--ytd-video-primary-info-renderer-title-font-size, 1.8rem); font-variant-ligatures: normal; font-variant-caps: normal; font-variant-numeric: ; font-variant-east-asian: ; transform: var(--ytd-video-primary-info-renderer-title-transform, none); text-shadow: var(--ytd-video-primary-info-renderer-title-text-shadow, none); font-style: normal; letter-spacing: normal; orphans: 2; text-align: start; text-indent: 0px; text-transform: none; white-space: normal; widows: 2; word-spacing: 0px; -webkit-text-stroke-width: 0px; text-decoration-style: initial; text-decoration-color: initial;\"><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\">Kina - Can We Kiss Forever? (feat. Adriana Proenza)</yt-formatted-string></h1><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><div><yt-formatted-string force-default-style=\"\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"word-break: break-word;\"><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div></yt-formatted-string></div><div id=\"info\" class=\"style-scope ytd-video-primary-info-renderer\" style=\"margin: 0px; padding: 0px; border: 0px; background: rgb(249, 249, 249); display: flex; flex-direction: row; align-items: center; font-family: Roboto, Arial, sans-serif; font-size: 10px;\"></div>', 'https://www.youtube.com/watch?v=w4WbWl0VZ-Y', 1, 54, '2020-02-21 03:44:05', '2020-02-21 03:44:05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_09_16_103130_create_departments_table', 2),
(4, '2019_09_16_103241_create_positions_table', 3),
(5, '2019_09_17_082452_create_roles_table', 4),
(6, '2019_09_17_082831_add_role_in_users_table', 5),
(7, '2019_09_17_084630_add_id_position_in_users_table', 6),
(8, '2019_09_18_090254_create_categories_table', 7),
(9, '2019_09_18_090405_create_news_table', 7),
(11, '2019_09_23_022904_create_comments_table', 8),
(12, '2019_09_23_045521_add_id_user_in_news_table', 9),
(13, '2019_09_23_050649_add_id_user_in_news_table', 10),
(14, '2019_09_26_031452_create_tests_table', 11),
(15, '2019_09_26_032050_create_tests_table', 12),
(16, '2019_10_31_030853_create_gallery_videos_table', 13),
(17, '2019_10_31_032001_create_gallery__images_table', 14),
(18, '2019_10_31_080624_add_id_video_column_in_comment', 14),
(19, '2019_11_01_033527_create_procedures_table', 15),
(20, '2019_11_01_034450_create_forms_table', 16),
(21, '2020_01_20_031123_create_blogs_table', 17),
(22, '2020_02_05_031439_create_consultings_table', 18),
(23, '2020_02_05_035353_add_author_inconsultings_table', 19),
(24, '2020_02_06_031219_add_column_in_consultings_table', 20),
(25, '2020_03_08_040933_create_customskotfs_table', 21),
(26, '2020_03_08_074737_create_customskotfs_table', 22),
(27, '2020_03_13_145805_create_slides_table', 23),
(28, '2020_03_14_034128_create_customconfigs_table', 24),
(29, '2020_05_17_040658_create_custompages_table', 25),
(30, '2020_05_17_045413_create_courses_table', 26),
(31, '2020_05_17_093706_create_abouts_table', 27),
(32, '2020_05_17_152639_create_partners_table', 28);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `news`
--

CREATE TABLE `news` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_category` int(10) UNSIGNED NOT NULL,
  `titlenone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `hightlight` int(11) NOT NULL DEFAULT 0,
  `author` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `user_id` int(10) UNSIGNED NOT NULL DEFAULT 54
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `news`
--

INSERT INTO `news` (`id`, `title`, `id_category`, `titlenone`, `description`, `content`, `image`, `hightlight`, `author`, `created_at`, `updated_at`, `user_id`) VALUES
(1, '123', 1, '123', 'Đây là tóm tắt', '<p>dsafsdfsdsdfsd&nbsp;Juventus ra mắt sao 0 đồng: Lương 400.000 bảng/tuần, quyết giành cúp C1</p><p>Juventus ra mắt sao 0 đồng: Lương 400.000 bảng/tuần, quyết giành cúp C1</p><p>Juventus ra mắt sao 0 đồng: Lương 4fefefe00.000 bảng/tuần, quyết giành cúp C1</p><p>Juventus ra mắt sao 0 đồng: Lương 400.000 bảng/tuần, quyết giành cúp C1<br></p>', 'WFro-1563264250-535-vu-lat-xe-khach-14-nguoi-thuong-vong-tai-nan-1-1563263813-width660height439.jpg', 1, 54, '2019-09-18 02:59:32', '2019-09-18 03:52:48', 54),
(3, 'day la tieu de', 2, 'day-la-tieu-de', 'tểtrtertretertertre', 'hgtrh rbrtj tỵ tjk uyujk&nbsp;', 'v9zt-tintuc_7.jpg', 1, 54, '2019-09-18 19:20:17', '2020-05-17 09:04:07', 54),
(5, 'Google inks pact for new 35-storey office', 1, 'google-inks-pact-for-new-35-storey-office', 'That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.', '<p class=\"excert\" style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower</p><p style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually</p><div class=\"quote-wrapper\" style=\"box-sizing: border-box; background: rgba(130, 139, 178, 0.1); padding: 30px; line-height: 1.733; color: rgb(136, 136, 136); font-style: italic; margin-top: 25px; margin-bottom: 25px;\"><div class=\"quotes\" style=\"box-sizing: border-box; background: rgb(255, 255, 255); padding: 25px 25px 25px 30px; border-left: 2px solid;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.</div></div><p style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower</p><p style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually</p>', 'QXq5-single_blog_2.png', 1, 54, '2019-09-18 21:41:37', '2019-09-18 21:41:37', 54),
(6, 'test Google inks pact for new 35-storey office', 1, 'test-google-inks-pact-for-new-35-storey-office', 'That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.', '<p class=\"excert\" style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">fefefeMCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower</p><p style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually</p><div class=\"quote-wrapper\" style=\"box-sizing: border-box; background: rgba(130, 139, 178, 0.1); padding: 30px; line-height: 1.733; color: rgb(136, 136, 136); font-style: italic; margin-top: 25px; margin-bottom: 25px;\"><div class=\"quotes\" style=\"box-sizing: border-box; background: rgb(255, 255, 255); padding: 25px 25px 25px 30px; border-left: 2px solid;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training.</div></div><p style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower</p><p style=\"box-sizing: border-box; margin-bottom: 20px; color: rgb(162, 162, 162); line-height: 28px; font-size: 15px;\">MCSE boot camps have its supporters and its detractors. Some people do not understand why you should have to spend money on boot camp when you can get the MCSE study materials yourself at a fraction of the camp price. However, who has the willpower to actually sit through a self-imposed MCSE training. who has the willpower to actually</p>', 'HCor-single_blog_3.png', 0, 7, '2019-09-18 21:43:47', '2019-09-19 00:29:18', 54),
(7, 'Speark', 2, 'speark', 'That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.', '<p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p><br></p>', 'NEyI-tintuc_7.jpg', 0, 45, '2019-09-22 21:42:40', '2020-05-17 09:03:39', 54),
(8, 'đơn xin nghỉ việc', 1, 'don-xin-nghi-viec', 'đơn xin nghỉ việc', '<table class=\"MsoTableGrid\" border=\"1\" cellspacing=\"0\" cellpadding=\"0\" width=\"644\" style=\"width: 482.85pt; margin-left: -8.1pt; border: none;\">\r\n <tbody><tr style=\"mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;\r\n  height:95.85pt\">\r\n  <td width=\"204\" valign=\"top\" style=\"width:152.9pt;border:solid windowtext 1.0pt;\r\n  mso-border-alt:solid windowtext .5pt;padding:0in 5.4pt 0in 5.4pt;height:95.85pt\">\r\n  <h1 style=\"margin:0in;margin-bottom:.0001pt;text-align:justify;line-height:\r\n  130%;mso-outline-level:1\"><!--[if gte vml 1]><v:shapetype id=\"_x0000_t75\"\r\n   coordsize=\"21600,21600\" o:spt=\"75\" o:preferrelative=\"t\" path=\"m@4@5l@4@11@9@11@9@5xe\"\r\n   filled=\"f\" stroked=\"f\">\r\n   <v:stroke joinstyle=\"miter\"/>\r\n   <v:formulas>\r\n    <v:f eqn=\"if lineDrawn pixelLineWidth 0\"/>\r\n    <v:f eqn=\"sum @0 1 0\"/>\r\n    <v:f eqn=\"sum 0 0 @1\"/>\r\n    <v:f eqn=\"prod @2 1 2\"/>\r\n    <v:f eqn=\"prod @3 21600 pixelWidth\"/>\r\n    <v:f eqn=\"prod @3 21600 pixelHeight\"/>\r\n    <v:f eqn=\"sum @0 0 1\"/>\r\n    <v:f eqn=\"prod @6 1 2\"/>\r\n    <v:f eqn=\"prod @7 21600 pixelWidth\"/>\r\n    <v:f eqn=\"sum @8 21600 0\"/>\r\n    <v:f eqn=\"prod @7 21600 pixelHeight\"/>\r\n    <v:f eqn=\"sum @10 21600 0\"/>\r\n   </v:formulas>\r\n   <v:path o:extrusionok=\"f\" gradientshapeok=\"t\" o:connecttype=\"rect\"/>\r\n   <o:lock v:ext=\"edit\" aspectratio=\"t\"/>\r\n  </v:shapetype><v:shape id=\"image3.png\" o:spid=\"_x0000_s1026\" type=\"#_x0000_t75\"\r\n   style=\'position:absolute;left:0;text-align:left;margin-left:-.6pt;\r\n   margin-top:19.85pt;width:128.25pt;height:62.25pt;z-index:251659264;\r\n   visibility:visible;mso-wrap-style:square;mso-wrap-distance-left:9pt;\r\n   mso-wrap-distance-top:0;mso-wrap-distance-right:9pt;\r\n   mso-wrap-distance-bottom:0;mso-position-horizontal:absolute;\r\n   mso-position-horizontal-relative:margin;mso-position-vertical:absolute;\r\n   mso-position-vertical-relative:text\'>\r\n   <v:imagedata src=\"file:///C:/Users/datnt/AppData/Local/Temp/msohtmlclip1/01/clip_image001.png\"\r\n    o:title=\"\"/>\r\n   <w:wrap type=\"square\" anchorx=\"margin\"/>\r\n  </v:shape><![endif]--><!--[if !vml]--><img width=\"171\" height=\"83\" src=\"file:///C:/Users/datnt/AppData/Local/Temp/msohtmlclip1/01/clip_image002.jpg\" align=\"left\" hspace=\"12\" v:shapes=\"image3.png\"><!--[endif]--><span style=\"font-size:\r\n  12.0pt;line-height:130%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:\r\n  AR-SA\"><o:p></o:p></span></h1>\r\n  </td>\r\n  <td width=\"215\" style=\"width:160.95pt;border:solid windowtext 1.0pt;border-left:\r\n  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;\r\n  padding:0in 5.4pt 0in 5.4pt;height:95.85pt\">\r\n  <h1 align=\"center\" style=\"margin:0in;margin-bottom:.0001pt;text-align:center;\r\n  line-height:130%;mso-outline-level:1\"><span style=\"font-size:12.0pt;\r\n  line-height:130%;font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:\r\n  AR-SA\">CÔNG TY CỔ PHẦN IEG TOÀN CẦU<o:p></o:p></span></h1>\r\n  </td>\r\n  <td width=\"225\" style=\"width:169.0pt;border:solid windowtext 1.0pt;border-left:\r\n  none;mso-border-left-alt:solid windowtext .5pt;mso-border-alt:solid windowtext .5pt;\r\n  padding:0in 5.4pt 0in 5.4pt;height:95.85pt\">\r\n  <h1 style=\"margin:0in;margin-bottom:.0001pt;text-align:justify;line-height:\r\n  130%;mso-outline-level:1\"><span style=\"font-size:12.0pt;line-height:130%;\r\n  font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:AR-SA\">Mã số: </span><span style=\"font-size:12.0pt;line-height:130%;font-family:&quot;Times New Roman&quot;,serif\">BM-HR-01-03</span><span style=\"font-size:12.0pt;line-height:130%;font-family:&quot;Times New Roman&quot;,serif;\r\n  mso-fareast-language:AR-SA\"><o:p></o:p></span></h1>\r\n  <h1 style=\"margin:0in;margin-bottom:.0001pt;text-align:justify;line-height:\r\n  130%;mso-outline-level:1\"><span style=\"font-size:12.0pt;line-height:130%;\r\n  font-family:&quot;Times New Roman&quot;,serif;mso-fareast-language:AR-SA\">Ngày ban\r\n  hành: 05/07/2018<o:p></o:p></span></h1>\r\n  <p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:\r\n  justify;line-height:130%\"><b><span style=\"font-size:12.0pt;line-height:130%;font-family:&quot;Times New Roman&quot;,serif;\r\n  mso-fareast-language:AR-SA\">Lần ban hành: 01<o:p></o:p></span></b></p>\r\n  <p class=\"MsoNormal\" style=\"margin-bottom:0in;margin-bottom:.0001pt;text-align:\r\n  justify;line-height:130%\"><b><span style=\"font-size:12.0pt;line-height:130%;font-family:&quot;Times New Roman&quot;,serif;\r\n  mso-fareast-language:AR-SA\">Số trang: 01<o:p></o:p></span></b></p>\r\n  </td>\r\n </tr>\r\n</tbody></table>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-size:14.0pt;line-height:107%;font-family:&quot;Times New Roman&quot;,serif\">ĐƠN\r\nXIN NGHỈ VIỆC<o:p></o:p></span></b></p>\r\n\r\n<p class=\"MsoNormal\"><b><i><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Kính gửi:<o:p></o:p></span></i></b></p>\r\n\r\n<p class=\"MsoListParagraphCxSpFirst\" style=\"text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">-<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Ban\r\nGiám đốc Công ty<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoListParagraphCxSpMiddle\" style=\"text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">-<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Trưởng\r\nPhòng Hành chính-Nhân sự<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoListParagraphCxSpLast\" style=\"text-indent:-.25in;mso-list:l0 level1 lfo1\"><!--[if !supportLists]--><span style=\"font-family:&quot;Times New Roman&quot;,serif;mso-fareast-font-family:&quot;Times New Roman&quot;\">-<span style=\"font-variant-numeric: normal; font-variant-east-asian: normal; font-stretch: normal; font-size: 7pt; line-height: normal; font-family: &quot;Times New Roman&quot;;\">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n</span></span><!--[endif]--><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Trưởng\r\nPhòng/ban……………………………<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Tên tôi\r\nlà: ……………………………………………..<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Chức vụ: ………………………………………………<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Thuộc bộ\r\nphận: ………………………………………..<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Nay tôi\r\nlàm đơn này, kính xin Ban Giám Đốc cho tôi thôi được thôi việc từ\r\nngày………..tháng……….năm<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Lý\r\ndo…………………………………………………..<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Tôi xin\r\ncam kết sẽ bàn giao lại công việc, hồ sơ tài liệu, tài sản được cấp phát, và\r\ncông nợ (nếu có) cho nhân viên/hoặc bộ phận do công ty chỉ định trước khi nghỉ\r\nviệc.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Tôi rất\r\nmong Ban Giám Đốc xem xét, và chấp thuận cho tôi.<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\">Tôi xin\r\nchân thành cảm ơn!<o:p></o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<table class=\"MsoTableGrid\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"border: none;\">\r\n <tbody><tr>\r\n  <td width=\"319\" valign=\"top\" style=\"width:239.4pt;padding:0in 5.4pt 0in 5.4pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\">TRƯỞNG\r\n  PHÒNG/BAN<o:p></o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  </td>\r\n  <td width=\"319\" valign=\"top\" style=\"width:239.4pt;padding:0in 5.4pt 0in 5.4pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\">NGƯỜI\r\n  VIẾT ĐƠN<o:p></o:p></span></b></p>\r\n  </td>\r\n </tr>\r\n <tr>\r\n  <td width=\"319\" valign=\"top\" style=\"width:239.4pt;padding:0in 5.4pt 0in 5.4pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\">PHÒNG\r\n  HÀNH CHÍNH-NHÂN SỰ<o:p></o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></b></p>\r\n  </td>\r\n  <td width=\"319\" valign=\"top\" style=\"width:239.4pt;padding:0in 5.4pt 0in 5.4pt\">\r\n  <p class=\"MsoNormal\" align=\"center\" style=\"text-align:center\"><b><span style=\"font-family:&quot;Times New Roman&quot;,serif\">TỔNG\r\n  GIÁM ĐỐC<o:p></o:p></span></b></p>\r\n  </td>\r\n </tr>\r\n</tbody></table>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></p>\r\n\r\n<p class=\"MsoNormal\"><span style=\"font-family:&quot;Times New Roman&quot;,serif\"><o:p>&nbsp;</o:p></span></p>', 'iZZk-65805781_2603898416289831_2664872687129591808_n.png', 0, 54, '2019-10-31 03:18:23', '2019-10-31 03:18:23', 54),
(9, 'abcJuventus ra mắt sao 0 đồng: Lương 400.000 bảng/tuần, quyết giành cúp C1', 1, 'abcjuventus-ra-mat-sao-0-dong-luong-400000-bangtuan-quyet-gianh-cup-c1', 'That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.', '<p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><hr><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.</p><p>That dominion stars lights dominion divide years for fourth have don\'t stars is that he earth it first without heaven in place seed it second morning saying.<br></p>', '9QxO-seascape.jpg', 0, 54, '2019-12-01 21:23:02', '2020-05-17 09:07:20', 54);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `partners`
--

CREATE TABLE `partners` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `link` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `partners`
--

INSERT INTO `partners` (`id`, `name`, `link`, `alt`, `image`, `created_at`, `updated_at`) VALUES
(1, 'National Geographic', 'https://www.nationalgeographic.com/', 'National Geographic', '27Xn-logo_dt_01.png', '2020-05-17 09:38:38', '2020-05-17 09:46:19'),
(2, 'fulbright university vietnam', 'https://fulbright.edu.vn/vi/', 'fulbright university vietnam', 'k5H9-logo_dt_02.png', '2020-05-17 09:41:14', '2020-05-17 09:41:14'),
(3, 'National Geographic', 'https://www.nationalgeographic.com/', 'National Geographic', '27Xn-logo_dt_01.png', '2020-05-17 09:38:38', '2020-05-17 09:46:19'),
(4, 'fulbright university vietnam', 'https://fulbright.edu.vn/vi/', 'fulbright university vietnam', 'k5H9-logo_dt_02.png', '2020-05-17 09:41:14', '2020-05-17 09:41:14'),
(5, 'National Geographic', 'https://www.nationalgeographic.com/', 'National Geographic', '27Xn-logo_dt_01.png', '2020-05-17 09:38:38', '2020-05-17 09:46:19'),
(6, 'fulbright university vietnam', 'https://fulbright.edu.vn/vi/', 'fulbright university vietnam', 'YTmg-iconinfo_04.png', '2020-05-17 09:41:14', '2020-05-17 09:52:31');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', NULL, NULL),
(2, 'members', NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `slides`
--

CREATE TABLE `slides` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alt` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `location` int(10) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `slides`
--

INSERT INTO `slides` (`id`, `name`, `image`, `alt`, `location`, `created_at`, `updated_at`) VALUES
(1, 'banner1', '26NV-slide.jpg', 'banner1', 1, '2020-03-13 08:37:45', '2020-03-13 08:37:45'),
(2, 'banner2', 'rSsR-slide.jpg', 'banner2', 1, '2020-03-13 08:38:27', '2020-03-13 08:38:27'),
(3, 'banner day hoc', 'WigN-bg_pageSmall_1.png', 'banner3', 2, '2020-03-13 20:00:54', '2020-03-13 20:00:54'),
(4, 'slide 2 dayj vaf hoc', 'AAEX-bg_pageSmall.png', 'banner2', 2, '2020-03-13 20:02:29', '2020-03-13 20:02:29');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `fullname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `usersname` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birthday` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `fullname`, `usersname`, `email`, `role`, `email_verified_at`, `password`, `image`, `birthday`, `remember_token`, `created_at`, `updated_at`) VALUES
(54, 'Nguyễn Tiến Đạt', 'ieg1043', 'datnt@ieg.vn', 1, NULL, '$2y$10$zD/FspVV9.knyCOzz8bRd.dg0cr4Zj/QOsvge.vobelxHLZZDvEHe', 'Vohn-about_img.png', '04/25/2017', NULL, '2019-09-20 20:23:58', '2019-09-23 19:44:54'),
(259, 'Tien Dat', NULL, 'datnt1@ieg.vn', 2, NULL, '$2y$10$S8dng.26DtRORF23zBaXFOCyC3tohjuW0ZEFWNUss4noalQVWcLBa', 'vF8T-image.png', NULL, NULL, '2019-12-01 21:07:41', '2019-12-01 21:17:55'),
(260, 'datttt tien nguyen', NULL, 'dat1234@ieg.com', 1, NULL, '$2y$10$rw/v/WQSOlPM9VqiRFOTreqzCo6HjxMoj7saFXByJ56defvUexEFm', 'uUEL-hoa-la.png', NULL, NULL, '2020-05-16 20:40:35', '2020-05-16 20:40:35');

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `blogs_author_foreign` (`author`);

--
-- Chỉ mục cho bảng `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `comments_iduser_foreign` (`idUser`),
  ADD KEY `comments_idnews_foreign` (`idNews`),
  ADD KEY `comments_id_video_foreign` (`id_video`);

--
-- Chỉ mục cho bảng `consultings`
--
ALTER TABLE `consultings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `consultings_author_foreign` (`author`);

--
-- Chỉ mục cho bảng `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `custompages`
--
ALTER TABLE `custompages`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `customskotfs`
--
ALTER TABLE `customskotfs`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `gallery_videos`
--
ALTER TABLE `gallery_videos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `gallery_videos_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`),
  ADD KEY `news_id_catagory_foreign` (`id_category`),
  ADD KEY `news_user_id_foreign` (`user_id`),
  ADD KEY `author` (`author`);

--
-- Chỉ mục cho bảng `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `slides`
--
ALTER TABLE `slides`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_foreign` (`role`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `consultings`
--
ALTER TABLE `consultings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT cho bảng `customconfigs`
--
ALTER TABLE `customconfigs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `custompages`
--
ALTER TABLE `custompages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `customskotfs`
--
ALTER TABLE `customskotfs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT cho bảng `gallery_videos`
--
ALTER TABLE `gallery_videos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT cho bảng `news`
--
ALTER TABLE `news`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT cho bảng `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `slides`
--
ALTER TABLE `slides`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=261;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `blogs`
--
ALTER TABLE `blogs`
  ADD CONSTRAINT `blogs_author_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_id_video_foreign` FOREIGN KEY (`id_video`) REFERENCES `gallery_videos` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `comments_idnews_foreign` FOREIGN KEY (`idNews`) REFERENCES `news` (`id`),
  ADD CONSTRAINT `comments_iduser_foreign` FOREIGN KEY (`idUser`) REFERENCES `users` (`id`);

--
-- Các ràng buộc cho bảng `consultings`
--
ALTER TABLE `consultings`
  ADD CONSTRAINT `consultings_author_foreign` FOREIGN KEY (`author`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `gallery_videos`
--
ALTER TABLE `gallery_videos`
  ADD CONSTRAINT `gallery_videos_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `news`
--
ALTER TABLE `news`
  ADD CONSTRAINT `news_ibfk_1` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id`);

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_foreign` FOREIGN KEY (`role`) REFERENCES `roles` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
