@extends('be.layouts.index')
@section('title')
Sửa bài viết
@endsection
@section('content')
@include('msg')
<div class="col-md-12">
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Sửa bài viết</div>
            <div class="ibox-tools">
                <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
            </div>
        </div>
        <div class="ibox-body">
            <form action="admin/customs/edit/{{$kotf->id}}" method="post" enctype="multipart/form-data"
                class="form-horizontal" id="form-sample-1" novalidate="novalidate">
                {{ csrf_field() }}
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Fullname</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="fullname" type="text" value="{{$kotf->fullname}}"
                            placeholder="Tên Đầy đủ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Position</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="position" value="{{$kotf->position}}" type="text"
                            placeholder="Chức vụ">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Trích dẫn</label>
                    <div class="col-sm-10">
                        <input class="form-control" name="quote" type="text" value="{{$kotf->quote}}"
                            placeholder="Trích dẫn">
                    </div>
                </div>
                <div class="form-group">
                    <label>Content</label>
                    <textarea id="summernote" rows="15" name="content" placeholder="content">{{$kotf->id}}</textarea>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Ảnh 1</label>
                    <div class="col-sm-6">
                        <input class="form-control" name="image1" type="file">
                    </div>
                    <div class="col-md-4 cols-sm-4">
                        <img src="upload/kotf/{{$kotf->image1}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Ảnh 2</label>
                    <div class="col-sm-6">
                        <input class="form-control" name="image2" type="file">
                    </div>
                    <div class="col-md-4 cols-sm-4">
                        <img src="upload/kotf/{{$kotf->image2}}" alt="">
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2 col-form-label">Vị trí ảnh</label>
                    <div class="col-sm-10">
                        <select name="layouts" id="">
                            <option @if ($kotf->layouts == 0)
                                selected
                                @endif value="0">Lớn nửa bên trái</option>
                            <option @if ($kotf->layouts == 1)
                                selected
                                @endif value="1">nhỏ bên trái nửa trái</option>
                            <option @if ($kotf->layouts == 2)
                                selected
                                @endif value="2">nhỏ bên phải nửa trái</option>
                            <option @if ($kotf->layouts == 3)
                                selected
                                @endif value="3">nhỏ giữa</option>
                            <option @if ($kotf->layouts == 4)
                                selected
                                @endif value="4">nhỏ phải</option>
                        </select>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-10 ml-sm-auto">
                        <button class="btn btn-info" type="submit">Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<hr>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection