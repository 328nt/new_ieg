<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About extends Model
{
    protected $fillable = ['statistical'];
    protected $casts = [
        'statistical' => 'array'
    ];
}
