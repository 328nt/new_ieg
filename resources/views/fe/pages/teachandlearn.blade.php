@extends('fe/layouts/index')
@section('content')

<!--Banner-->
<div class="container-fluid">
    {{-- <div class="row">
        <img src="fe/fe/image/bg_pageSmall.png" style="width:100%">
    </div> --}}
    <div class="row">
        <div class="owl-carousel owl-theme banner_home">
            @foreach ($slides as $slide)
            @if ($slide->location == 2)
            <div class="item">
                <img src="upload/slide/{{$slide->image}}" alt="{{$slide->alt}}">
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<!--Banner_END-->

<div class="qoutes wpx1470 mt_100">
    <p class="qoutes_content">
        Tại IEG, tất các các thông tin quan trọng cả bên trong & bên ngoài nhà trường được tổng hợp một các có hệ thống
        để việc lựa chọn luồng thông tin cho quá trình học tập trở nên thuận lợi hơn.
    </p>
</div>

<!--Khóa học-->
<div class="container wpx920_width100vh">
    <h1 class="title_block">CÁC KHÓA HỌC TẠI IEG</h1>
    <div class="row flex-box block_01">
        {{-- <div class="col-md-8 h400 mb_30 col-xs-12">
            <div class="row">
                <a href="#">
                    <img src="upload/courses/BSBu-00.jpg" />
                    <div class="content_img">
                        <h2 class="tile-basic">Toán</h2>
                        <div class="row">
                            <div class="col-md-3 course-item">
                                fdsfdsfdsfdsf
                            </div>
                            <div class="col-md-3 course-item">
                                fdsfdsfdsfdsf
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </div> --}}
        <div class="col-md-8 h400 mb_30 col-xs-12">
            <img src="upload/courses/BSBu-00.jpg" />
            <div class="content_img">
                <h2 class="tile-basic">Tiếng anh tích hợp</h2>
                <div class="row course-row flex-box">
                    <div class="col-md-3 col-sm-6 col-xs-6 plr5">
                        <div class="course-item">
                            Học sinh tiểu học <br> 8-16 tuổi
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 plr5">
                        <div class="course-item">
                            Học sinh tiểu học <br> 8-16 tuổi
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 plr5">
                        <div class="course-item">
                            Học sinh tiểu học <br> 8-16 tuổi
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-6 col-xs-6 plr5">
                        <div class="course-item">
                            Học sinh tiểu học <br> 8-16 tuổi
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-4 h400 mb_30 col-xs-12">
            <a href="#">
                <img src="upload/courses/OYNg-VN.jpg" />
                <div class="content_img">
                    <h2 class="tile-basic">Toán</h2>
                </div>
            </a>
        </div>
        <div class="col-md-4 h400 mb_30 col-xs-12">
            <div class="row plr15" style="height: 50%; padding-bottom:10px">
                <a href="#">
                    <img src="fe/fe/image/bgKhoahoc_1.jpg" />
                    <div class="content_img2">
                        <h2 class="tile-basic">Imas</h2>
                        <h2 class="tile-basic">IKMC</h2>
                    </div>
                </a>
            </div>
            <div class="row plr15" style="height: 50%; padding-top:10px;">
                <a href="#">
                    <img src="upload/courses/OYNg-VN.jpg" />
                    <div class="content_img3">
                        <h2 class="tile-basic">Trại hè quốc tế</h2>
                    </div>
                </a>
            </div>
        </div>
        <div class="col-md-4 h400 mb_30 col-xs-12">
            <a href="#">
                <img src="upload/courses/OYNg-VN.jpg" />
                <div class="content_img">
                    <h2 class="tile-basic">Ielts</h2>
                </div>
            </a>
        </div>
        <div class="col-md-4 h400 mb_30 col-xs-12">
            <a href="#">
                <img src="upload/courses/OYNg-VN.jpg" />
                <div class="content_img">
                    <h2 class="tile-basic">Khoa học</h2>
                </div>
            </a>
        </div>
    </div>
</div>
<!--Khóa học-->
{{-- <div class="container wpx920_width100vh">
    <h1 class="title_block">CÁC KHÓA HỌC TẠI IEG</h1>
    <div class="row flex-box block_01">
        @foreach ($courses as $course)
        <div class="col-md-{{$course->col_md}} h400 mb_30 col-xs-12">
<a href="{{route('course', [$course->id, $course->slug_name])}}">
    <img src="upload/courses/{{$course->image}}" />
    <div class="content_img">
        <h2 class="tile-basic">{{$course->name}} </h2>
    </div>
</a>
</div>
@endforeach
</div>
</div> --}}
<!--/Khóa học_END-->
<!--block_02-->
<div class="container-fluid">
    <h1 class="title_block">Tại IEG</h1>
    <div class="row flex-box block_02">
        <div class="col-6-cust bgInfo_1 text-right flex-box bg_khohoc_1">
            <div>
                <div class="wpx720 pull-right">
                    <h3 class="title_bgInfo title_block mb_20">Trại hè IKMC</h3>
                    <p><a class="btn_cust--bl2 pull-right" href="http://kangaroo-math.vn/">Tìm hiểu thêm</a></p>
                </div>
            </div>
        </div>
        <div class="col-6-cust bgInfo_2 flex-box bg_khohoc_2">
            <div class="numbers">
                <div class="wpx720 pull-left flex-box">
                    <div class="col-4-cust">
                        <span class="count">7000</span>
                        <p class="font24">Học viên</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">70</span>
                        <p class="font24">Thí sinh</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">70000</span>
                        <p class="font24">Thí sinh</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">3</span>
                        <p class="font24">Học viên</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">50</span>
                        <p class="font24">Giải thưởng Olympic</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">50</span>
                        <p class="font24">Giải thưởng Olympic</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6-cust bgInfo_2 flex-box bg_khohoc_3">
            <div class="numbers">
                <div class="wpx720 pull-left flex-box">
                    <div class="col-4-cust">
                        <span class="count">7000</span>
                        <p class="font24">Học viên</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">70</span>
                        <p class="font24">Thí sinh</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">70000</span>
                        <p class="font24">Thí sinh</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">3</span>
                        <p class="font24">Học viên</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">50</span>
                        <p class="font24">Giải thưởng Olympic</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">50</span>
                        <p class="font24">Giải thưởng Olympic</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-6-cust bgInfo_1 flex-box bg_khohoc_4">
            <div>
                <div class="wpx720">
                    <h3 class="title_bgInfo title_block mb_20">IMAS</h3>
                    <p><a class="btn_cust--bl2" href="http://imas.ieg.vn/">Tìm hiểu thêm</a></p>
                </div>
            </div>
        </div>
    </div>
</div>
<!--/block_02_END-->

@endsection