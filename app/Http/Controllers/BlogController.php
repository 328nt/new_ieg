<?php

namespace App\Http\Controllers;

use App\Blog;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $blogs = Blog::All()->sortByDesc('id');
        // dd($blogs);
        return view('be/blog/list', ['blogs'=>$blogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be/blog/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'title' => 'required|max:500',
            'description' => 'required',
            'content' => 'required',
            'image' => 'required',

        ],[
            'title.required' =>'chưa nhập tiêu đề',
            'description.required' =>'chưa nhập tóm tắt',
            'content.required' =>'chưa nhập nội dung',
            'image.required' => 'vui lòng nhập ảnh',

        ]);
        $blog = new Blog();
        $blog->author = Auth::user()->id;
        $blog->title = $rq->title;
        $blog->titlenone = str_slug($rq->title);
        $blog->description = $rq->description;
        $blog->content = $rq->content;
        if ($rq->hasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/blog/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/blog", $img);
            $blog->image = $img;
        } else {
            $blog->image = "";
        }
        
        $blog->save();
        return redirect('admin/blogs/list')->with('msg','thêm blog thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function show(blog $blog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $blog = Blog::Find($id);
        return view('be/blog/edit', ['blog'=>$blog ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $this->validate($rq,[
            'title' => 'required|max:500',
            'description' => 'required',
            'content' => 'required',

        ],[
            'title.required' =>'chưa nhập tiêu đề',
            'description.required' =>'chưa nhập tóm tắt',
            'content.required' =>'chưa nhập nội dung',

        ]);
        $blog = Blog::find($id);
        $blog->title = $rq->title;
        $blog->titlenone = str_slug($rq->title);
        $blog->description = $rq->description;
        $blog->content = $rq->content;
        if ($rq->HasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/blog/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/blog", $img);
            unlink("upload/blog/".$blog->image);
            $blog->image = $img;
        }
        
        $blog->save();
        return redirect('admin/blogs/edit/'.$id)->with('msg','Sửa bài viết thành công thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\blog  $blog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $blog = Blog::find($id);
        $blog->delete();
        return redirect('admin/blog/list')->with('msg', 'xóa bài viết thành công');
    }
}
