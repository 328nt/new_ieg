@extends('fe/layouts/index')
@section('content')

<!--Banner-->
<div class="slider container-fluid">
    <div class="row">
        <div class="owl-carousel owl-theme banner_home">
            @foreach ($slides as $slide)
            @if ($slide->location == 1)
            <div class="item">
                <img src="upload/slide/{{$slide->image}}" alt="{{$slide->alt}}">
            </div>
            @endif
            @endforeach
        </div>
    </div>
</div>
<!--Banner_END-->

<!--Khóa học-->
    @include('fe/pages/form_course')
<!--/Khóa học_END-->
<!--block_02-->
<div class="container-fluid"> {{-- data-aos="fade-up" --}}
    <h1 class="title_block">Tại IEG</h1>
    <div class="row flex-box block_02">
        <div class="col-6-cust bgInfo_1 text-right flex-box bgInfo_1_home">
            <div>
                <div class="wpx720 pull-right">
                    <h3 class="title_bgInfo title_block mb_20">{{$about->inieg_title}}</h3>
                    <div class="content_bgInfo">{{$about->inieg_content}}</div>
                </div>
            </div>
            <p><a class="btn_cust--bl2 pull-right" href="{{$about->inieg_link}}">Tìm hiểu thêm</a></p>
        </div>
        <div class="col-6-cust bgInfo_2 flex-box bgInfo_2_home">
            <div class="numbers">
                <div class="wpx720 pull-left flex-box">
                    <div class="col-4-cust">
                        <span class="count">{{$about->statistical['num1']}}</span>
                        <p class="font24">{{$about->statistical['title1']}}</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">{{$about->statistical['num2']}}</span>
                        <p class="font24">{{$about->statistical['title2']}}</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">{{$about->statistical['num3']}}</span>
                        <p class="font24">{{$about->statistical['title3']}}</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">{{$about->statistical['num4']}}</span>
                        <p class="font24">{{$about->statistical['title4']}}</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">{{$about->statistical['num5']}}</span>
                        <p class="font24">{{$about->statistical['title5']}}</p>
                    </div>
                    <div class="col-4-cust">
                        <span class="count">{{$about->statistical['num6']}}</span>
                        <p class="font24">{{$about->statistical['title6']}}</p>
                    </div>
                </div>
            </div>
            <p><a class="btn_cust--bl2" href="{{$about->statistical['statistical_link']}}">Tìm hiểu thêm</a></p>
        </div>
    </div>
</div>
<!--/block_02_END-->

<!--block_03-->
<div class="container wpx920_width100vh">
    <h1 class="title_block">IEG và tôi</h1>
    <div class="row">
        <div class="wpx1470 block_03">
            <div class="owl-carousel owl-theme sliderInfo cust_btn_nextPrev">
                @foreach ($blogs as $blog)
                <div class="item">
                    <a href="toi-va-ieg/{{$blog->id}}/{{$blog->titlenone}}.html">
                        <img src="upload/blog/{{$blog->image}}">
                        <div class="sliderInfo--text">
                            {!!$blog->description!!}
                        </div>
                    </a>
                    <h5 class="h5--sliderInfo--text">{{$blog->title}}</h5>
                    <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p>
                </div>
                @endforeach
            </div>
            <p><a class="btn_boxMore" href="#">Xem thêm...</a></p>
        </div>
    </div>
</div>
<!--/block_03_END-->

<!--block_04-->
<div class="container wpx920_width100vh consulting">
    <div class="row">
        <div class="wpx1470 block_04" style="margin-top:0px; padding: 50px 0px;">
            <h4 class="text-center font24">Đăng ký nhận tư vấn từ IEG</h4>
            {{-- <form class="row" action="{{route('consulting')}}" method="POST">
                {{ csrf_field() }}
                <div class="col-md-12 flex-box form-flex mb_30">
                    <div class="col-4-cust">
                        <input type="text" name="fullname" required placeholder="Họ và tên...">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="phone" required placeholder="Số điện thoại">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="email" required placeholder="Email...">
                    </div>
                </div>
                <div class="col-md-12 flex-box form-flex">
                    <div class="col-8-cust">
                        <textarea placeholder="Nội dung" name="content"></textarea>
                    </div>
                    <div class="col-4-cust flex-box flext-wrapRev">
                        <button class="btn_block04" type="reset">Reset</button>
                        <button class="btn_block04" type="submit">Gửi đi</button>
                    </div>
                </div>
            </form> --}}
            <form class="row" action="{{route('consulting')}}" method="POST">
                {{ csrf_field() }}
                <div class="col-md-12 flex-box form-flex mb_30">
                    <div class="col-4-cust" >
                        <select name="center" id="">
                            <option selected disabled hidden >Khu vực</option> 
                            <option value="Hà Nội">Hà Nội</option>
                            <option value="Tp.HCM">Tp.HCM</option>
                        </select>
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="phone" required placeholder="Số điện thoại">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="email" required placeholder="Email...">
                    </div>
                </div>
                <div class="col-md-12 flex-box form-flex mb_30">
                    <div class="col-4-cust">
                        <input type="text" name="fullname" required placeholder="Họ và tên phụ huynh...">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="student_name" required placeholder="Họ và tên học sinh...">
                    </div>
                    <div class="col-4-cust">
                        <input type="text" name="dob" required placeholder="Năm sinh học sinh">
                    </div>
                </div>
                <div class="col-md-12 flex-box form-flex">
                    <div class="col-4-cust" >
                        <select name="course" id="">
                            <option selected disabled hidden >Môn học quan tâm</option> 
                            @foreach ($courses as $item)
                            <option value="{{$item->name}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-8-cust">
                        <textarea placeholder="content" required name="text"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="text-center">

                        <button class="btn_block04" type="submit">Send</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!--/block_04_END-->

<!--block_05-->
<div class="container-fluid">
    <h1 class="title_block">Đối tác & Đồng hành</h1>
    <div class="row block_05">
        <div class="wpx1470">
            <div class="wpx1200">
                <div class="owl-carousel owl-theme slider_doitac cust_btn_nextPrev">
                    @foreach ($partners as $partner)
                <div class="item"><a href="{{$partner->link}}" target="_blank" rel="noopener noreferrer"><img alt="{{$partner->alt}}" src="upload/partners/{{$partner->image}}"></a></div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
<!--/block_05_END-->

<!--block_06-->
{{-- <div class="container-fluid" data-aos="zoom-in" data-aos-duration="1500"> --}}
<div class="container-fluid">
    <h1 class="title_block">Chỉ đường</h1>
    <div class="row block_06">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs" role="tablist">
            <li role="presentation" class="active">
                <a class="text-center iconMap" href="#iegHanoi" aria-controls="iegHanoi" role="tab" data-toggle="tab">
                    <img src="fe/fe/image/icoMap.png">
                    <br />IEG Hà Nội
                </a>
            </li>
            <li role="presentation">
                <a class="text-center iconMap" href="#iegHochiminh" aria-controls="iegHochiminh" role="tab"
                    data-toggle="tab">
                    <img src="fe/fe/image/icoMap.png">
                    <br />IEG Hồ Chí Minh
                </a>
            </li>
            {{-- <li role="presentation">
                <a class="text-center iconMap" href="#iegDanang" aria-controls="iegDanang" role="tab" data-toggle="tab">
                    <img src="fe/fe/image/icoMap.png">
                    <br />Đà Nẵng
                </a>
            </li> --}}
        </ul>

        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="iegHanoi">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.0189040072028!2d105.82900181440743!3d21.03192959304721!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9f8560efe5%3A0x47879973073eb0fc!2zMTI4IFBo4buRIE5ndXnhu4VuIFRow6FpIEjhu41jLCDEkGnhu4duIELDoG4sIMSQ4buRbmcgxJBhLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1582341575263!5m2!1svi!2s"
                    width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>

            </div>
            <div role="tabpanel" class="tab-pane" id="iegHochiminh">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.0189040072605!2d105.82900181540234!3d21.031929593044925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9f8560efe5%3A0x47879973073eb0fc!2zMTI4IFBo4buRIE5ndXnhu4VuIFRow6FpIEjhu41jLCDEkGnhu4duIELDoG4sIMSQ4buRbmcgxJBhLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1563852145886!5m2!1svi!2s"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>
            {{-- <div role="tabpanel" class="tab-pane" id="iegDanang">
                <iframe
                    src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3724.0189040072605!2d105.82900181540234!3d21.031929593044925!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3135ab9f8560efe5%3A0x47879973073eb0fc!2zMTI4IFBo4buRIE5ndXnhu4VuIFRow6FpIEjhu41jLCDEkGnhu4duIELDoG4sIMSQ4buRbmcgxJBhLCBIw6AgTuG7mWksIFZp4buHdCBOYW0!5e0!3m2!1svi!2s!4v1563852145886!5m2!1svi!2s"
                    frameborder="0" style="border:0" allowfullscreen></iframe>
            </div> --}}
        </div>
    </div>
</div>
<!--/block_06_END-->
@endsection