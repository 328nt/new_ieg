<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customconfig extends Model
{
    protected $fillable = ['store_ieg'];
    protected $casts = [
        'store_ieg' => 'array'
    ];
}
