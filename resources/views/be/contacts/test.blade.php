@extends('be.layouts.index')
@section('title')
Danh sách bài viết
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Danh sách đăng ký kiểm tra năng lực</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>Họ tên PH</th>
                        <th>Email/Phone</th>
                        <th>Thông tin Học sinh</th>
                        <th>Content</th>
                        <th>Liên hệ</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($consultings as $consulting)
                    @if ($consulting->category == 2)
                    <tr>
                        <td>{{$consulting->id}}</td>
                        <td>{{$consulting->fullname}}</td>
                        <td>{{$consulting->phone}}
                            <br>{{$consulting->email}}</td>
                        <td>{{$consulting->student_name}} <br> {{$consulting->dob}} <br>{{$consulting->center}}</td>
                        <td>{{$consulting->content}}</td>
                        <td>
                            @if ($consulting->status == 0)
                            chưa liên hệ <br>

                            <form action="{{route('contacted')}}" method="post">
                                {{ csrf_field() }}
                                <input type="text" class="form-control" name="id" hidden value="{{$consulting->id}}">
                                <input type="submit" name="submit" value="Đã Liên Hệ" class="submit-btn">
                            </form>
                            @else
                            <i class="fa fa-check" style="color:green;"></i>
                            {{$consulting->users->fullname}}
                            @endif
                        </td>
                    </tr>
                    @endif
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            //"ajax": './assets/demo/data/table_data.json',
            /*"columns": [
                { "data": "name" },
                { "data": "office" },
                { "data": "extn" },
                { "data": "start_date" },
                { "data": "salary" }
            ]*/
        });
    })
</script>
@endsection