@extends('be.layouts.index')
@section('title')
Danh sách bài viết
@endsection
@section('content')

<!-- START PAGE CONTENT-->
<div class="page-content fade-in-up">

    @include('msg')
    <div class="ibox">
        <div class="ibox-head">
            <div class="ibox-title">Tin tức HR</div>
        </div>
        <div class="ibox-body" style="overflow-x:auto;">
            <table class="table table-striped table-bordered table-hover" id="example-table" cellspacing="0"
                width="100%">
                <thead>
                    <tr>
                        <th>id</th>
                        <th>image1</th>
                        <th>image2</th>
                        <th>fullname</th>
                        <th>Position</th>
                        <th>quote</th>
                        <th>content</th>
                        <th>layouts</th>
                        <th>action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($kotfs as $kotf)
                    <tr>
                        <td>{{$kotf->id}}</td>
                        <td><img width="100px" src="upload/kotf/{{$kotf->image1}}" alt=""></td>
                        <td><img width="100px" src="upload/kotf/{{$kotf->image2}}" alt=""></td>
                        <td>{{$kotf->fullname}}</td>
                        <td>{{$kotf->position}}</td>
                        <td>{{$kotf->quote}}</td>
                        <td>{{substr($kotf->content,0,100)}}</td>
                        <td>{{$kotf->layouts}}</td>
                        <td class="center"><i class="fa fa-pencil fa-fw"></i> <a
                                href="admin/customs/edit/{{$kotf->id}}">Edit</a> <br>
                            <i class="fa fa-trash-o  fa-fw"></i><a href="admin/customs/delete/{{$kotf->id}}"> Delete</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>
<!-- END PAGE CONTENT-->
@endsection
@section('script')

<script src="assets/vendors/DataTables/datatables.min.js" type="text/javascript"></script>

<script type="text/javascript">
    $(function() {
        $('#example-table').DataTable({
            pageLength: 10,
            //"ajax": './assets/demo/data/table_data.json',
            /*"columns": [
                { "data": "name" },
                { "data": "office" },
                { "data": "extn" },
                { "data": "start_date" },
                { "data": "salary" }
            ]*/
        });
    })
</script>
@endsection