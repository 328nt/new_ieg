<?php

namespace App\Http\Controllers;

use App\Partner;
use Illuminate\Http\Request;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $partners = Partner::All();
        return view('be/partners/list', ['partners'=>$partners]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be/partners/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required|max:500',
            'image' => 'required',

        ],[
            'name.required' =>'chưa nhập tên đối tác',
            'image.required' => 'vui lòng nhập ảnh',

        ]);
        $partners = new Partner();
        $partners->name = $rq->name;
        if ($rq->hasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/partners/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/partners", $img);
            $partners->image = $img;
        } else {
            $partners->image = "";
        }
        $partners->link = $rq->link;
        $partners->alt = $rq->alt;
        
        $partners->save();
        return redirect('admin/partners/list')->with('msg','thêm đối tác thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function show(Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $partner = Partner::Find($id);
        return view('be/partners/edit', ['partner'=>$partner ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        
        $this->validate($rq,[

        ],[

        ]);
        $partners = Partner::Find($id);
        $partners->name = $rq->name;
        $partners->link = $rq->link;
        $partners->alt = $rq->alt;
        if ($rq->hasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/partners/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/partners", $img);
            unlink("upload/partners/".$partners->image);
            $partners->image = $img;
        }
        
        $partners->save();
        return redirect('admin/partners/edit/'.$id)->with('msg','Sửa đối tác thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Partner  $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $course = Partner::find($id);
        $course->delete();
        return redirect('admin/partners/list')->with('msg', 'xóa đối tác thành công');
    }
}
