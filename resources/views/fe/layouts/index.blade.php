<!DOCTYPE html>
<html>


<!-- Mirrored from ieg.v3design.net/ by HTTrack Website Copier/3.x [XR&CO'2014], Fri, 01 Nov 2019 11:16:46 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
    <meta http-equiv="Content-Type" content="text/html" charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta http-equiv="content-language" content="vi" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title>Tổ chức Giáo dục IEG</title>
    <base href="{{asset("")}}">
    <meta name="description" content="Tổ chức Giáo dục IEG" />
    <meta name="keywords" content="Tổ chức Giáo dục IEG">

    <link rel="shortcut icon" type="image/x-icon" href="fe/fe/image/logo.ico" />
    <link rel="stylesheet" type="text/css" href="fe/fe/css/bootstrap.css">

    <script type="text/javascript" src="fe/fe/js/jquery.min.js"></script>
    <script type="text/javascript" src="fe/fe/js/bootstrap.min.js"></script>
    <!--owl-slider-->
    <!-- Stylesheets -->


    <!-- Owl Stylesheets -->
    <link rel="stylesheet" href="fe/fe/js/owl_slider/owl.carousel.min.css">
    <link rel="stylesheet" href="fe/fe/js/owl_slider/owl.theme.default.min.css">
    <link rel="stylesheet" href="fe/fe/css/all.css">
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->


    <!-- Yeah i know js should not be in header. Its required for demos.-->

    <!-- javascript -->
    <script src="fe/fe/js/owl_slider/owl.carousel.js"></script>

    <!--MenuMobile-->
    <link rel="stylesheet" type="text/css" href="fe/fe/css/menuMobile/component.css" />
    <script src="fe/fe/css/menuMobile/modernizr.custom.js"></script>
    <!--Animate-->
    <link href="fe/fe/css/aos.css" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="fe/fe/css/styles.css">
    <link rel="stylesheet" type="text/css" href="fe/fe/css/responsive_page.css">

    <link href='fe/fe/css/jquery.fancybox.min.css?v=975' rel='stylesheet' type='text/css' media='all' />
    <script defer src="fe/fe/js/jquery.fancybox.min.js?v=975" type="text/javascript"></script>
</head>

<body>

    <header>
        {{-- <nav class="navbar navbar-default">
        </nav> --}}
        <nav class="navbar navbar-default">
            <div class="container posiRelative">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <div class="container">
                        <div class="top-header row">
                            <a href="http://google.com" target="_blank" rel="noopener noreferrer">Nghề nghiệp</a>
                        </div>
                    </div>
                </div>
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="/"><img src="upload/about/{{$about->logo1}}"></a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav nav_custContent">
                        <li class="nav-item {{ request()->is('cau-chuyen-ieg.html*') ? 'active' : '' }}"><a
                                href="cau-chuyen-ieg.html">Câu chuyện IEG <span class="sr-only">(current)</span></a>
                        </li>
                        <li class="nav-item {{ request()->is('nguoi-giu-lua.html*') ? 'active' : '' }}"><a
                                href="nguoi-giu-lua.html">Những người giữ lửa</a></li>
                        <li class="dropdown nav-item {{ request()->is('day-va-hoc*.html') ? 'active' : '' }} ">
                            <a href="day-va-hoc.html" class="dropdown-toggle disabled" data-toggle="dropdown"
                                role="button" aria-haspopup="true" aria-expanded="true">Dạy và Học<span
                                    class="caret"></span></a>
                            <ul class="dropdown-menu">
                                @foreach ($courses as $course)
                                <li class="nav-item {{ request()->is('day-va-hoc/'.$course->id.'/*') ? 'active' : '' }}">
                                    <a
                                href="{{route('course', [$course->id, $course->slug_name])}}">{{$course->name}}</a></li>
                                @endforeach
                            </ul>
                        </li>
                        <li class="nav-item {{ request()->is('toi-va-ieg*') ? 'active' : '' }}"><a
                                href="toi-va-ieg.html">Tôi và IEG</a></li>
                                <li class="nav-item {{ request()->is('blog*') ? 'active' : '' }}"><a
                                        href="blog.html">Blog</a></li>
                        <li class="nav-item {{ request()->is('tin-tuc*') ? 'active' : '' }}"><a
                                href="tin-tuc.html">Tin tức</a></li>
                    </ul>
                    <ul class="nav navbar-nav navbar-right">
                        <li class="search_relative">
                            <a href="#" id="button_show"><span class="glyphicon glyphicon-search font16"></span></a>
                            <form id="item_hidden" class="search_main">
                                <input type="text" name="" placeholder="Tìm kiếm...">
                            </form>
                        </li>

                        <li><a class="btn_block045" href="login.html">Login</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </header>
    <!--/END-Header-->

    @yield('content')
    <footer>
        <div class="container flex-box footerCust">
            <div class="item_flex item_01">
                <img src="upload/about/{{$about->logo2}}">
            </div>
            <div class="item_flex item_02">
                <div class="item_flex--item">
                    <ul class="list-unstyled">
                        <h2 class="titleList_footer">Về IEG</h2>
                        <li><a href="/">Giới thiệu</a></li>
                        <li><a href="/tin-tuc.html">Tin tức</a></li>
                        <li><a href="#">Tuyển dụng</a></li>
                        <li><a href="/tin-tuc.html">Báo chí</a></li>
                    </ul>
                </div>
                <div class="item_flex--item">
                    <ul class="list-unstyled">
                        <h2 class="titleList_footer">Khóa học</h2>
                        @foreach ($courses as $item)
                        <li><a href="{{route('course', [$item->id, $item->slug_name])}}">{{$item->name}}</a></li>
                        @endforeach
                        <li><a href="#">Lịch khai giảng</a></li>
                        <li><a href="#">Học phí</a></li>
                        <li><a href="#">Học bổng</a></li>
                    </ul>
                </div>
                <div class="item_flex--item">
                    <ul class="list-unstyled">
                        <h2 class="titleList_footer">sự kiện</h2>
                        <li><a href="http://kangaroo-math.vn/" target="_blank">Kỳ thi KGR</a></li>
                        <li><a href="http://imas.ieg.vn/" target="_blank">Kỳ thi IMas</a></li>
                        <li><a href="#" target="_blank">Trại hè</a></li>
                        <li><a href="#" target="_blank">Ngoại khóa</a></li>
                        <li><a href="#" target="_blank">Thi Olympic</a></li>
                    </ul>
                </div>
                <div class="item_flex--item">
                    <ul class="list-unstyled">
                        <h2 class="titleList_footer">liên hệ</h2>
                        <li>
                            <p>
                                {{$about->address1}}
                                <br />Hotline {{$about->hotline1}}
                                <br />ĐT: {{$about->phone1}}
                                <br />{{$about->address2}}
                                <br />Hotline {{$about->hotline2}}
                                <br />ĐT: {{$about->phone2}}
                            </p>
                        </li>
                    </ul>
                </div>
            </div>
            <aside class="side-tool">
                <ul class="plugin_ico list-unstyled">
                    <li><a href="#"><i class="fab fa-facebook-square"></i></a></li>
                    <li><a href="#"><i class="fab fa-youtube"></i></a></li>
                    <li><a href="#"><i class="far fa-envelope"></i></a></li>
                    <li><a href="#"><i class="fas fa-phone-volume"></i></a></li>
                </ul>
                <div class="side-tool__sign-up">
                    <form action="{{route('contact_register')}}" method="post">
                        {{ csrf_field() }}
                        <div class="side-tool__sign-up__expand-btn"> <i class="far fa-edit"></i>
                            <p>Đăng<br> ký
                                <br>ngay</p>
                        </div>
                        <div class="side-tool__sign-up__form">
                            <input type="text" name="fullname" required placeholder="Họ tên">
                            <input type="email" name="email" required placeholder="Email">
                            <input type="password" name="password" required placeholder="Mật khẩu">

                        </div>
                        <div class="side-tool__sign-up__submit">
                            <button type="submit" class="btn-form-register"> <i class="far fa-paper-plane"></i>
                                <br>Gửi </button>
                        </div>
                    </form>
                </div>
            </aside>
        </div>
        <div class="container footerCust_bot">
            <span class="colorMain">© 2019 IEG</span>
            <span class="colorMain pull-right">Học viện phát triển Tư duy và Kỹ năng IEG</span>
        </div>
    </footer>

    <script src="fe/fe/js/main.js"></script>
    <script src="fe/fe/js/aos.js"></script>
    <script>
        AOS.init();
    </script>

</html>