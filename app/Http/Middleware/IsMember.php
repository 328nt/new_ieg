<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class IsMember
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::check()) {
            $user = Auth::User();
            if ($user->role == 1) {
                return $next($request);
            } else {
                return redirect('admin/news/list') ->with('msg','Bạn không có quyền để truy cập, vui lòng liên hệ quản trị viên');
            }
            
        }
    }
}
