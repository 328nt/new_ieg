<?php

namespace App\Http\Controllers;

use App\Slide;
use Illuminate\Http\Request;

class SlideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $slides = Slide::All();
        return view('be/slide/list', compact('slides'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('be/slide/add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $rq)
    {
        $this->validate($rq,[
            'name' => 'required',
            'image' => 'required',

        ],[
            'name.required' =>'chưa nhập tiêu đề',
            'image.required' =>'chưa nhập tóm tắt',

        ]);
        $slide = new Slide();
        $slide->name = $rq->name;
        $slide->location = $rq->location;
        $slide->alt = $rq->alt;
        if ($rq->hasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/slide/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/slide", $img);
            $slide->image = $img;
        } else {
            $slide->image = "";
        }
        
        $slide->save();
        return redirect('admin/slides/list')->with('msg','thêm slide thành công');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function show(Slide $slide)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $slide = Slide::Find($id);
        return view('be/slide/edit', ['slide'=>$slide ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($rq,[
            'name' => 'required',
            'image' => 'required',

        ],[
            'name.required' =>'chưa nhập tiêu đề',
            'image.required' =>'chưa nhập tóm tắt',

        ]);
        $slide = Slide::find($id);
        $slide->name = $rq->name;
        $slide->location = $rq->location;
        $slide->alt = $rq->alt;
        if ($rq->HasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/slide/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/slide", $img);
            unlink("upload/slide/".$slide->image);
            $slide->image = $img;
        }
        
        $slide->save();
        return redirect('admin/slides/edit/'.$id)->with('msg','Sửa slide thành công thành công');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slide  $slide
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $slide = Slide::find($id);
        $slide->delete();
        return redirect('admin/slide/list')->with('msg', 'xóa slide thành công');
    }
}
