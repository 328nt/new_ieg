<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->string('icon', 100)->nullable();
            $table->string('title')->nullable();
            $table->longText('quote')->nullable();
            $table->string('header_course')->nullable();
            $table->string('sec1_title')->nullable();
            $table->longText('sec1_content')->nullable();
            $table->string('sect1_image')->nullable();
            $table->string('sec2_title')->nullable();
            $table->longText('sec2_content')->nullable();
            $table->string('sect2_image')->nullable();
            $table->string('sec3_title')->nullable();
            $table->longText('sec3_content')->nullable();
            $table->string('sect3_image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
