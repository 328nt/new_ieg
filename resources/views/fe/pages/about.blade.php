@extends('fe/layouts/index')
@section('content')

<!--Banner-->
<div class="container-fluid">
  <div class="row"> <img src="fe/image/bgcc.png" style="width:100%"> </div>
</div>
<!--Banner_END-->

<div class="wpx1470 text-center mt_40 mb_40">
  {{-- <img src="fe/image/lichsu.png"> --}}
  <h1 class="title_block font36 mt_80 mb_80">{{$customs->title}}</h1>
</div>

<!--qoutes-->
<div class="qoutes wpx1470">
  
  <p class="qoutes_content">
    {{$customs->quote}}
</p>
</div>
<!--/qoutes_END-->

<!--Mục đích-->

<!--Đặc điểm khóa học-->
<div class="wpx1470 wdthIMG dacdiemKH">
  <div class="row search_relative mt_160 no-margin">
    <div class="col-md-7 col-sm-7 col-xs-12 oject_img"> <img src="upload/custom/{{$customs->sec1_image}}"> </div>
    <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust">
      <h2 class="title_h2"><img src="fe/image/sumenh.png" />{{$customs->sec1_title}}</h2>
      <p class="font18">{{$customs->sec1_content}}</p>
    </div>
  </div>
  <div class="row search_relative mt_160 mb_100 no-margin">
    <div class="col-md-7 col-sm-7 col-xs-12 oject_img pull-right"> <img src="upload/custom/{{$customs->sec2_image}}"> </div>
    <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust absoluteLeft_0">
      <h2 class="title_h2"><img src="fe/image/tamnhin.png" />{{$customs->sec2_title}}</h2>
      <p class="font18">{{$customs->sec2_content}}</p>
    </div>
  </div>
  <div class="row search_relative mt_160 no-margin">
    <div class="col-md-7 col-sm-7 col-xs-12 oject_img"> <img src="upload/custom/{{$customs->sec3_image}}"> </div>
    <div class="col-md-5 col-sm-5 col-xs-12 col-md-5--cust">
      <h2 class="title_h2"><img src="fe/image/giatri.png" />{{$customs->sec3_title}}</h2>
      <p class="font18">{{$customs->sec3_content}}</p>
    </div>
  </div>
</div>
<!--/Đặc điểm khóa học_END-->
<div class="bg-green mt_100 ">
  <div class="container wpx1470">
    <h1 class="title_block mt_80 mb_80">Giá trị văn hoá</h1>
    <div class="lists_year mb_80">
      <div class="list_year_item display_flex">
        <div class="year">Tin tưởng</div>
        <div class="year_content">

          <p>Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
            khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc
            biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp</p>
        </div>
      </div>
      <div class="list_year_item display_flex">
        <div class="year">Xuất sắc</div>
        <div class="year_content">
          <p>Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
            khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc
            biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp</p>
        </div>
      </div>
      <div class="list_year_item display_flex">
        <div class="year">Đổi mới</div>
        <div class="year_content">
          <p>Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
            khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và đặc
            biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp</p>
        </div>
      </div>
      <div class="list_year_item display_flex">
        <div class="year">Chia sẻ</div>
        <div class="year_content">

          <div class="year_content_wrap">
            <p>Tùy nội dung của mỗi buổi học và tùy trình độ, học sinh sẽ được thử sức với các dạng trò chơi và độ khó
              khác nhau. Đây là cách vừa kích thích trí thông minh của các em, vừa kích thích niềm đam mê Toán học và
              đặc biệt để việc học không còn là gánh nặng mà là niềm vui cho mỗi ngày đến lớp</p>
          </div>
        </div>
      </div>
    </div>
  </div>


</div>
<!--block_03-->
<div class="container wpx920_width100vh" > <!--data-aos="fade-up-right"-->
  <h1 class="title_block" style="margin-top:90px">Cách chúng tôi lan tỏa</h1>
  <div class="row">
    <div class="wpx1470 block_03">
      <div class="owl-carousel owl-theme sliderInfo cust_btn_nextPrev">
        <div class="item"> <a href="https://www.iegconsulting.vn"> <img src="fe/fe/image/img_01.jpg">
          </a>
          <h5 class="h5--sliderInfo--text">IEG Consulting</h5>
        </div>
        <div class="item"> <a href="https://www.iegfoundation.vn"> <img src="fe/fe/image/img_02.jpg">
          </a>
          <h5 class="h5--sliderInfo--text">IEG Foundation</h5>
        </div>
        <div class="item"> <a href="https://kangaroo-math.vn/"> <img src="fe/fe/image/img_02.jpg">
            <div class="sliderInfo--text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </div>
          </a>
          <h5 class="h5--sliderInfo--text">KỲ THI TOÁN QUỐC TẾ KANGAROO</h5>
        </div>
        <div class="item"> <a href="http://imas.ieg.vn/"> <img src="fe/fe/image/img_01.jpg">
            <div class="sliderInfo--text"> Lorem Ipsum is simply dummy text of the printing and typesetting industry.
              Lorem Ipsum has been the industry's standard dummy text ever since the 1500s </div>
          </a>
          <h5 class="h5--sliderInfo--text">KỲ THI ĐÁNH GIÁ NĂNG LỰC TOÁN QUỐC TẾ IMAS</h5>
          {{-- <p class="p--sliderInfo--text">Lớp Toán Học - Giải thưởng Olypic Toán 2019</p> --}}
        </div>
      </div>
      <p><a class="btn_boxMore" href="toi-va-ieg.html">Xem thêm...</a></p>
    </div>
  </div>
</div>
<!--/block_03_END-->

<!--block_05-->
<div class="container-fluid">
  <h1 class="title_block" style="margin-top:90px">Đối tác & Đồng hành</h1>
  <div class="row block_05 mb_0">
    <div class="wpx1470">
      <div class="wpx1200">
        <div class="owl-carousel owl-theme slider_doitac cust_btn_nextPrev">
          <div class="item"><img src="fe/image/logo_dt_01.png"></div>
          <div class="item"><img src="fe/image/logo_dt_02.png"></div>
          <div class="item"><img src="fe/image/logo_dt_01.png"></div>
          <div class="item"><img src="fe/image/logo_dt_02.png"></div>
          <div class="item"><img src="fe/image/logo_dt_01.png"></div>
          <div class="item"><img src="fe/image/logo_dt_02.png"></div>
        </div>
      </div>
    </div>
  </div>
</div>
<!--/block_05_END-->


@endsection