<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    
    public function authors()
    {
        return $this->belongsTo('App\User', 'author', 'id');
    }
}
