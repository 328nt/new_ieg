<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnInConsultingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('consultings', function (Blueprint $table) {
            $table->string('parent_name')->nullable();
            $table->string('student_name')->nullable();
            $table->string('dob')->nullable();
            $table->string('center')->nullable();
            $table->string('password')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('consultings', function (Blueprint $table) {
            //
        });
    }
}
