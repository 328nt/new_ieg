@extends('fe/layouts/index')
@section('content')

<div class="container-fluid">
    <div class="row">
        <img src="../fe/image/bg_pageSmall_1.png" style="width:100%">
    </div>
</div>
<!--Banner_END-->

<div class="container text-center mt_40 mb_40">
    <h1 class="title_block font36">{{$blog1->title}}</h1>
</div>

<div class="content_baiviet wpx1470">
    <div class="content_left wpx400">
        <ul class="content_left--list">

            <li class="active"><a>Danhs Mục</a></li>
            <li class="
                "><a href="tin-tuc.html">tin tuc</a></li>
            <li class="
                "><a href="toi-va-ieg.html">toi va ieg</a></li>
            <li class="
                "><a href="su-kien.html">su kien</a></li>
            <li class="
                "><a href="tin-tuc.html">tin tuc</a></li>
            <li class="active
                "><a href="highcharts-demo.html">Highcharts Demo</a></li>
        </ul>
        <ul class="content_left--list mt_130_cust ">
            <li class="active"><a>Bài viết tương tự</a></li>
            @foreach ($blogs as $blog)

            <li>
                <ul class="list-inline">
                    <li class="col-md-6">
                        <a href="tin-tuc/de-doc-nhung-noi.html">
                            <img src="upload/blog/{{$blog->image}}">
                        </a>
                    </li>
                    <li class="col-md-6">
                        <a class="font18 apd-10"
                            href="toi-va-ieg/{{$blog->id}}/{{$blog->titlenone}}.html">{{$blog->title}}</a>
                    </li>
                </ul>
            </li>
            @endforeach
        </ul>
    </div>
    <div class="content_right">

        <div class="text-center mt_40 mb_40">
            <h1 class="title_block font36">{{$blog1->title}}</h1>
        </div>
        <div class="toivaIEG_baivietchitiet mb_100">
            <span class="wimg100"><img src="upload/blog/{{$blog1->image}}"></span>
            <p class="mt_40"><span class="time_blogs">{{date("d/m/Y", strtotime($blog1->created_at))}}</span></p>
            <p>{!!$blog1->content!!}</p>
            <div class="col-md-12 mt_40">
                <div class="row">
                    <ul class="flex-box">
                        <li>
                            <a href="#"><i class="fab fa-facebook-f"></i></a>
                            <a href="#"><i class="fab fa-twitter"></i></a>
                        </li>
                        <li>
                            <ul>
                                <li>
                                    <a href="#">Creat</a>
                                </li>
                                <li>
                                    Admin User
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<!--Khóa học-->
<div class="container wpx920_width100vh">
    <h1 class="title_block">CÁC KHÓA HỌC TẠI IEG</h1>
    <div class="row flex-box block_01">
        <div class="col-2-cust text-center">
            <a href="day-va-hoc/tieng-anh.html">
                <img src="fe/fe/image/iconinfo_01.png">
                <p class="info_KH">Tiếng Anh</p>
            </a>
        </div>
        <div class="col-2-cust text-center">
            <a href="khoa-hoc.html">
                <img src="fe/fe/image/iconinfo_02.png">
                <p class="info_KH">Khoa Học</p>
            </a>

        </div>
        <div class="col-2-cust text-center">
            <a href="day-va-hoc/toan-hoc.html">
                <img src="fe/fe/image/iconinfo_03.png">
                <p class="info_KH">Toán Học</p>
            </a>
        </div>
        <div class="col-2-cust text-center">
            <a href="day-va-hoc/socrates.html">
                <img src="fe/fe/image/iconinfo_04.png">
                <p class="info_KH">Socrates</p>
            </a>

        </div>
        <div class="col-2-cust text-center">
            <a href="day-va-hoc/ielts.html">
                <img src="fe/fe/image/iconinfo_05.png">
                <p class="info_KH">IELTS</p>
            </a>

        </div>
    </div>
</div>
<!--/Khóa học_END-->
@endsection