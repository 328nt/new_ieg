<?php

namespace App\Http\Controllers;

use App\Consulting;
use Illuminate\Http\Request;
use Auth;

class ConsultingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index_consultings()
    {
        $consultings = Consulting::all();
        return view ('be/contacts/consultings', compact('consultings'));
    }
    
    public function index_test()
    {
        $consultings = Consulting::all();
        return view ('be/contacts/test', compact('consultings'));
    }
    public function index_register()
    {
        $consultings = Consulting::all();
        return view ('be/contacts/register', compact('consultings'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store_consulting(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        $consult = new Consulting();
        $consult->fullname = $rq->fullname;
        $consult->email = $rq->email;
        $consult->phone = $rq->phone;
        $consult->content = $rq->content;
        $consult->course = $rq->course;
        $consult->category = '1'; //consulting
        $consult->save();
        return redirect()->route('congratulations')->with('msg','Đăng ký tư vấn thành công, vui lòng đợi nhân viên gọi lại tư vấn !');
    }
    
    public function store_test(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        $test = new Consulting();
        $test->fullname = $rq->fullname;
        $test->phone = $rq->phone;
        $test->email = $rq->email;
        $test->student_name = $rq->student_name;
        $test->dob = $rq->dob;
        $test->center = $rq->center;
        $test->content = $rq->content;
        $test->category = '2'; //test
        $test->save();
        return redirect()->route('congratulations')->with('msg','Đăng ký lịch test thành công, vui lòng đợi nhân viên gọi lại tư vấn !');
    }
    
    public function store_register(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        $consult = new Consulting();
        $consult->fullname = $rq->fullname;
        $consult->email = $rq->email;
        $consult->password = bcrypt($rq->pwd);
        $consult->category = '3'; //register
        $consult->save();
        return redirect()->route('congratulations')->with('msg','Đăng ký thành công !');
    }

    // public function contacted(Request $rq)
    // {
    //     $id = $rq->id;
    //     dd($id);
    //     $update = Consulting::find($id);
    //     $update->status = '1';
    //     $update->author = Auth::user()->id;
    //     $update->save();
    //     return redirect()->back()->with('msg', 'Đã liên hệ');
    // }
    
    public function contacted(Request $rq)
    {
        $id = $rq->id;
        $consult = Consulting::find($id);
        $consult->status = '1';
        $consult->author = Auth::user()->id;
        $consult->save();
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Consulting  $consulting
     * @return \Illuminate\Http\Response
     */
    public function show(Consulting $consulting)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Consulting  $consulting
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $consult = Consulting::find($id);
        return view('be/contacts/edit', compact('consult'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Consulting  $consulting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $rq, $id)
    {
        $consult = Consulting::find($id);
        dd($consult);
        $consult->fullname = $rq->fullname;
        $consult->author = Auth::user()->id;
        $consult->save();
        return redirect()->back()->withInput();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Consulting  $consulting
     * @return \Illuminate\Http\Response
     */
    public function destroy(Consulting $consulting)
    {
        //
    }
}
