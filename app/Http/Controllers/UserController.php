<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Role;

use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function index()
    {
        $users = User::All();
        return view('be/user/list', ['users'=>$users]);
    }

    public function create()
    {
        $roles = Role::All();
        return view('be/user/add', ['roles'=>$roles]);
    }
    public function store(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        $users = new User();
        $users->fullname = $rq->fullname;
        $users->email = $rq->email;
        $users->birthday = $rq->birthday;
        $users->role = $rq->role;
        $users->password = bcrypt($rq->password);
        if ($rq->hasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/users/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/users", $img);
            $users->image = $img;
        } else {
            $users->image = "";
        }
        
        $users->save();
        return redirect('admin/users/list')->with('msg','thêm quản trị viên thành công !');
    }

    public function edit($id)
    {
        $roles = Role::All();
        $users = User::Find($id);
        return view('be/user/edit', ['roles'=>$roles, 'users'=>$users]);
    }

    public function update(Request $rq, $id)
    {
        $this->validate($rq,[

        ],[

        ]);
        $users = User::Find($id);
        
        $users->fullname = $rq->fullname;
        $users->email = $rq->email;
        $users->role = $rq->role;
        if ($rq->changepass == "on")
        {
            $this->validate($rq,[
                'password' => 'required',
                'repassword' => 'required|same:password',
            ],[
                'name.required' => 'mật khẩu không được để trống',
                'repassword.same' => 'mật khẩu nhập lại chưa đúng'
            ]);
            $users->password = bcrypt($rq->password);
        }
        if ($rq->HasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/users/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/users", $img);
            unlink("upload/users/".$users->image);
            $users->image = $img;
        }
        $users->save();
        return redirect('admin/users/edit/'.$id)->with('msg','sửa thông tin Quản trị viên thành công');
    }

    public function destroy($id)
    {
        $users = User::Find($id);
        $users->delete();
        return redirect('admin/users/list')->with('msg','xóa quản trị viên thành công !');
    }

    public function getloginadmin()
    {
        return view('be/login');
    }
    public function postloginadmin(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        if (Auth::attempt(['email' => $rq->email, 'password' => $rq->password])) {
            return redirect('admin/users/list');
        }
        else {
            return redirect('admin/login')->with('msg','sai tài khoản hoặc mật khẩu !');
        }
    }
    public function logoutadmin()
    {
        Auth::logout();
        return redirect('admin/login')->with('msg','Đăng xuất thành công !');
    }
}
