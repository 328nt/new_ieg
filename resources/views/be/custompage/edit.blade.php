@extends('be.layouts.index')
@section('title')
CustomPages 
@endsection
@section('content')
@include('msg')
<div class="">
    <div class="row">
        <div class="col-md-12">
            <div class="ibox">
                <div class="ibox-head">
                    <div class="ibox-title">CustomPages</div>
                    <div class="ibox-tools">
                        <a class="ibox-collapse"><i class="fa fa-minus"></i></a>
                        <a class="fullscreen-link"><i class="fa fa-expand"></i></a>
                    </div>
                </div>
                <div class="ibox-body">
                    <form action="{{route('update_config', $custom->id)}}" method="post" enctype="multipart/form-data"
                        class="form-horizontal" id="form-sample-1" novalidate="novalidate">
                        {{ csrf_field() }}
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">title</label>
                            <textarea name="title" id="" cols="80%" rows="5">{{$custom->title}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">quote</label>
                                <textarea name="quote" id="" cols="80%" rows="5">{{$custom->quote}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">Sec-1-title</label>
                            <textarea name="sec1_title" id="" cols="80%" rows="5">{{$custom->sec1_title}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">Sec-1-content</label>
                                <textarea name="sec1_content" id="" cols="80%" rows="5">{{$custom->sec1_content}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9">
                            <label class="col-sm-2 col-form-label">Image1</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="sec1_image" type="file">
                            </div>
                            </div>
                            <div class="col-sm-3">
                                <img width="300px" src="upload/custom/{{$custom->sec1_image}}" alt="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">Sec-2-title</label>
                            <textarea name="sec2_title" id="" cols="80%" rows="5">{{$custom->sec2_title}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">Sec-2-content</label>
                                <textarea name="sec2_content" id="" cols="80%" rows="5">{{$custom->sec2_content}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9">
                            <label class="col-sm-2 col-form-label">Image2</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="sec2_image" type="file">
                            </div>
                            </div>
                            <div class="col-sm-3">
                                <img width="300px" src="upload/custom/{{$custom->sec2_image}}" alt="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">Sec-3-title</label>
                            <textarea name="sec3_title" id="" cols="80%" rows="5">{{$custom->sec3_title}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10">
                                <label class="col-sm-2 col-form-label">Sec-3-content</label>
                                <textarea name="sec3_content" id="" cols="80%" rows="5">{{$custom->sec3_content}}</textarea>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-9">
                            <label class="col-sm-2 col-form-label">Image3</label>
                            <div class="col-sm-8">
                                <input class="form-control" name="sec3_image" type="file">
                            </div>
                            </div>
                            <div class="col-sm-3">
                                <img width="300px" src="upload/custom/{{$custom->sec3_image}}" alt="">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-sm-10 ml-sm-auto">
                                <button class="btn btn-info" type="submit">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
<script type="text/javascript">
    $(function() {
        $('#summernote').summernote();
        $('#summernote_air').summernote({
            airMode: true
        });
    });
</script>
@endsection