<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;
use App\Gallery_videos;
use App\Category;
use App\Customskotf;
use App\Customconfig;
use App\News;
use App\Blog;
use App\Role;
use App\User;
use App\Course;
use App\Custompages;
use App\About;
use App\Partner;
use App\Mail\Sendmail;
use Mail;

class PagesController extends Controller
{
    function __construct() 
    {
        $courses = Course::all();
        $partners = Partner::all();
        $about = About::all()->first();
        // dd($about);
        view::share(['courses'=>$courses, 'about'=>$about,  'partners'=>$partners]);
    }
    public function index()
    {
        $blogs = Blog::orderBy('id', 'DESC')->paginate(4);
        // dd($blogs);
        return view('fe/pages/home', compact('blogs'));
    }
    
    public function about()
    {
        $customs = Custompages::all()->first();
        return view('fe/pages/about', compact('customs'));
    }

    public function kotf()
    {
        $kotfs = Customskotf::all();
        return view('fe/pages/kotf', compact('kotfs'));
    }

    public function teachandlearn()
    {
        $courses = Course::all();
        return view('fe/pages/teachandlearn', compact('courses'));
    }
    public function course($id)
    {
        $course = Course::find($id);
        return view('fe/pages/course', compact('course'));
    }

    public function math()
    {
        return view('fe/pages/math');
    }
    public function eng()
    {
        return view('fe/pages/eng');
    }
    public function science()
    {
        return view('fe/pages/science');
    }
    public function socrates()
    {
        return view('fe/pages/socrates');
    }
    public function ielts()
    {
        return view('fe/pages/ielts');
    }
    public function congratulations()
    {
        return view('fe/pages/congratulations');
    }
    public function events()
    {
        $events = News::where('id_category', 2)->take(6)->get();
        return view('fe/pages/events', compact('events'));
    }

    public function meandieg()
    {
        $blogs = Blog::orderBy('id', 'DESC')->paginate(10);
        $blog1 = $blogs->shift();
        // dd($blog1);
        return view('fe/pages/meandieg', compact('blogs', 'blog1'));
    }
    public function blogsingle($id)
    {
        $blog1 = Blog::find($id);
        // dd($blog1);
        $blogs = Blog::orderBy('id', 'DESC')->paginate(3);
        return view('fe/pages/blog', compact('blog1', 'blogs'));
    }


    public function news()
    {
        $spotlight = News::where('id_category', 1)->where('hightlight', 1)->orderBy('id','DESC')->take(3)->get();
        $allspl = $spotlight->shift();
        $events = News::where('id_category', 2)->take(2)->get();
        $news = News::orderBy('id', 'DESC')->paginate(7);
        $new1 = $news->shift();
        // dd($new);
        return view('fe/pages/news', compact('news', 'new1', 'spotlight', 'events', 'allspl'));
    }
    
    // public function blogsingle($id)
    // {
    //     $new1 = News::find($id);
    //     // dd($new1);
    //     $news = News::orderBy('id', 'DESC')->paginate(3);
    //     return view('fe/pages/new', compact('new1', 'news'));
    // }
    
    public function newsingle($id)
    {
        $spotlight = News::where('hightlight', 1)->take(3)->get();
        $new1 = News::find($id);
        $categories = Category::all();
        // $previous = News::where('id', '<', $new->id)->max('id');
        // $previous_title = News::find($previous);
        // $next = News::where('id', '>', $new->id)->min('id');
        // $next_title = News::find($next);
        $news = News::orderBy('id', 'DESC')->paginate(3);
        return view('fe/pages/new', compact('new1', 'news', 'categories'));
    }

    public function images()
    {
        return view('fe.pages.images');
    }

    
    public function gallery_videos()
    {
        $videos = Gallery_videos::all();
        $spotlight = Gallery_videos::where('highlight', 1)->orderBy('id','DESC')->take(3)->get();
        return view('fe.pages.gallery_videos', compact('videos', 'spotlight'));
    }
    public function video($id)
    {
        $video = Gallery_videos::find($id);
        $previous = Gallery_videos::where('id', '<', $video->id)->max('id');
        $previous_title = Gallery_videos::find($previous);
        $next = Gallery_videos::where('id', '>', $video->id)->min('id');
        $next_title = Gallery_videos::find($next);
        $videos = Gallery_videos::orderBy('id', 'DESC')->paginate(3);
        return view('fe.pages.video', compact('video', 'previous','previous_title', 'next_title', 'next', 'videos'));
    }

    

    public function sendmail()
    {
        return view('be.mail.sendmail');
    }
    public function gogoemail(request $req)

    
    {
        // $id = $req->id;
        // $contestants = Contestants::find($id);
        // $contestants->save();
        $contestant = $req->email;
        
        // $contestant = ["datnt@ieg.vn"];
        
        $contestant = json_decode($contestant);
        // dd($contestant);
        foreach($contestant as $contestants){

            // dd($contestants);
            Mail::to($contestants)->send(new Sendmail($contestants));
        }

    return redirect()->back()->with('msg','ô kê rồi đấy !!!!');
    }


    public function search()
    {
        return view('fe/pages/search');
    }


    

    public function search_results(Request $rq) 
    {
        $fullname = trim($rq->search);
        $dpt = $rq->department;
        $staff = User::where('fullname', 'like',"%{$fullname}%")->where('id_department', $dpt)->get();
        $data['staff'] = $staff;
        dd($data['staff']);
        return view('fe/pages/about', compact('staff'));
    }


    //account
    public function getaccount($id)
    {
        $departments = Department::All();
        $positions = Position::All();
        $roles = Role::All();
        $user = User::Find($id);
        return view('fe/account/edit', ['positions'=>$positions, 'roles'=>$roles, 'departments'=>$departments, 'user'=>$user]);
    }

    public function postaccount(Request $rq, $id)
    {
        $this->validate($rq,[

        ],[

        ]);
        $users = User::Find($id);
        $users->lead = $rq->phone;

        if ($rq->changepass == "on")
        {
            $this->validate($rq,[
                'password' => 'required',
                'repassword' => 'required|same:password',
            ],[
                'name.required' => 'mật khẩu không được để trống',
                'repassword.same' => 'mật khẩu nhập lại chưa đúng'

            ]);
            $users->password = bcrypt($rq->password);
        }


        if ($rq->HasFile('image')) {
            $file = $rq->file('image');
            $name = $file->getClientOriginalName();
            $img = str_random(4)."-".$name;
            while (file_exists("upload/users/".$img)){
                $img = str_random(4)."-".$name;
            }
            $file->move("upload/users", $img);
            unlink("upload/users/".$users->image);
            $users->image = $img;
        }
        $users->save();
        return redirect('staff/account/'.$id)->with('msg','sửa thông tin nhân viên thành công');
    }

    

    public function getloginstaff()
    {
        return view('fe/login');
    }
    public function postloginstaff(Request $rq)
    {
        $this->validate($rq,[

        ],[

        ]);
        if (Auth::attempt(['usersname' => $rq->usersname, 'password' => $rq->password])) {
            return redirect('/about');
        }
        else {
            return redirect('staff/login')->with('msg','sai tài khoản rồi thím ơi !');
        }
    }
    public function logoutstaff()
    {
        Auth::logout();
        return redirect('staff/login')->with('msg','Đăng xuất thành công bạn ôi !');
    }
}
